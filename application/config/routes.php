<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['default_controller'] = 'welcome';

$route = [
    "translate_uri_dashes" => FALSE,

    "default_controller" => "index",
    "404_override" => "errors/err404",

    // Auth
    "auth/login" => [
        "POST" => "Auth/doLogin",
    ],

    // Profile
    "userprofile" => "Profile/index",
    "userprofile/changepassword" => "Profile/changePassword",
    "userprofile/changedisplayname" => "Profile/changeDisplayName",

    // Companies
    "companies/edit/(:num)" => [
        "GET" => "Companies/editCompanyView/$1",
        "POST" => "Companies/editCompany"
    ],
    "companies/create" => [
        "GET" => "Companies/createComapnyView",
        "POST" => "Companies/createComapny"
    ],
    "companies/disable/(:num)" => ["POST" => "Companies/disableCompany/$1"],
    "companies/enable/(:num)" => ["POST" => "Companies/enableCompany/$1"],

    // Servers
    "servers/edit/(:num)" => [
        "GET" => "Servers/editServerView/$1",
        "POST" => "Servers/editServer"
    ],

    "servers/create" => [
        "GET" => "Servers/createServerView",
        "POST" => "Servers/createServer"
    ],

    "servers/delete/(:num)" => ["POST" => "Servers/deleteServer/$1"],

    // Domains
    "domains/create" => ["POST" => "Domains/createDomain"],
    "domains/delete/(:num)" => ["POST" => "Domains/deleteDomain/$1"],

    // Accounts
    "accounts/create/upfile" => [
        "GET" => "Accounts/createAccountsUpFileView",
        "POST" => "Accounts/createAccountsUpFile"
    ],
    "accounts/create/confirm" => [
        "POST" => "Accounts/createAccountsConfirm"
    ],
    "/accounts/getbydomain/(:num)" => [
        "POST" => "Accounts/getByDomain/$1"
    ],
    "/accounts/editAccount" => [
        "POST" => "Accounts/editAccount",
    ],
    "/accounts/deleteaccount/(:num)" => ["POST" => "Accounts/deleteAccount/$1"],

    // Jobs
    "jobs/editjob/(:num)" => [
        "GET" => "Jobs/editJobView/$1",
        "POST" => "Jobs/editJob/$1"
    ],
    "jobs/create" => [
        "GET" => "Jobs/createJobView",
        "POST" => "Jobs/createJob"
    ],
    "jobs/delete/(:num)" => [
        "POST" => "Jobs/deleteJob/$1"
    ],

    // Users
    "users/edit" => ["POST" => "Users/editUser"],
    "users/create" => ["POST" => "Users/createUser"],
    "users/delete/(:num)" => ["POST" => "Users/deleteUser/$1"],
];










