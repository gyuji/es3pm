<?php

namespace ES3\Controllers;

use ES3\Controllers\BaseController;
use ES3\Services\Empresas as SEmpresas;
use ES3\Services\Auth as SAuth;
use ES3\Forms\FormCreateCompany;
use ES3\Forms\FormEditCompany;
use ES3\Validations;
use ES3\Response;
use ES3\Views;

class Companies extends BaseController
{
	public function __initialize()
	{
		SAuth::requireAuth();
	}

	public function index()
	{
		$this->view->menu($this->getMenu())
			->getItem("companies")->active = true;
		$this->view->set([
				"title" => "Empresas",
				"ngCtrSec" => "ctrCompanies"
			])->setVB([
				"companiesList" => SEmpresas::getInstance()->getCompaniesListUserAccess()
			]);
		$this->view->breadcrumb()
			->setPageDesc("Empresas")
			->addCrumb([
				"Empresas",
				"Todas as empresas"
			]);
		$this->view->load("companies/grid");
	}

	public function editCompanyView($IDCompany)
	{
		$SEmpresas = SEmpresas::getInstance();
		$userAuth = SAuth::getInstance()->getUserAuth();

		if (!$SEmpresas->isPermitedAccessCompany($IDCompany)) {
			$this->view->menu($this->getMenu())
				->getItem("companies")->active = true;
			$this->view->set([
					"title" => "Empresas",
					"ngCtrSec" => "ctrCompanies"
				])->setVB([
					"companiesList" => SEmpresas::getInstance()->getCompaniesListUserAccess()
				]);
			$this->view->breadcrumb()
				->setPageDesc("Empresas")
				->addCrumb([
					["label" => "Empresas", "url" => "/companies"],
					"Todas as empresas"
				])->setTopAlert("warning", "Você não tem permissão para acessar a empresa #$IDCompany");
			$this->view->load("companies/grid");
			return;
		}
		

		$CompanyData = $SEmpresas->getCompanyByID($IDCompany);
		$FormEditCompany = new FormEditCompany();
		$FormEditCompany->fill($CompanyData);
		$resaleList = $SEmpresas->getResaleListUserAccess();
		foreach ($resaleList as $row) {
			$FormEditCompany->IDRevenda->options[$row->ID] = "CID{$row->ID} : {$row->RazaoSocial}";
			$FormEditCompany->IDRevenda->data[$row->ID] = $row;
		}
		if ($userAuth->IDEmpresas == $IDCompany) {
			$FormEditCompany->Tipo->disabled = true;
			$FormEditCompany->IDRevenda->disabled = true;
			$FormEditCompany->btnDelete = false;
		}

		$this->view->menu($this->getMenu())
			->getItem("companies")->active = true;
		$this->view->set([
				"title" => "Empresas ",
				"ngCtrSec" => "ctrCompanies",
				"companyData" => $CompanyData
			])->setForm($FormEditCompany);
		$this->view->breadcrumb()
			->setPageDesc("Empresas")
			->addCrumb([
				["label" => "Empresas", "url" => "/companies"],
				$CompanyData->RazaoSocial
			])->setButtonReturn("/companies");
		$this->view->load("companies/edit");
	}

	public function editCompany()
	{
		$SEmpresas = SEmpresas::getInstance();
		$FormEditCompany = new FormEditCompany(false);
		$ValidationHtml = $FormEditCompany->validate()->getHtml();

		if ($ValidationHtml) Response::send([
			"status" => false,
			"code" => "invalidinput",
			"msg" => $ValidationHtml
		]);

		$serviceResponse = SEmpresas::getInstance()->updateCompany($FormEditCompany->toObject());

		$serviceResponse->send();
	}

	public function createComapnyView()
	{
		$FormCreateCompany = new FormCreateCompany();
		$resaleList = SEmpresas::getInstance()->getResaleListUserAccess();
		foreach ($resaleList as $row) {
			$FormCreateCompany->IDRevenda->options[$row->ID] = "CID{$row->ID} : {$row->RazaoSocial}";
			$FormCreateCompany->IDRevenda->data[$row->ID] = $row;
		}

		$this->view->menu($this->getMenu())
			->getItem("companies")->active = true;
		$this->view->set([
				"title" => "Empresas | Cadastrar",
				"ngCtrSec" => "ctrCompanies"
			])->setForm($FormCreateCompany);
		$this->view->breadcrumb()
			->setPageDesc("Cadastrar Empresa")
			->addCrumb([
				["label" => "Empresas", "url" => "/companies"],
				"Cadastrar Empresa"
			])->setButtonReturn("/companies");
		$this->view->load("companies/create");
	}

	public function createComapny()
	{
		$FormCreateCompany = new FormCreateCompany(false);
		$ValidationHtml = $FormCreateCompany->validate()->getHtml();

		if ($ValidationHtml) Response::send([
			"status" => false,
			"code" => "invalidinput",
			"msg" => $ValidationHtml
		]);

		$serviceResponse = SEmpresas::getInstance()->createCompany($FormCreateCompany->toObject());
		
		if ($serviceResponse->status) {
			$serviceResponse->redirect = base_url("/companies/edit/{$FormCreateCompany->cid->value}");
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "success",
				"msg" => "Empresa <b>CID{$FormCreateCompany->cid->value} {$FormCreateCompany->RazaoSocial->value}</b> cadastrada com sucesso"
			]);
		}
		$serviceResponse->send();
	}

	public function disableCompany($IDCompany)
	{
		$serviceResponse = SEmpresas::getInstance()->disableCompany($IDCompany);
		$rsEmpresa = SEmpresas::getInstance()->getCompanyByID($IDCompany);
		if ($serviceResponse->status) {
			$serviceResponse->redirect = base_url("/companies/edit/{$IDCompany}");
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "success",
				"msg" => "Empresa <b>CID{$rsEmpresa->ID} {$rsEmpresa->RazaoSocial}</b> desativada com sucesso"
			]);
		}
		$serviceResponse->send();
	}

	public function enableCompany($IDCompany)
	{
		$serviceResponse = SEmpresas::getInstance()->enableCompany($IDCompany);
		$rsEmpresa = SEmpresas::getInstance()->getCompanyByID($IDCompany);
		if ($serviceResponse->status) {
			$serviceResponse->redirect = base_url("/companies/edit/{$IDCompany}");
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "success",
				"msg" => "Empresa <b>CID{$rsEmpresa->ID} {$rsEmpresa->RazaoSocial}</b> habilitada com sucesso"
			]);
		}
		$serviceResponse->send();
	}
}
