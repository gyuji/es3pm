<?php

namespace ES3\Controllers;

use ES3\Forms\Form;
use ES3\Forms\FormLogin;
use ES3\Services\Auth as SAuth;
use ES3\Response;
use ES3\Validations;

class Auth extends BaseController
{
    public function index()
    {
        $this->login();
    }

    public function login()
    {
        SAuth::redirectIsAuth();

        $this->view->set([
            "title" => "ES3 - Login",
            "ngCtrSec" => "ctrLogin"
        ])->setForm(new FormLogin());
        $this->view->load("login");
    }

    public function doLogin()
    {
        $formLogin = new FormLogin(false);
        $username = $formLogin->username;
        $password = $formLogin->password;
        $fieldValidation = $formLogin->validate();
        
        // Verifica se os dados vieram do formulários
        if ($fieldValidation->get("username")->code == "required" || 
            $fieldValidation->get("password")->code == "required") Response::send([
            "status" => false,
            "code" => "missingparams",
            "msg" => $fieldValidation->getHtml()
        ]);

        // Valida os dados do forumulário
        if ($fieldValidation->get("username")->code == "invalid:email") Response::send([
            "status" => false,
            "code" => "invalidparams::username",
            "msg" => $fieldValidation->getHtml()
        ]);

        $SAuth = SAuth::getInstance();
        if (!$SAuth->doLogin($username->value, $password->value)) {
            Response::send([
                "status" => false,
                "code" => "failedlogin",
                "msg" => "Usuário ou senha incorretos"
            ]);
        }

        Response::send([
            "status" => true,
            "code" => "successlogin",
            "msg" => "Usuário autenticado com sucesso",
            "redirect" => base_url("/")
        ]);
    }

    public function logout()
    {
        SAuth::getInstance()->destroy();
        $this->login();
    }
}