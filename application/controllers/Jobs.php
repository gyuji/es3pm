<?php

namespace ES3\Controllers;

use ES3\Forms\Form;
use ES3\Forms\FormCreateJob;
use ES3\Forms\FormEditJob;
use ES3\Services\Jobs as SJobs;
use ES3\Services\Auth as SAuth;
use ES3\Services\Empresas as SEmpresas;
use ES3\Services\Dominios as SDominios;
use ES3\Services\Servidores as SServidores;
use ES3\Services\Contas as SContas;
use ES3\Response;
use ES3\Validations;

class Jobs extends BaseController
{
    public function __initialize()
	{
		SAuth::requireAuth();
    }

    public function index()
    {
        $rsJobs = SJobs::getInstance()->getAllByCompany();

        $this->view->menu($this->getMenu())
            ->getItem("jobs")->active = true;
		$this->view->breadcrumb()
			->setPageDesc("Jobs")
			->addCrumb([
				"Jobs",
				"Todos os Jobs"
			]);
		$this->view->set([
				"title" => "Jobs",
                "ngCtrSec" => "ctrJobs"
            ])->setVB([
                "jobsData" => $rsJobs
            ]);
		$this->view->load("jobs/grid");
    }

    public function editJobView($IDJob)
    {
        $rsJobs = SJobs::getInstance()->getByID($IDJob);

        if (!$rsJobs) {
            $this->session->set_flashdata("Breadcrumb::topAlert", [
                "type" => "warning",
                "msg" => "Job #<b>$IDJob</b> não encontrado para edição"
            ]);
            redirect("jobs");
        }

        $formEditJob = new FormEditJob();
        $formEditJob->fill($rsJobs);
        

        $this->view->menu($this->getMenu())
            ->getItem("jobs")->active = true;
		$this->view->breadcrumb()
			->setPageDesc("Jobs")
			->addCrumb([
				["label" => "Jobs", "url" => "/jobs"],
				"#$IDJob"
			])->setButtonReturn("/jobs");
		$this->view->set([
				"title" => "Editar Job",
                "ngCtrSec" => "ctrJobs"
            ])->setVB([
                "accList" => SContas::getInstance()->getByDomain($rsJobs->IDDominios),
                "jobData" => $rsJobs
            ])->setForm($formEditJob);
		$this->view->load("jobs/edit");
    }

    public function editJob()
    {
        $formEditJob = new FormEditJob(false);
        $fieldValidation = $formEditJob->validate()->getHtml();
        if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
        }

        $serviceResponse = SJobs::getInstance()->updateJob($formEditJob->toObject());

        if ($serviceResponse->status) Response::send([
            "status" => true,
            "code" => "ok",
            "msg" => "Jobs #<b>{$formEditJob->ID->value}</b> atualizado com sucesso"
        ]);
        $serviceResponse->send();
    }

    public function createJobView()
    {
        $rsEmpresas = SEmpresas::getInstance()->getCompaniesListUserAccess();
        $rsDominios = SDominios::getInstance()->getByCompaniesList();
        $rsServidores = SServidores::getInstance()->getServersList();

        $formCreateJob =new FormCreateJob();

        if (count($rsEmpresas) == 1) {
            $formCreateJob->IDEmpresas->value = array_values($rsEmpresas)[0]->ID;
            $formCreateJob->IDEmpresas->disabled = true;
        }

        if (count($rsDominios) == 1) {
            $formCreateJob->IDDominios->value = array_values($rsDominios)[0]->ID;
            $formCreateJob->IDDominios->disabled = true;
        }

        $this->view->menu($this->getMenu())
            ->getItem("jobs")->active = true;
		$this->view->breadcrumb()
			->setPageDesc("Jobs")
			->addCrumb([
				["label" => "Jobs", "url" => "/jobs"],
				"Cadastrar um novo job"
			])->setButtonReturn("/jobs");
		$this->view->set([
				"title" => "Cadastrar um novo Job",
                "ngCtrSec" => "ctrJobs"
            ])->setVB([
                "EmpresasList" => $rsEmpresas,
                "DominiosList" => $rsDominios,
                "ServidoresList" => $rsServidores
            ])->setForm($formCreateJob);
		$this->view->load("jobs/create");
    }

    public function createJob()
    {
        $formCreateJob = new FormCreateJob(false);
        $fieldValidation = $formCreateJob->validate()->getHtml();
        if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
        }

        $serviceResponse = SJobs::getInstance()->createJob($formCreateJob->toObject());

        if ($serviceResponse->status) Response::send([
            "status" => true,
            "code" => "ok",
            "redirect" => base_url("/jobs/editjob/{$serviceResponse->data}"),
            "msg" => "Jobs #<b>{$serviceResponse->data}</b> criado com sucesso"
        ]);
        $serviceResponse->send();
    }

    public function deleteJob($IDJob)
    {
        $serviceResponse = SJobs::getInstance()->disableJob($IDJob);
		if ($serviceResponse->status) {
			$serviceResponse->redirect = base_url("jobs");
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "success",
				"msg" => "Job <b>#{$IDJob} removido com sucesso"
			]);
		}
		$serviceResponse->send();
    }
}