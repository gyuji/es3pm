<?php

namespace ES3\Controllers;

use ES3\View\View;
use ES3\View\Item;
use ES3\View\Subitem;
use ES3\View\Navbar;
use ES3\Services\Auth as SAuth;

class BaseController extends \CI_Controller
{
    /**
     * @var View
     */
    protected $view;

    public function __construct() {
        parent::__construct();

        if (method_exists($this, "__initialize")) $this->__initialize();

        $this->view = new View();
    }

    public function getMenu()
    {
        $SAuth = SAuth::getInstance();
        $menu = new Navbar();
        $menu->addItem(new Item([
           "id" => "dashboard",
           "label" => "Dashboard",
           "type" => Item::SINGLE,
           "icon" => "fa fa-th-large",
           "href" => "/"
        ]));
        $menu->addItem(new Item([
            "id" => "accounts",
            "label" => "Contas",
            "type" => Item::SINGLE,
            "icon" => "fa fa-group",
            "href" => "/accounts"
        ]));
        $menu->addItem(new Item([
            "id" => "jobs",
            "label" => "Jobs",
            "type" => Item::SINGLE,
            "icon" => "fa fa-tasks",
            "href" => "/jobs"
        ]));
        if ($SAuth->isResale() || $SAuth->isRootCompany()) {
            $menu->addItem(new Item([
                "id" => "companies",
                "label" => "Empresas",
                "type" => Item::SINGLE,
                "icon" => "fa fa-building",
                "href" => "/companies"
            ]));
            $menu->addItem(new Item([
                "id" => "users",
                "label" => "Usuários",
                "href" => "/users",
                "type" => Item::SINGLE,
                "icon" => "fa fa-user-circle"
            ]));
        }
        if ($SAuth->isRootCompany()) {
            $menu->addItem(new Item([
                "id" => "domains",
                "label" => "Domínios",
                "type" => Item::SINGLE,
                "icon" => "fa fa-at",
                "href" => "/domains"
            ]));
            $menu->addItem(new Item([
                "id" => "servers",
                "label" => "Servidores",
                "type" => Item::SINGLE,
                "icon" => "fa fa-server",
                "href" => "/servers"
            ]));
        }

        /*$menu->addItem(new Item([
            "id" => "batata",
            "label" => "fdasfdsafd",
            "type" => Item::DROPDOWN
        ]))->addSubitem("batata", new Subitem([
            "label" => "Item 1",
            "href" => "https://google.com"
        ]))->addSubitem("batata", new Subitem([
            "label" => "Item 1",
            "href" => "https://google.com"
        ]));*/

        return $menu;
    }
}
