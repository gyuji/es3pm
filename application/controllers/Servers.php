<?php

namespace ES3\Controllers;

use ES3\Controllers\BaseController;
use ES3\Services\Servidores as SServidores;
use ES3\Services\Auth as SAuth;
use ES3\Forms\FormCreateServer;
use ES3\Forms\FormEditServer;
use ES3\Validations;
use ES3\Response;
use ES3\Views;

class Servers extends BaseController
{
	public function __initialize()
	{
		SAuth::requireAuth();
	}

	public function index()
	{
		$this->view->menu($this->getMenu())
			->getItem("servers")->active = true;
		$this->view->set([
				"title" => "Servidores",
				"ngCtrSec" => "ctrServers"
			])->setVB([
				"serversList" => SServidores::getInstance()->getServersList()
			]);
		$this->view->breadcrumb()
			->setPageDesc("Servidores")
			->addCrumb([
				"Servidores",
				"Todas os servidores"
			]);
		$this->view->load("servers/grid");
	}

	public function editServerView($IDServer)
	{
		$SServidores = SServidores::getInstance();
		$SAuth = SAuth::getInstance();
		if (!$SAuth->isRootCompany()) redirect("503");
		$userAuth = SAuth::getInstance()->getUserAuth();

		$ServerData = $SServidores->getServerById($IDServer);

		if (!$ServerData) {
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "danger",
				"msg" => "O Servidor solicitado não existe."
			]);
			redirect("servers");
		}

		$FormEditServer = new FormEditServer();
		$FormEditServer->fill($ServerData);

		$this->view->menu($this->getMenu())
			->getItem("servers")->active = true;
		$this->view->set([
				"title" => "Servidor ",
				"ngCtrSec" => "ctrServers",
				"companyData" => $ServerData
		])->setForm($FormEditServer);
		$this->view->breadcrumb()
			->setPageDesc("Servidores")
			->addCrumb([
				["label" => "Servidores", "url" => "/servers"],
				$ServerData->Apelido." (".$ServerData->Endereco.")"
			])->setButtonReturn("/servers");
		$this->view->load("servers/edit");
	}

	public function editServer()
	{
		$SServidores = SServidores::getInstance();
		$FormEditServer = new FormEditServer(false);
		$ValidationHtml = $FormEditServer->validate()->getHtml();

		if ($ValidationHtml) Response::send([
			"status" => false,
			"code" => "invalidinput",
			"msg" => $ValidationHtml
		]);

		$serviceResponse = SServidores::getInstance()->updateServer($FormEditServer->toObject());
		if (!$serviceResponse->status) {
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "danger",
				"msg" => "O Servidor solicitado não existe."
			]);
			redirect("servers");
		}
		$serviceResponse->send();
	}

	public function createServerView()
	{
		$FormCreateServer = new FormCreateServer();
		$this->view->menu($this->getMenu())
			->getItem("servers")->active = true;
		$this->view->set([
				"title" => "Servidor | Cadastrar",
				"ngCtrSec" => "ctrServers"
			])->setForm($FormCreateServer);
		$this->view->breadcrumb()
			->setPageDesc("Cadastrar Servidor")
			->addCrumb([
				["label" => "Servidores", "url" => "/servers"],
				"Cadastrar Servidor"
			])->setButtonReturn("/servers");
		$this->view->load("servers/create");
	}

	public function createServer()
	{
		$FormCreateServer = new FormCreateServer(false);
		$ValidationHtml = $FormCreateServer->validate()->getHtml();

		if ($ValidationHtml) Response::send([
			"status" => false,
			"code" => "invalidinput",
			"msg" => $ValidationHtml
		]);
		
		$serviceResponse = SServidores::getInstance()->createServer($FormCreateServer->toObject());
		
		if ($serviceResponse->status) {
			$serviceResponse->redirect = base_url("/servers/edit/{$serviceResponse->data}");
			$this->session->set_flashdata('Breadcrumb::topAlert', [
				"type" => "success",
				"msg" => "Servidor <b>{$FormCreateServer->Apelido->value}</b> cadastrado com sucesso"
			]);
		}
		$serviceResponse->send();
	}

	public function deleteServer($IDServer)
	{
		$serviceResponse = SServidores::getInstance()->deleteServer($IDServer);
		$serviceResponse->redirect = base_url("/servers");
		$this->session->set_flashdata('Breadcrumb::topAlert', [
			"type" => "success",
			"msg" => "Servidor removido com sucesso"
		]);
		$serviceResponse->send();
	}
}
