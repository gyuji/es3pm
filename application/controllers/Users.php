<?php

namespace ES3\Controllers;

use ES3\Forms\Form;
use ES3\Forms\FormGridEditUser;
use ES3\Forms\FormCreateUser;
use ES3\Services\Usuarios as SUsuarios;
use ES3\Services\Empresas as SEmpresas;
use ES3\Services\Auth as SAuth;
use ES3\Response;
use ES3\Validations;

class Users  extends BaseController
{
    public function __initialize()
	{
		SAuth::requireAuth();
    }
    
    public function index()
    {
        $SAuth = SAuth::getInstance();
        if (!$SAuth->isResale() && !$SAuth->isRootCompany()) redirect("503");
        $userAuth = $SAuth->getUserAuth();
        $rsUsers = SUsuarios::getInstance()->getByCompanyUserAccess();
        $rsEmpresas = SEmpresas::getInstance()->getCompaniesListUserAccess();

        $formGridEditUser = new FormGridEditUser();
        $formGridEditUser->rowsData = $rsUsers;

        $formCreateUser = new FormCreateUser();
        $formCreateUser->IDEmpresas->value = $userAuth->IDEmpresas;

        $this->view->menu($this->getMenu())
            ->getItem("users")->active = true;
		$this->view->breadcrumb()
			->setPageDesc("Usuários")
			->addCrumb([
				"Usuários",
				"Todos os usuários"
			]);
		$this->view->set([
				"title" => "Usuários",
                "ngCtrSec" => "ctrUsers",
                "companiesList" => $rsEmpresas
            ])->setVB([
                "companiesList" => $rsEmpresas
            ])->setForm($formCreateUser)
            ->setForm($formGridEditUser);
		$this->view->load("users/formgrid");
    }

    public function editUser()
    {
        $formGridEditUser = new FormGridEditUser(false);
        $fieldValidation = $formGridEditUser->validate();
        if ($fieldValidation->getHtml()) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "validate" => $fieldValidation->get()
            ]);
        }

        $serviceResponse = SUsuarios::getInstance()->updateUser($formGridEditUser->toObject());

        if ($serviceResponse->status) Response::send([
            "status" => true,
            "code" => "ok",
            "msg" => "Usuário <b>{$formGridEditUser->displayname->value} ({$formGridEditUser->username->value})</b> atualizado com sucesso"
        ]);
        $serviceResponse->send();
    }

    public function createUser()
    {
        $formCreateUser = new FormCreateUser(false);
        $fieldValidation = $formCreateUser->validate()->getHtml();
        if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
        }

        $serviceResponse = SUsuarios::getInstance()->createUser($formCreateUser->toObject());

        if ($serviceResponse->status) Response::send([
            "status" => true,
            "code" => "ok",
            "msg" => "Usuário <b>{$formCreateUser->displayname->value} ({$formCreateUser->username->value})</b> criado com sucesso",
            "data" => $serviceResponse->data
        ]);
        $serviceResponse->send();
    }

    public function deleteUser($IDUser) {
        $serviceResponse = SUsuarios::getInstance()->disableUser($IDUser);
		$serviceResponse->send();
    }
}