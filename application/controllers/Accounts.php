<?php

namespace ES3\Controllers;

use ES3\Controllers\BaseController;
use ES3\Services\Dominios as SDominios;
use ES3\Services\Auth as SAuth;
use ES3\Services\Contas as SContas;
use ES3\Forms\FormUpListAccounts;
use ES3\Forms\FormConfirmAccounts;
use ES3\Forms\FormEditAccounts;
use ES3\Validations;
use ES3\Response;
use ES3\Views;

class Accounts extends BaseController
{
	public function __initialize()
	{
		SAuth::requireAuth();
    }

    public function index()
    {
        $this->view->menu($this->getMenu())
			->getItem("accounts")->active = true;
		$this->view->set([
				"title" => "Contas",
				"ngCtrSec" => "ctrAccounts"
			])->setVB([
				"domainsList" => SDominios::getInstance()->getByCompaniesList()
			])->setForm(new FormEditAccounts());
		$this->view->breadcrumb()
			->setPageDesc("Contas")
			->addCrumb([
				"Contas",
				"Todas as contas"
			]);
		$this->view->load("accounts/grid");
    }

    public function createAccountsUpFileView()
    {
        $this->view->menu($this->getMenu())
			->getItem("accounts")->active = true;
		$this->view->set([
				"title" => "Contas",
				"ngCtrSec" => "ctrAccounts"
			])->setVB([
				"domainsList" => SDominios::getInstance()->getByCompaniesList()
			])->setform(new FormUpListAccounts())
			->setform(new FormConfirmAccounts());
		$this->view->breadcrumb()
			->setPageDesc("Contas")
			->addCrumb([
				["label" => "Contas", "url" => "/accounts"],
				"Cadastrar contas"
			])->setButtonReturn("/accounts");
		$this->view->load("accounts/upfile");
	}
	
	public function createAccountsUpFile()
    {
		$formUpListAccounts = new FormUpListAccounts(false);
		$fieldValidation = $formUpListAccounts->validate()->getHtml();
		if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
		}
		
		Response::send([
			"status" => true,
			"code" => "ok",
			"data" => [
				"accList" => $formUpListAccounts->accList
			]
		]);
	}

	public function createAccountsConfirm()
	{
		$formConfirmAccounts = new FormConfirmAccounts();
		$formConfirmAccounts->setDomains(SDominios::getInstance()->getByCompaniesList());
		$fieldValidation = $formConfirmAccounts->customValidate()->getHtml();
		if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
		}

		$serviseResponse = SContas::getInstance()->createAccounts($formConfirmAccounts->accList);

		if ($serviseResponse->status) Response::send([
            "status" => true,
			"code" => "ok",
			"redirect" => base_url("/accounts"),
            "msg" => "Contas cadastradas com sucesso"
        ]);
        $serviceResponse->send();
	}

	public function getByDomain($IDDomain)
	{
		Response::send([
            "status" => true,
			"code" => "ok",
            "data" => SContas::getInstance()->getByDomain($IDDomain)
        ]);
	}

	public function editAccount()
	{
		$formEditAccounts = new FormEditAccounts(false);
		$fieldValidation = $formEditAccounts->validate()->getHtml();
		if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
		}

		$serviseResponse = SContas::getInstance()->saveAccount($formEditAccounts->toObject());

		if ($serviseResponse->status) Response::send([
            "status" => true,
			"code" => "ok",
            "msg" => "Contas <b>{$formEditAccounts->addrOrig->value}</b> -> <b>{$formEditAccounts->addrDest->value}</b> (#".($formEditAccounts->index->value+1)." foi salvo com sucesso)"
        ]);
        $serviceResponse->send();
	}

	public function deleteAccount($IDAcc)
	{
		$serviceResponse = SContas::getInstance()->disableAccount($IDAcc);
		$serviceResponse->send();
	}
}