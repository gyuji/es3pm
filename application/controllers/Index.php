<?php

namespace ES3\Controllers;

use ES3\Services\Auth as SAuth;
use ES3\Services\Servidores as SServidores;
use ES3\Services\Dominios as SDominios;
use ES3\Services\Usuarios as SUsuarios;
use ES3\Services\Empresas as SEmpresas;
use ES3\Services\Contas as SContas;

class Index extends BaseController
{
	public function __initialize()
	{
		SAuth::requireAuth();
	}

	public function index()
	{
		$this->view->menu($this->getMenu())
			->getItem("dashboard")->active = true;
		$this->view->breadcrumb()
			->setPageDesc("Dashboard");
		$this->view->set([
			"title" => "Dashboard",
			"ngCtrSec" => "ctrDashboard"
		])->setVB([
			"countDomains" => SDominios::getInstance()->countDomains(),
			"countUsers" => SUsuarios::getInstance()->countUsers(),
			"countServer" => SServidores::getInstance()->countServers(),
			"countCompanies" => SEmpresas::getInstance()->countCompanies(),
			"countAccounts" => SContas::getInstance()->countAccounts()
		]);
		$this->view->load("index");
	}
}
