<?php

namespace ES3\Controllers;

use ES3\Forms\Form;
use ES3\Forms\FormGridEditDomain;
use ES3\Forms\FormCreateDomain;
use ES3\Services\Usuarios as SUsuarios;
use ES3\Services\Empresas as SEmpresas;
use ES3\Services\Dominios as SDominios;
use ES3\Services\Auth as SAuth;
use ES3\Response;
use ES3\Validations;

class Domains  extends BaseController
{
    public function __initialize()
	{
		SAuth::requireAuth();
    }

    public function index()
    {
        $SAuth = SAuth::getInstance();
        if (!$SAuth->isRootCompany()) redirect("503");
        $userAuth = $SAuth->getUserAuth();
        $rsEmpresas = SEmpresas::getInstance()->getCompaniesListUserAccess();
        $formCreateDomain = new FormCreateDomain();
        $formCreateDomain->IDEmpresas->value = $userAuth->IDEmpresas;
        $rsDomains = SDominios::getInstance()->getByCompaniesList();

        $this->view->menu($this->getMenu())
            ->getItem("domains")->active = true;
		$this->view->breadcrumb()
			->setPageDesc("Domínios")
			->addCrumb([
				"Domínios",
				"Todos os domínios"
			]);
		$this->view->set([
				"title" => "Domínios",
                "ngCtrSec" => "ctrDomains",
                "companiesList" => $rsEmpresas,
                "domainsData" => $rsDomains
            ])->setVB([
                "companiesList" => $rsEmpresas,
                "domainsData" => $rsDomains
            ])->setForm($formCreateDomain);
		$this->view->load("domains/formgrid");
    }

    public function createDomain()
    {
        $formCreateDomain = new FormCreateDomain(false);
        $fieldValidation = $formCreateDomain->validate()->getHtml();
        if ($fieldValidation) {
            Response::send([
                "status" => false,
                "code" => "invalidinput",
                "msg" => $fieldValidation
            ]);
        }

        $serviceResponse = SDominios::getInstance()->createDomain($formCreateDomain->toObject());

        if ($serviceResponse->status) Response::send([
            "status" => true,
            "code" => "ok",
            "msg" => "Domínio <b>{$formCreateDomain->Endereco->value}</b> criado com sucesso",
            "data" => $serviceResponse->data
        ]);
        $serviceResponse->send();
    }

    public function deleteDomain($IDDomain) {
        $serviceResponse = SDominios::getInstance()->disableDomain($IDDomain);
		$serviceResponse->send();
    }
}