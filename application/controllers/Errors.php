<?php

namespace ES3\Controllers;

use ES3\Utils;
use ES3\Response;
class Errors extends BaseController
{
    public function err404()
    {
        header("HTTP/1.0 404 Not Found");
        if (Utils::getHeader("HTTP_IS_AJAX") && Utils::getHeader("HTTP_ERROR_AJAX")) {
            Response::send([
                "status" => false,
                "code" => "404",
                "msg" => "Página não encontrada"
            ]);
        }
    }
}