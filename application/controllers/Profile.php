<?php

namespace ES3\Controllers;

use ES3\Forms\Form;
use ES3\Forms\FormChangeUserPass;
use ES3\Forms\FormChangeDisplayName;
use ES3\Services\Usuarios as SUsuarios;
use ES3\Services\Auth as SAuth;
use ES3\Response;
use ES3\Validations;

class Profile extends BaseController
{
    public function __initialize()
	{
		SAuth::requireAuth();
    }
    
    public function index()
    {
		$FormChangeDisplayName = new FormChangeDisplayName();
		$FormChangeDisplayName->displayname->value = SAuth::getInstance()->getUserAuth("Nome");

		$this->view->menu($this->getMenu());
		$this->view->breadcrumb()
			->setPageDesc("Meu Perfil")
			->addCrumb([
				"Meu perfil",
				SAuth::getInstance()->getUserAuth("Nome")
			]);
		$this->view->set([
				"title" => "Perfil do usuário",
				"ngCtrSec" => "ctrProfile"
			])
			->setForm(new FormChangeUserPass())
			->setForm($FormChangeDisplayName);
		$this->view->load("userprofile");
	}
	
	public function changePassword()
	{
		$FormChangeUserPass = new FormChangeUserPass(false);
        $password2 = $FormChangeUserPass->password2;
		$fieldValidation = $FormChangeUserPass->validate();
		
		// Verifica se os dados vieram do formulários
        if ($fieldValidation->get("password1")->code == "required" || 
            $fieldValidation->get("password2")->code == "required") Response::send([
            "status" => false,
            "code" => "missingparams",
            "msg" => $fieldValidation->getHtml()
		]);
		
		if ($fieldValidation->get("password2")->code == "notequal") Response::send([
            "status" => false,
            "code" => "invalidpass",
            "msg" => $fieldValidation->getHtml()
		]);

		// Salva a nova senha
		$SAuth = SAuth::getInstance();
		if ($SAuth->changePassword($password2->value)) {
			Response::send([
				"status" => true,
				"code" => "ok",
				"msg" => "Senha salva com sucesso"
			]);
		} else {
			Response::send([
				"status" => false,
				"code" => "erroronsave",
				"msg" => "Erro ao salvar senha"
			]);
		}
	}

	public function changeDisplayName()
	{
		$FormChangeDisplayName = new FormChangeDisplayName(false);
        $displayname = $FormChangeDisplayName->displayname;
		$fieldValidation = $FormChangeDisplayName->validate();

		// Verifica se os dados vieram do formulários
        if ($fieldValidation->get("displayname")->code == "required") Response::send([
            "status" => false,
            "code" => "missingparams",
            "msg" => $fieldValidation->getHtml()
		]);

		$SAuth = SAuth::getInstance();
		if ($SAuth->changeDisplayName($displayname->value)) {
			Response::send([
				"status" => true,
				"code" => "ok",
				"msg" => "Nome de exibição salvo com sucesso"
			]);
		} else {
			Response::send([
				"status" => false,
				"code" => "erroronsave",
				"msg" => "Erro ao salvar nome de exibição"
			]);
		}
	}
}