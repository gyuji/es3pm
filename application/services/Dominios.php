<?php

namespace ES3\Services;

use ES3\Services\Auth as SAuth;
use ES3\Services\Empresas as SEmpresas;
use ES3\Utils;
use ES3\Response;

class Dominios extends BaseService 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($ID)
    {
        return $this->CI->Dominios->getByID($ID);
    }

    public function getByCompaniesList()
    {
        $rsEmpresas = SEmpresas::getInstance()->getCompaniesListUserAccess();
        $listCIDs = [];
        foreach ($rsEmpresas as $row) {
            $listCIDs[] = $row->ID;
        }
        if (!$listCIDs) return [];
        return $this->CI->Dominios->getByCompaniesList($listCIDs);
    }

    public function createDomain(\stdClass $formData)
    {
        // Verifica se o domínios já existe
        if ($this->CI->Dominios->getByDomain($formData->Endereco, true)) {
            return new Response([
                "status" => false,
                "code" => "endeco::exists",
                "msg" => "O endereço <b>{$formData->Endereco}</b> já existe"
            ]);
        }

        // Verifica se o usuário tem permissão de cadastrar domínio para essa empresa
        if (!SEmpresas::getInstance()->isPermitedAccessCompany($formData->IDEmpresas)) {
            return new Response([
                "status" => false,
                "code" => "cid::notpermitted",
                "msg" => "Você não tem permissão para criar domínios para esta empresa"
            ]);
        }

        // Prepara os dados para atualização no banco de dados
        $entityDominos = (object)[
            "IDEmpresas" => $formData->IDEmpresas,
            "Endereco" => $formData->Endereco,
            "CriadoEm" => Utils::now(),
            "Ativo" => true
        ];

        if (!$this->CI->Dominios->createDomain($entityDominos)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        $entityDominos->ID = $this->CI->Dominios->getInsertID();
        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Domínio <b>{$formData->Endereco}</b> cadastrado com sucesso",
            "data" => [
                "domainData" => $entityDominos
            ] 
        ]);
    }

    public function disableDomain($IDDomain)
    {
        // Verifica se o domínio existe
        $rsDominios = $this->CI->Dominios->getByID($IDDomain);
        if (!$rsDominios) return new Response([
            "status" => false,
            "code" => "id::unexists",
            "msg" => "O domínio de ID $IDDomain não existe"
        ]);

        // Verifica se o usuário pode editar esse CID
        $IDEmpresas = SAuth::getInstance()->getUserAuth("IDEmpresas");
        if (!SEmpresas::getInstance()->isPermitedAccessCompany($IDEmpresas)) return new Response([
            "status" => false,
            "code" => "cid::notpermitted",
            "msg" => "Você não tem permissão para remover um domínio da empresa de CID {$IDEmpresas}"
        ]);

        if (!$this->CI->Dominios->disableDomain($IDDomain)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Domínio {$rsDominios->Endereco} removido com sucesso"
        ]);
    }

    public function countDomains()
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        return $this->CI->Dominios->countDomains();
    }
}