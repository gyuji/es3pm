<?php

namespace ES3\Services;

use ES3\Utils;
use ES3\Services\Auth as SAuth;
use ES3\Services\Empresas as SEmpresas;
use ES3\Response;

class Usuarios extends BaseService 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function savePassword($ID, $password)
	{
		return $this->CI->Usuarios->savePassword($ID, SAuth::hash($password));
    }
    
    public function saveDisplayName($ID, $displayname)
    {
        return $this->CI->Usuarios->saveDisplayName($ID, $displayname);
    }

    public function getByCompany($IDCompany, bool $Ativo = null)
    {
        return $this->CI->Usuarios->getByCompany($IDCompany, $Ativo);
    }

    public function getByCompanyUserAccess()
    {
        $rsEmpresas = SEmpresas::getInstance()->getCompaniesListUserAccess();
        $listCIDs = [];
        foreach ($rsEmpresas as $row) {
            $listCIDs[] = $row->ID;
        }
        if (!$listCIDs) return [];
        return $this->CI->Usuarios->getByCompaniesList($listCIDs); 
    }

    public function updateUser(\stdClass $formData)
    {

        // Verifica se o usuário tem permissão editar esse usuário
        if (!SEmpresas::getInstance()->isPermitedAccessCompany($formData->IDEmpresas)) {
            return new Response([
                "status" => false,
                "code" => "cid::notpermitted",
                "msg" => "Você não tem permissão para editar esse usuário"
            ]);
        }

        // Verifica se o endereço de e-mail já existe
        if ($this->CI->Usuarios->existsEmail($formData->username, $formData->ID, true)) {
            return new Response([
                "status" => false,
                "code" => "username::exists",
                "msg" => "O endereço de e-mail <b>{$formData->username}</b> já existe"
            ]);
        }

        // Prepara os dados para atualização no banco de dados
        $entityUsuario = (object)[
            "IDEmpresas" => $formData->IDEmpresas,
            "Nome" => $formData->displayname,
            "Email" => $formData->username,
            "EditadoEm" => Utils::now()
        ];
        if ($formData->password2) $entityUsuario->Senha = SAuth::hash($formData->password2);
        if (!$this->CI->Usuarios->updateUser($formData->ID, $entityUsuario)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Usuário <b>{$formData->displayname} ({$formData->username})</b> salva com sucesso"
        ]);
    }

    public function createUser(\stdClass $formData)
    {
        // Verifica se o usuário tem permissão editar esse usuário
        if (!SEmpresas::getInstance()->isPermitedAccessCompany($formData->IDEmpresas)) {
            return new Response([
                "status" => false,
                "code" => "cid::notpermitted",
                "msg" => "Você não tem permissão para criar usuários para esta empresa"
            ]);
        }

        // Verifica se o endereço de e-mail já existe
        if ($this->CI->Usuarios->existsEmail($formData->username, null, true)) {
            return new Response([
                "status" => false,
                "code" => "username::exists",
                "msg" => "O endereço de e-mail <b>{$formData->username}</b> já existe"
            ]);
        }

        // Prepara os dados para atualização no banco de dados
        $entityUsuario = (object)[
            "IDEmpresas" => $formData->IDEmpresas,
            "Nome" => $formData->displayname,
            "Email" => $formData->username,
            "Senha" => SAuth::hash($formData->password2),
            "CriadoEm" => Utils::now(),
            "Ativo" => true
        ];

        if (!$this->CI->Usuarios->createUser($entityUsuario)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }
        unset($entityUsuario->Senha);
        $entityUsuario->ID = $this->CI->Usuarios->getInsertID();
        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Usuário <b>{$formData->displayname} ({$formData->username})</b> cadastrado com sucesso",
            "data" => [
                "userData" => $entityUsuario
            ] 
        ]);
    }

    public function disableUser($IDUser)
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        if ($IDUser == $userAuth->ID) return new Response([
            "status" => false,
            "code" => "id::ownid",
            "msg" => "Não é permitido remover o seu próprio usuário"
        ]);

        // Verifica se o CID existe
        $rsUser = $this->CI->Usuarios->getByID($IDUser);
        if (!$rsUser) return new Response([
            "status" => false,
            "code" => "id::unexists",
            "msg" => "O usuário de ID $IDUser não existe"
        ]);

        // Verifica se o usuário pode editar esse CID
        if (!SEmpresas::getInstance()->isPermitedAccessCompany($rsUser->IDEmpresas)) return new Response([
            "status" => false,
            "code" => "cid::notpermitted",
            "msg" => "Você não tem permissão para remover um usuário da empresa de CID {$rsUser->IDEmpresas}"
        ]);

        if (!$this->CI->Usuarios->disableUser($IDUser)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Usuário {$rsUser->Nome} ({$rsUser->Email}) removido com sucesso"
        ]);
    }

    public function countUsers()
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        return $this->CI->Usuarios->countUsers();
    }

}
