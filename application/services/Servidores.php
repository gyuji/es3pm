<?php

namespace ES3\Services;

use ES3\Services\Auth as SAuth;
use ES3\Utils;
use ES3\Response;

class Servidores extends BaseService 
{
    public function __construct()
    {
        parent::__construct();
    }

   
    public function getServersList()
    {
        return $this->CI->Servidores->getListServers();
    }

    public function getServerById($IDServer)
    {
        return $this->CI->Servidores->getByID($IDServer);
    }

    public function createServer(\stdClass $formData): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        // Verifica se o Endereço existe
        if ($this->CI->Servidores->existsEndereco($formData->Endereco, true)) return new Response([
            "status" => false,
            "code" => "endereco::exists",
            "msg" => "O Endereço inserido já existe"
        ]);


        // Prepara os dados para inserção no banco de dados
        $entityServidor = (object)[
            "Apelido" => $formData->Apelido,
            "Endereco" => $formData->Endereco,
            "Porta" => $formData->Porta,
            "Seguranca" => $formData->Seguranca,
            "TemplateMapeamento" => $formData->Map,
            "CriadoEm" => Utils::now(),
            "Ativo" => true
        ];
        if (!$this->CI->Servidores->insertServer($entityServidor)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }
        return new Response([
            "data" => $this->CI->Servidores->getInsertID(),
            "status" => true,
            "code" => "ok",
            "msg" => "Empresa cadastrada com sucesso"
        ]);
    }


    public function updateServer(\stdClass $formData): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();

        // Prepara os dados para inserção no banco de dados
        $entityServidor = (object)[
            "Apelido" => $formData->Apelido,
            // "Endereco" => $formData->Endereco,
            // "Porta" => $formData->Porta,
            "Seguranca" => $formData->Seguranca,
            "TemplateMapeamento" => $formData->Map,
            "EditadoEm" => Utils::now()
        ];

        if (!$this->CI->Servidores->updateServer($formData->ID, $entityServidor)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Servidor salvo com sucesso"
        ]);
    }

    public function deleteServer($IDServer): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();

        if (!$this->CI->Servidores->deleteServer($IDServer)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }
        
        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Feito"
        ]);
    }

    public function countServers()
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        return $this->CI->Servidores->countServers();
    }
}
