<?php

namespace ES3\Services;

use ES3\Services\Auth as SAuth;
use ES3\Utils;
use ES3\Response;

class Contas extends BaseService 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function createAccounts(array $accList): Response
    {
        if (!$accList) return new Response(["status" => false, "code" => "missing"]);
        $hasError = false;
        foreach ($accList as &$acc) {
            $objAccount = (object)[
                "IDDominios" => $acc->domain->ID,
                "EndOrigem" => $acc->addrOrig,
                "SenhaOrigem" => $acc->passOrig,
                "EndDestino" => $acc->addrDest,
                "SenhaDestino" => $acc->passOrig,
                "CriadoEm" => Utils::now(),
                "Ativo" => true
            ];
            if (!$this->CI->Contas->insertAccounts($objAccount)) $hasError = true;
        }
        if ($hasError) {
            return new Response([
                "status" => false, 
                "code" => "notsaveall",
                "msg" => "Não foi possível salvar todas as contas, por favor tente novamente"
            ]);
        }
        return new Response([
            "status" => true
        ]);
    }

    public function getByDomain($IDDomain)
    {
        return $this->CI->Contas->getByDomain($IDDomain);
    }

    public function saveAccount(\stdClass $formData)
    {
        $objAccount = (object)[
            "SenhaOrigem" => $formData->passOrig,
            "SenhaDestino" => $formData->passDest,
            "EditadoEm" => Utils::now()
        ];
        return new Response([
            "status" => $this->CI->Contas->saveAccount($formData->ID, $objAccount),
        ]);
    }

    public function disableAccount($IDAcc)
    {
        // Verifica se o CID existe
        $rsAcc = $this->CI->Contas->getByID($IDAcc);
        if (!$rsAcc) return new Response([
            "status" => false,
            "code" => "id::unexists",
            "msg" => "O usuário de ID $IDAcc não existe"
        ]);

        if (!$this->CI->Contas->disableAccount($IDAcc)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Conta {$rsAcc->EndOrigem} -> {$rsAcc->EndDestino} removido com sucesso"
        ]);
    }
    
    public function countAccounts()
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        return $this->CI->Contas->countAccounts();
    }
}