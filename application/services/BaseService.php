<?php

namespace ES3\Services;

class BaseService 
{
    static protected $instance = [];
    public $CI;
    
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    static public function &getInstance()
    {
        $class = get_called_class();
        if (!isset(self::$instance[$class])) self::$instance[$class] = new $class();
        return self::$instance[$class];
    }
}
