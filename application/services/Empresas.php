<?php

namespace ES3\Services;

use ES3\Services\Auth as SAuth;
use ES3\Utils;
use ES3\Response;

class Empresas extends BaseService 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCompanyByID($ID)
    {
        return $this->CI->Empresas->getByID($ID);
    }

    public function isPermitedAccessCompany($ID)
    {
        $userCompany = SAuth::getInstance()->getUserAuth("Empresas")->ID;
        return $this->CI->Empresas->isPermitedAccessCompany($userCompany, $ID);
    }

    public function isPermitedAccessResale($ID)
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        $userCompany = $userAuth->IDEmpresas;
        if ($userAuth->Empresas->Tipo == $this->CI->Empresas::TIPO_REVENDA && $ID == $userCompany) return true;
        return $this->CI->Empresas->isPermitedAccessResale($userCompany, $ID);
    }

    public function getCompaniesListUserAccess()
    {
        $userCompany = SAuth::getInstance()->getUserAuth("Empresas")->ID;
        $rs = $this->CI->Empresas->getTreeCompaniesById($userCompany);
        ksort($rs);
        return $rs;
    }

    public function getResaleListUserAccess()
    {
        if (SAuth::getInstance()->getUserAuth("Empresas")->Tipo != $this->CI->Empresas::TIPO_REVENDA) return [];
        $userCompany = SAuth::getInstance()->getUserAuth("Empresas")->ID;
        return $this->CI->Empresas->getTreeResaleById($userCompany);
    }

    public function getAllocatedExecutionLimit($IDEmpresas)
    {
        return $this->CI->Empresas->getAllocatedExecutionLimit($IDEmpresas);
    }

    public function createCompany(\stdClass $formData): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        // Verifica se o CID existe
        if ($this->CI->Empresas->existsCID($formData->cid)) return new Response([
            "status" => false,
            "code" => "cid::exists",
            "msg" => "O CID inserido já existe"
        ]);

        // Verifica se o CNPF/CPF existe
        $formData->CnpjCpf = $formData->PForPJ == "PF" ? $formData->Cpf : $formData->Cnpj;
        if ($this->CI->Empresas->existsCnpjCpf($formData->CnpjCpf)) return new Response([
            "status" => false,
            "code" => "cnpjcpf::exists",
            "msg" => "O ".($formData->PForPJ == "PF" ? "CPF" : "CNPJ")." inserido já existe"
        ]);

        // Verifica se a razão social já existe
        if ($this->CI->Empresas->existsRazaoSocial($formData->RazaoSocial)) return new Response([
            "status" => false,
            "code" => "razaosocial::exists",
            "msg" => "A razão social inserida já existe"
        ]);

        // Verifica se o usuário logado pode cadastrar para a revenda
        if (!$this->isPermitedAccessResale($formData->IDRevenda)) return new Response([
            "status" => false,
            "code" => "revenda::notaccess",
            "msg" => "Empresa revenda inválida"
        ]);
        $Revenda = $this->CI->Empresas->getByID($formData->IDRevenda);

        // Verifica o limite de execuções
        $allocationDisp = $this->getAllocationExecutionDisp($Revenda);
        if ($formData->LimiteExecucoes > 0 && $allocationDisp < $formData->LimiteExecucoes) {
            return new Response([
                "status" => false,
                "code" => "limiteexecucoes::exceded",
                "msg" => ($allocationDisp > 0 ? "O limite de execuções disponível para alocação é de <b>$allocationDisp</b>." :
                    "Não há mais disponibilidade de alocação de execuções para a empresa <b>CID{$Revenda->ID} {$Revenda->RazaoSocial}</b>.<br>
                    Para aumentar a disponibilidade você pode realocar de outras empresas clientes ou contatar o administrador do sistema 
                    solicitando aumento do limite.").
                    "<hide-data name=\"excededLimite\">allocationDisp=$allocationDisp|exceded=".($formData->LimiteExecucoes - $allocationDisp)."</hide-data>"
            ]);
        }

        // Prepara os dados para inserção no banco de dados
        $entityEmpresa = (object)[
            "ID" => $formData->cid,
            "IDEmpresasRevenda" => $formData->IDRevenda,
            "CnpjCpf" => $formData->CnpjCpf,
            "RazaoSocial" => $formData->RazaoSocial,
            "Tipo" => $formData->Tipo,
            "LimiteExecucoes" => $formData->LimiteExecucoes,
            "LimiteRepeticoes" => $formData->LimiteRepeticoes,
            "CriadoEm" => Utils::now(),
            "Ativo" => true
        ];
        if (!$this->CI->Empresas->insertCompany($entityEmpresa)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }
        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Empresa cadastrada com sucesso"
        ]);
    }

    public function getAllocationExecutionDisp(\stdClass $Empresa)
    {
        $allocatedLimit = $this->getAllocatedExecutionLimit($Empresa->ID);
        return $Empresa->LimiteExecucoes - $allocatedLimit;
    }

    public function getAllocationRepetationDisp(\stdClass $Empresa)
    {
        return $Empresa->LimiteRepeticoes;
    }

    public function updateCompany(\stdClass $formData): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        // Verifica se o CID existe
        if (!$this->CI->Empresas->existsCID($formData->cid)) return new Response([
            "status" => false,
            "code" => "cid::unexists",
            "msg" => "O CID inserido não existe"
        ]);

        // Verifica se o usuário pode editar esse CID
        if (!$this->isPermitedAccessCompany($formData->cid)) return new Response([
            "status" => false,
            "code" => "cid::notpermitted",
            "msg" => "Você não tem permissão para editar essa empresa"
        ]);

        // Verifica se o CNPF/CPF existe
        $formData->CnpjCpf = $formData->PForPJ == "PF" ? $formData->Cpf : $formData->Cnpj;
        if ($this->CI->Empresas->existsCnpjCpf($formData->CnpjCpf, $formData->cid)) return new Response([
            "status" => false,
            "code" => "cnpjcpf::exists",
            "msg" => "O ".($formData->PForPJ == "PF" ? "CPF" : "CNPJ")." inserido já existe"
        ]);
        

        // Verifica se a Razão social existe
        if ($this->CI->Empresas->existsRazaoSocial($formData->RazaoSocial, $formData->cid)) return new Response([
            "status" => false,
            "code" => "razaosocial::exists",
            "msg" => "A razão social inserida já existe"
        ]);

        // Verifica se o usuário logado pode cadastrar para a revenda
        if (!$this->isPermitedAccessResale($formData->IDRevenda)) return new Response([
            "status" => false,
            "code" => "revenda::notaccess",
            "msg" => "Empresa revenda inválida"
        ]);
        $Revenda = $this->CI->Empresas->getByID($formData->IDRevenda);

        // Verifica o limite de execuções
        $allocationDisp = $this->getAllocationExecutionDisp($Revenda);
        if ($formData->LimiteExecucoes > 0 && $allocationDisp < $formData->LimiteExecucoes) {
            return new Response([
                "status" => false,
                "code" => "limiteexecucoes::exceded",
                "msg" => ($allocationDisp > 0 ? "O limite de execuções disponível para alocação é de <b>$allocationDisp</b>." :
                    "Não há mais disponibilidade de alocação de execuções para a empresa <b>CID{$Revenda->ID} {$Revenda->RazaoSocial}</b>.<br>
                    Para aumentar a disponibilidade você pode realocar de outras empresas clientes ou contatar o administrador do sistema 
                    solicitando aumento do limite.").
                    "<hide-data name=\"excededLimite\">allocationDisp=$allocationDisp|exceded=".($formData->LimiteExecucoes - $allocationDisp)."</hide-data>"
            ]);
        }

        // Prepara os dados para inserção no banco de dados
        $entityEmpresa = (object)[
            "IDEmpresasRevenda" => $formData->IDRevenda,
            "CnpjCpf" => $formData->CnpjCpf,
            "RazaoSocial" => $formData->RazaoSocial,
            "Tipo" => $formData->Tipo,
            "LimiteExecucoes" => $formData->LimiteExecucoes,
            "LimiteRepeticoes" => $formData->LimiteRepeticoes,
            "EditadoEm" => Utils::now()
        ];
        if (!$this->CI->Empresas->updateCompany($formData->cid, $entityEmpresa)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Empresa salva com sucesso"
        ]);
    }

    public function disableCompany($IDCompany): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        if ($IDCompany == $userAuth->IDEmpresas) return new Response([
            "status" => false,
            "code" => "cid::owncid",
            "msg" => "Não é permitido desabilitar a própria empresa"
        ]);

        // Verifica se o CID existe
        if (!$this->CI->Empresas->existsCID($IDCompany)) return new Response([
            "status" => false,
            "code" => "cid::unexists",
            "msg" => "A empresa de CID $IDCompany não existe"
        ]);

        // Verifica se o usuário pode editar esse CID
        if (!$this->isPermitedAccessCompany($IDCompany)) return new Response([
            "status" => false,
            "code" => "cid::notpermitted",
            "msg" => "Você não tem permissão para desabilitar a empresa de CID $IDCompany"
        ]);

        if (!$this->CI->Empresas->disableCompany($IDCompany)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Empresa desabilitada com sucesso"
        ]);
    }

    public function enableCompany($IDCompany): Response
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        if ($IDCompany == $userAuth->IDEmpresas) return new Response([
            "status" => false,
            "code" => "cid::owncid",
            "msg" => "Não é permitido habilitar a própria empresa"
        ]);

        // Verifica se o CID existe
        if (!$this->CI->Empresas->existsCID($IDCompany)) return new Response([
            "status" => false,
            "code" => "cid::unexists",
            "msg" => "A empresa de CID $IDCompany não existe"
        ]);

        // Verifica se o usuário pode editar esse CID
        if (!$this->isPermitedAccessCompany($IDCompany)) return new Response([
            "status" => false,
            "code" => "cid::notpermitted",
            "msg" => "Você não tem permissão para habilitar a empresa de CID $IDCompany"
        ]);

        if (!$this->CI->Empresas->enableCompany($IDCompany)) {
            return new Response([
                "status" => false,
                "code" => "errorinsert",
                "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
            ]);
        }

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Empresa habilitada com sucesso"
        ]);
    }
    
    public function countCompanies()
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        return $this->CI->Empresas->countCompanies();
    }

}
