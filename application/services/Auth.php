<?php

namespace ES3\Services;

use ES3\Utils;
use ES3\Services\Usuarios as SUsuarios;
use ES3\Response;

class Auth extends BaseService 
{
    private $userAuth;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function doLogin(String $username, String $password)
    {
        $rsUsuario = $this->CI->Usuarios->getByEmail($username);
        if (!$rsUsuario) return false;
        $rsUsuario = $rsUsuario[0];
        if ($rsUsuario->Empresas && !self::verifyHash($password, $rsUsuario->Senha)) return false;
        $this->userAuth = (object)$rsUsuario;
        $this->CI->session->set_userdata("userAuth", $rsUsuario);
        return true;
    }

    public function getUserAuth(String $property = null)
    {
        return $property ? ($this->userAuth->$property ?? null) : $this->userAuth;
    }

    public function destroy()
    {
        $this->CI->session->sess_destroy();
        self::requireAuth();
    }

    public function isRootCompany()
    {
        return $this->userAuth->Empresas->ID == 1;
    }

    public function isResale()
    {
        return $this->userAuth->Empresas->Tipo == \ES3\Models\Empresas::TIPO_REVENDA;
    }

    public function changePassword(String $password)
    {
        return SUsuarios::getInstance()->savePassword($this->userAuth->ID, $password);
    }

    public function changeDisplayName(String $displayname)
    {
        $saveStatus = SUsuarios::getInstance()->saveDisplayName($this->userAuth->ID, $displayname);
        if ($saveStatus) $this->userAuth->Nome = $displayname;
        return $saveStatus;
    }

    static public function hash(String $str)
    {
        return password_hash($str, PASSWORD_DEFAULT);
    }

    static public function verifyHash(String $str, String $hash)
    {
        return password_verify($str, $hash);
    }

    static public function isAuth()
    {
        if (get_instance()->session->has_userdata("userAuth")) {
            self::getInstance()->userAuth = (object)get_instance()->session->userdata("userAuth");
            return true;
        }
        return false;
    }

    static public function requireAuth()
    {
        if (!self::isAuth()) {
            if (Utils::getHeader("HTTP_IS_AJAX") == 1) {
                Response::send(["redirect" => base_url("/auth/login")]);
            } else {
                redirect("/auth/login", "location");
            }
        }
    }

    static public function redirectIsAuth()
    {
        if (self::isAuth()) {
            redirect("/", "location");
        }
    }
}