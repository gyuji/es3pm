<?php

namespace ES3\Services;

use ES3\Services\Auth as SAuth;
use ES3\Services\Empresas as SEmpresas;
use ES3\Services\Dominios as SDominios;
use ES3\Models\Jobs as MJobs;
use ES3\Utils;
use ES3\Response;

class Jobs extends BaseService 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($ID)
    {
        return $this->CI->Jobs->getByID($ID);
    }

    public function getAllByCompany()
    {
        $userAuth = SAuth::getInstance()->getUserAuth();
        return $this->CI->Jobs->getByCompany($userAuth->IDEmpresas);
    }

    public function createJob(\stdClass $formData)
    {
        $rsEmpresa = SEmpresas::getInstance()->getCompanyByID($formData->IDEmpresas);

        // Verifica se o limite de execucoes é permitido
        $executionAllocated = $this->getExecutionAllocated($formData->IDEmpresas);
        if ($rsEmpresa->LimiteExecucoes - $executionAllocated < $formData->Execucoes) return new Response([
            "status" => false,
            "code" => "execucao::excededallocated",
            "msg" => "O limite de execuções inserido excede o disponível da empresa. Limite disponível: <b>".($rsEmpresa->LimiteExecucoes - $executionAllocated)."</b>"
        ]);

        // Verifica se já tem um job para esse domínio
        $rsDominio = SDominios::getInstance()->getByID($formData->IDDominios);
        $rsJobsOfDomain = $this->CI->Jobs->existsJobsActiveOfDomain($formData->IDDominios);
        if ($rsJobsOfDomain) return new Response([
            "status" => false,
            "code" => "domain::existsjob",
            "msg" => "Já existe um job para o domínio <b>{$rsDominio->Endereco}</b>"
        ]);

        $objJobs = (object)[
            "IDEmpresas" => $formData->IDEmpresas,
            "IDDominios" => $formData->IDDominios,
            "IDServidores" => $formData->IDServidores,
            "Execucoes" => $formData->Execucoes,
            "Repeticoes" => $formData->Repeticoes,
            "Status" => MJobs::AGUARDANDO,
            "Ativo" => true,
            "CriadoEm" => Utils::now()
        ];
        
        if (!$this->CI->Jobs->createJobs($objJobs)) return new Response([
            "status" => false,
            "code" => "errorinsert",
            "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
        ]);

        $lastID = $this->CI->Servidores->getInsertID();
        return new Response([
            "data" => $lastID,
            "status" => true,
            "code" => "ok",
            "msg" => "Job #<b>{$lastID}</b> cadastrado com sucesso"
        ]);
    }

    public function updateJob(\stdClass $formData)
    {
        $rsEmpresa = SEmpresas::getInstance()->getCompanyByID($formData->IDEmpresas);
        $rsJobs = $this->getByID($formData->ID);
        if (!$rsJobs) {
            return new Response([
                "status" => false,
                "code" => "id::unexists",
                "msg" => "O job #{$formData->ID} não foi encontrado para edição"
            ]);
        }

        if ($formData->Status != MJobs::AGUARDANDO) {
            return new Response([
                "status" => false,
                "code" => "status::notstatuscorrect",
                "msg" => "O job deve estar no status de <b>Aguardando</b> para ser editado"
            ]);
        }

        // Verifica se o limite de execucoes é permitido
        $executionAllocated = $this->getExecutionAllocated($formData->IDEmpresas, $formData->ID);
        if ($rsEmpresa->LimiteExecucoes - $executionAllocated < $formData->Execucoes) return new Response([
            "status" => false,
            "code" => "execucao::excededallocated",
            "msg" => "O limite de execuções inserido excede o disponível da empresa. Limite disponível: <b>".($rsEmpresa->LimiteExecucoes - $executionAllocated)."</b>"
        ]);

        // Verifica se já tem um job para esse domínio
        $rsDominio = SDominios::getInstance()->getByID($formData->IDDominios);
        $rsJobsOfDomain = $this->CI->Jobs->existsJobsActiveOfDomain($formData->IDDominios, $formData->ID);
        if ($rsJobsOfDomain) return new Response([
            "status" => false,
            "code" => "domain::existsjob",
            "msg" => "Já existe um job para o domínio <b>{$rsDominio->Endereco}</b>"
        ]);

        $objJobs = (object)[
            "Execucoes" => $formData->Execucoes,
            "Repeticoes" => $formData->Repeticoes,
            "EditadoEm" => Utils::now()
        ];
        
        if (!$this->CI->Jobs->updateJobs($formData->ID, $objJobs)) return new Response([
            "status" => false,
            "code" => "errorinsert",
            "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
        ]);

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Job #<b>{$formData->ID}</b> editado com sucesso"
        ]);
    }

    public function getExecutionAllocated($IDEmpresas, $diffID = null)
    {
        return $this->CI->Jobs->getCountExecutionByCompany($IDEmpresas, $diffID);
    }

    public function disableJob($ID)
    {
        $rsJobs = $this->getByID($ID);
        if (!$rsJobs) {
            return new Response([
                "status" => false,
                "code" => "id::unexists",
                "msg" => "O job #{$ID} não foi encontrado para remoção"
            ]);
        }

        if ($rsJobs->Status != MJobs::AGUARDANDO) {
            return new Response([
                "status" => false,
                "code" => "status::notstatuscorrect",
                "msg" => "O job deve estar no status de Aguardando para ser removido"
            ]);
        }

        if (!$this->CI->Jobs->disableJobs($rsJobs->ID)) return new Response([
            "status" => false,
            "code" => "errorinsert",
            "msg" => "Houve um erro durante o processo, por favor contate o administrador do sistema."
        ]);

        return new Response([
            "status" => true,
            "code" => "ok",
            "msg" => "Job #{$rsJobs->ID} removido com sucesso"
        ]);
    }
}
