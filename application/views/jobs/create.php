<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="m-t" name="formCreateJob.form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?=$IDEmpresas->label?></label>
                                    <select class="form-control" ng-model="formCreateJob.data.IDEmpresas.value" <?php $IDEmpresas->html() ?>>
                                        <option ng-repeat="company in _VB.EmpresasList" ng-value="company.ID" ng-if="company.Ativo">CID{{company.ID}} - {{company.RazaoSocial}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?=$IDDominios->label?></label>
                                    <select class="form-control" ng-model="formCreateJob.data.IDDominios.value" <?php $IDDominios->html() ?>>
                                        <option ng-repeat="domain in _VB.DominiosList" ng-value="domain.ID" ng-if="domain.IDEmpresas == formCreateJob.data.IDEmpresas.value">{{domain.Endereco}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?=$IDServidores->label?></label>
                                    <select class="form-control" ng-model="formCreateJob.data.IDServidores.value" <?php $IDServidores->html() ?>>
                                        <option ng-repeat="server in _VB.ServidoresList" ng-value="server.ID">{{server.Apelido}} - {{server.Endereco}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?=$Execucoes->label?> {{formCreateJob.data.IDEmpresas.value ? 'máx '+_VB.EmpresasList[formCreateJob.data.IDEmpresas.value].LimiteExecucoes : null}}</label>
                                    <input class="form-control" ng-model="formCreateJob.data.Execucoes.value"
                                        <?php $Execucoes->html() ?> max="{{_VB.EmpresasList[formCreateJob.data.IDEmpresas.value].LimiteExecucoes}}">
                                </div>
                                <div class="form-group">
                                    <label><?=$Repeticoes->label?> {{formCreateJob.data.IDEmpresas.value ? 'máx '+_VB.EmpresasList[formCreateJob.data.IDEmpresas.value].LimiteRepeticoes : null}}</label>
                                    <input class="form-control" ng-model="formCreateJob.data.Repeticoes.value"
                                        <?php $Repeticoes->html() ?> max="{{_VB.EmpresasList[formCreateJob.data.IDEmpresas.value].LimiteRepeticoes}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                            <button class="btn btn-primary m-b" ng-click="formCreateJob.onsubmit()"
                                ng-disabled="!formCreateJob.form.$valid || formCreateJob.sending"
                                ng-bind="formCreateJob.sending ? 'Salvando...' : 'Salvar'">
                            </button>
                            <es3-alert alertdata="formCreateJob.alert"></es3-alert>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $thisView->includes("footer") ?>  