<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="m-t" name="formEditJob.form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?=$IDEmpresas->label?></label>: CID{{_VB.jobData.Empresas.ID}} - {{_VB.jobData.Empresas.RazaoSocial}}
                                </div>
                                <div class="form-group">
                                    <label><?=$IDDominios->label?></label>: {{_VB.jobData.Dominios.Endereco}}
                                </div>
                                <div class="form-group">
                                    <label><?=$IDServidores->label?></label>: {{_VB.jobData.Servidores.Apelido}} - {{_VB.jobData.Servidores.Endereco}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?=$Execucoes->label?> {{_VB.jobData.Empresas.LimiteExecucoes}}</label>
                                    <input class="form-control" ng-model="formEditJob.data.Execucoes.value"
                                        <?php $Execucoes->html() ?> max="{{_VB.jobData.Empresas.LimiteExecucoes}}">
                                </div>
                                <div class="form-group">
                                    <label><?=$Repeticoes->label?> {{_VB.jobData.Empresas.LimiteRepeticoes}}</label>
                                    <input class="form-control" ng-model="formEditJob.data.Repeticoes.value"
                                        <?php $Repeticoes->html() ?> max="{{_VB.jobData.Empresas.LimiteRepeticoes}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php if ($formEditJob->btnSubmit): ?>
                                <button class="btn btn-danger m-b" ng-click="formEditJob.confirmDelete()"
                                    ng-disabled="!formEditJob.form.$valid || formEditJob.deleting"
                                    ng-bind="formEditJob.deleting ? 'Removendo...' : 'Remover'">
                                </button>
                                <button class="btn btn-primary m-b pull-right" ng-click="formEditJob.onsubmit()"
                                    ng-disabled="!formEditJob.form.$valid || formEditJob.sending"
                                    ng-bind="formEditJob.sending ? 'Salvando...' : 'Salvar'">
                                </button>
                                <?php endif; if ($formEditJob->btnRuning): ?>
                                    <button class="btn btn-primary m-b pull-right" ng-click="formEditJob.changeStatus('<?=\ES3\Models\Jobs::EXECUTANDO?>')"
                                        ng-bind="formEditJob.eBtnRuning ? 'Iniciando...' : 'Iniciar'">
                                    </button>
                                <?php endif; if ($formEditJob->btnStop): ?>
                                    <button class="btn btn-primary m-b pull-right" ng-click="formEditJob.changeStatus('<?=\ES3\Models\Jobs::PAUSADO?>')"
                                        ng-bind="formEditJob.eBtnStop ? 'Pausando...' : 'Pausar'">
                                    </button>
                                <?php endif; if ($formEditJob->btnCancel): ?>
                                    <button class="btn btn-primary m-b pull-right" ng-click="formEditJob.changeStatus('<?=\ES3\Models\Jobs::CANCELADO?>')"
                                        ng-bind="formEditJob.eBtnCancel ? 'Cancelando...' : 'Cancelar'">
                                    </button>
                                <?php endif; if ($formEditJob->btnFinish): ?>
                                    <button class="btn btn-primary m-b pull-right" ng-click="formEditJob.changeStatus('<?=\ES3\Models\Jobs::CONCLUIDO?>')"
                                        ng-bind="formEditJob.eBtnFinish ? 'Concluindo...' : 'Concluir'">
                                    </button>
                                <?php endif; ?>
                                <es3-alert alertdata="formEditJob.alert"></es3-alert>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Contas para transfência</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td>Origem
                                        <input class="form-control" placeholder="Endereço de oriem" ng-model="filterGridAcc.EndOriegm" ng-show="filterGridAcc.filtermode">
                                    </td>
                                    <td>Destino
                                        <input class="form-control" placeholder="Endereço de destino" ng-model="filterGridAcc.EndDestino" ng-show="filterGridAcc.filtermode">
                                    </td>
                                    <td>Execução
                                        <input class="form-control" placeholder="Número de execução" ng-model="filterGridAcc.Execucao" ng-show="filterGridAcc.filtermode">
                                    </td>
                                    <td>Status
                                        <select class="form-control" ng-model="filterGridAcc.Status" ng-show="filterGridAcc.filtermode">
                                            <option></option>
                                            <option value="">a</option>
                                        </selecT>
                                    </td>
                                    <td width="60px">
                                        <a class="btn" ng-class="{'btn-primary':filterGridAcc.filtermode, 'btn-default':!filterGridAcc.filtermode}" ng-click="filterGridAcc.toggle()">
                                            <i class="fa fa-filter"></i>
                                        </a>
                                    </td>
                                <tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="acc in _VB.accList track by $index" ng-show="filterGridAcc.filter(acc)">
                                    <td>{{acc.EndOrigem}}</td>
                                    <td>{{acc.EndDestino}}</td>
                                    <td>{{}}</td>
                                    <td>{{}}</td>
                                    <td>
                                        <a class="btn btn-primary" href="{{_VB.baseURL + 'transfers/' + acc.ID}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                <tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>