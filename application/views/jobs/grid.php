<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{_VB.baseURL + 'jobs/create'}}">Adicionar Job</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID
                                    <input class="form-control" placeholder="ID" ng-model="filterGrid.ID" ng-show="filterGrid.filtermode" style="width: 65px;">
                                </th>
                                <th>Empresa
                                    <input class="form-control" placeholder="Empresa" ng-model="filterGrid.RazaoSocial" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Servidor
                                    <input class="form-control" placeholder="Servidor" ng-model="filterGrid.EndrecoServidor" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Domínio
                                    <input class="form-control" placeholder="Domínio" ng-model="filterGrid.EnderecoDominio" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Status
                                    <select class="form-control" ng-model="filterGrid.Status" ng-show="filterGrid.filtermode">
                                        <option></option>
                                        <option value="AGUARDANDO">AGUARDANDO</option>
                                        <option value="EXECUTANDO">EXECUTANDO</option>
                                        <option value="PAUSADO">PAUSADO</option>
                                        <option value="CANCELADO">CANCELADO</option>
                                        <option value="CONCLUIDO">CONCLUÍDO</option>
                                    </select>
                                </th>
                                <th>Criado em</th>
                                <th>
                                    <a class="btn" ng-class="{'btn-primary':filterGrid.filtermode, 'btn-default':!filterGrid.filtermode}" ng-click="filterGrid.toggle()">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="jobs in _VB.jobsData" ng-show="filterGrid.filter(jobs)">
                                <td>#{{jobs.ID}}</td>
                                <td>{{jobs.Empresas.RazaoSocial}}</td>
                                <td>{{jobs.Servidores.Endereco}}</td>
                                <td>{{jobs.Dominios.Endereco}}</td>
                                <td>
                                    <div ng-if="jobs.Status == 'AGUARDANDO'" class='bg-info p-xs-5 b-r-sm text-center'>Aguardando</div>
                                    <div ng-if="jobs.Status == 'EXECUTANDO'" class='bg-primary p-xs-5 b-r-sm text-center'>Executando</div>
                                    <div ng-if="jobs.Status == 'PAUSADO'" class='bg-warning p-xs-5 b-r-sm text-center'>Pausado</div>
                                    <div ng-if="jobs.Status == 'CANCELADO'" class='bg-danger p-xs-5 b-r-sm text-center'>Cancelado</div>
                                    <div ng-if="jobs.Status == 'CONCLUIDO'" class='bg-success p-xs-5 b-r-sm text-center'>Concluído</div>
                                </td>
                                <td>{{jobs.CriadoEm |replace:' ':'T' |date: 'dd/MM/yyyy HH:mm'}}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{_VB.baseURL + 'jobs/editjob/' + jobs.ID}}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $thisView->includes("footer") ?>

