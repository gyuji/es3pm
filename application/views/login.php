<?php $thisView->includes("head.guest"); ?>
<div ng-controller="<?= $ngCtrSec ?>">
    <div>
        <h1 class="logo-name">ES3</h1>
    </div>
    <h3>Bem-vindo ao portal de migração</h3>
    <p>Entre com as suas credenciais</p>
    <?php 
        
    ?>
    <form class="m-t" role="form" name="formLogin.form" ng-submit="formLogin.onsubmit()">
        <div class="form-group">
            <input class="form-control" ng-model="formLogin.data.username.value"
                <?php $username->html() ?>>
        </div>
        <div class="form-group">
            <input class="form-control" ng-model="formLogin.data.password.value"
                <?php $password->html() ?>>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b" ng-disabled="!formLogin.form.$valid || formLogin.sending">
            {{formLogin.sending ? "Autenticando..." : "Login"}}
        </button>
        <es3-alert alertdata="formLogin.alert"></es3-alert>
    </form>
</div>
<?php $thisView->includes("footer.guest"); ?>

