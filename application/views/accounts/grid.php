<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content" ng-class="{'sk-loading': formEditAccounts.gettingAccounts}">
                    <div class="sk-spinner sk-spinner-rotating-plane"></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <a class="btn btn-primary" href="accounts/create/upfile">Cadastrar contas</a>
                        </div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <div class="input-group ">
                                <input type="text" placeholder="Pesquisar domínios" class="input-sm form-control pull-right" ng-model="searchDomain">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search fs-18"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button ng-show="!searchDomain || domain.Endereco.indexOf(searchDomain) > -1" ng-repeat="domain in _VB.domainsList" 
                                class="btn m-5 {{formEditAccounts.domainSelectedGrid.Endereco == domain.Endereco ? 'btn-info' : 'btn-default'}}" ng-click="formEditAccounts.setSelectedDomain(domain)">
                                {{domain.Endereco}}
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <div class="alert alert-success" ng-if="!formEditAccounts.domainSelectedGrid">
                                    Selecione um dos domínios acima para visualizar as contas
                                </div>
                                <div class="alert alert-warning" ng-if="formEditAccounts.domainsLoaded[formEditAccounts.domainSelectedGrid.ID].accounts.length == 0">
                                    O domínio {{formEditAccounts.domainSelectedGrid.Endereco}} não possui contas de e-mail cadastradas.<br>
                                    <a href="{{_VB.baseURL}}accounts/create/upfile">Clique aqui</a> para cadastrar
                                </div>
                                <es3-alert alertdata="formEditAccounts.alert"></es3-alert>
                                <table class="table table-striped" ng-if="formEditAccounts.domainsLoaded[formEditAccounts.domainSelectedGrid.ID].accounts.length > 0">
                                    <thead>
                                        <tr>
                                            <td><b>Linha</b></td>
                                            <td colspan="2"><b>Origem</b></td>
                                            <td colspan="2"><b>Destino</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>#
                                                <input class="form-control" placeholder="Linha" ng-model="filterGrid.index" ng-show="filterGrid.filtermode" style="width: 65px;">
                                            </td>
                                            <td>Endereço
                                                <input class="form-control" placeholder="Endereço de origem" ng-model="filterGrid.EndOrig" ng-show="filterGrid.filtermode">
                                            </td>
                                            <td>Senha
                                                <input class="form-control" placeholder="Senha de origem" ng-model="filterGrid.SenhaOrig" ng-show="filterGrid.filtermode">
                                            </td>
                                            <td>Endereço
                                                <input class="form-control" placeholder="Endereço de destino" ng-model="filterGrid.EndDest" ng-show="filterGrid.filtermode">
                                            </td>
                                            <td>Senha
                                                <input class="form-control" placeholder="Senha de origem" ng-model="filterGrid.SenhaDest" ng-show="filterGrid.filtermode">
                                            </td>
                                            <td>
                                                <a class="btn" ng-class="{'btn-primary':filterGrid.filtermode, 'btn-default':!filterGrid.filtermode}" ng-click="filterGrid.toggle()">
                                                    <i class="fa fa-filter"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="account in formEditAccounts.domainsLoaded[formEditAccounts.domainSelectedGrid.ID].accounts" 
                                            ng-if="!account.deleted" ng-show="filterGrid.filter(account, $index)">
                                            <td>#{{$index+1}}</td>
                                            <td>{{account.EndOrigem}}</td>
                                            <td><input class="form-control" ng-model="account.SenhaOrigem"
                                                <?php $passOrig->html() ?>>
                                            </td>
                                            <td>{{account.EndDestino}}</td>
                                            <td><input class="form-control" ng-model="account.SenhaDestino"
                                                <?php $passDest->html() ?>>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary" ng-click="formEditAccounts.save(account, $index)">
                                                    <i class="fa fa-save"></i>
                                                </a>
                                                <a class="btn btn-danger" ng-click="formEditAccounts.confirmDelete($index)">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>