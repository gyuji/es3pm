<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row" ng-if="!formUpListAccounts.complete">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <p>O arquivo CSV (separado por ponto e vírgula <b>;</b>) deve seguir a estrutura abaixo:</p>
                    <p>Não é obrigadótio possuir cabeçalho!</p>
                    <div class="well">
                        <p class="fs-20">endereço de e-mail na origem<b>;</b>senha na origem<b>;</b>endereço de e-mail no destino<b>;</b>senha no destino</p>
                        <p class="fs-16">fulano1@domino.com<b>;</b>minhaSenha<b>;</b>fulano1@dominio.com<b>;</b>minhaNovaSenha</p>
                        <p class="fs-16">fulano2@domino.com<b>;</b>minhaSenha<b>;</b>fulano2@dominio.com<b>;</b>minhaNovaSenha</p>
                        <p class="fs-16">fulano3@domino.com<b>;</b>minhaSenha<b>;</b>fulano3@dominio.com<b>;</b>minhaNovaSenha</p>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form name="formUpListAccounts.form">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Selecione o arquivo com a lista das contas.<br>
                                Arquivos permitidos: {{formUpListAccounts.data.csvFile.validationFile.pattern.join(', ')}}<br>
                                Tamanho: {{formUpListAccounts.data.csvFile.validationFile.size.max}}</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Selecionar aquivo</span>
                                        <span class="fileinput-exists">Alterar</span>
                                        <input type="file" ng-model="formUpListAccounts.data.csvFile.value" ngf-select
                                            <?php $csvFile->getValidationFile()?> ngf-model-invalid="invalidFile">        
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                </div> 
                                <span class="text-red" ng-if="formUpListAccounts.form.$error.maxSize">
                                    O arquivo <b>{{invalidFile.name}} ({{invalidFile.size}} B)</b> ultrapassou o limite de <b>{{formUpListAccounts.data.csvFile.validationFile.size.max}}</b>
                                </span>
                                <span class="text-red" ng-if="formUpListAccounts.form.$error.pattern">
                                    Extensão inválida do arquivo <b>{{invalidFile.name}}</b>, permitido <b>{{formUpListAccounts.data.csvFile.validationFile.pattern.join(', ')}}</b>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <label>Ou se preferir insira o conteúdo CSV no campo abaixo</label>
                            <textarea id="csvTextarea"></textarea>
                            </div>
                        </div>
                        <div class="row m-t-5">
                            <div class="col-md-12">
                                <button class="btn btn-primary" ng-click="formUpListAccounts.onsubmit()"
                                    ng-disabled="!formUpListAccounts.form.$valid || formUpListAccounts.sending"
                                    ng-bind="formUpListAccounts.sending ? 'Enviando lista de contas...' : 'Enviar lista de contas'">
                                </button>
                            </div>
                        </div>
                        <es3-alert alertdata="formUpListAccounts.alert"></es3-alert>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row" ng-if="formUpListAccounts.complete">
        <div class="col-lg-12">
            <es3-alert alertdata="formConfirmAccounts.alert"></es3-alert>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Falta pouco! Foram enviadas {{formConfirmAccounts.accList.length}} contas, agora confirme os dados e envie.
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-primary" ng-click="formConfirmAccounts.onsubmit()"
                                ng-disabled="formConfirmAccounts.sending || formConfirmAccounts.nbAcc() == 0"
                                ng-bind="formConfirmAccounts.sending ? 'Enviando lista de contas...' : 'Enviar lista de contas'">
                            </button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td><b>Linha</b></td>
                                    <td colspan="2"><b>Origem</b></td>
                                    <td colspan="2"><b>Destino</b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Endereço</td>
                                    <td>Senha</td>
                                    <td>Endereço</td>
                                    <td>Senha</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="account in formConfirmAccounts.accList" ng-if="!account.deleted">
                                    <td>#{{$index+1}}</td>
                                    <td><input class="form-control" ng-model="account.addrOrig"
                                        <?php $addrOrig->html() ?>>
                                    </td>
                                    <td><input class="form-control" ng-model="account.passOrig"
                                        <?php $passOrig->html() ?>>
                                    </td>
                                    <td><input class="form-control" ng-model="account.addrDest"
                                        <?php $addrDest->html() ?>>
                                    </td>
                                    <td><input class="form-control" ng-model="account.passDest"
                                        <?php $passDest->html() ?>>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger" ng-click="account.deleted = true">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Removidos</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td><b>Linha</b></td>
                                    <td colspan="2"><b>Origem</b></td>
                                    <td colspan="2"><b>Destino</b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Endereço</td>
                                    <td>Senha</td>
                                    <td>Endereço</td>
                                    <td>Senha</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="account in formConfirmAccounts.accList" ng-if="account.deleted">
                                    <td>#{{$index+1}}</td>
                                    <td>{{account.addrOrig}}</td>
                                    <td>{{account.passOrig}}</td>
                                    <td>{{account.addrDest}}</td>
                                    <td>{{account.passDest}}</td>
                                    <td>
                                        <a class="btn btn-primary" ng-click="account.deleted = false">
                                            <i class="fa fa-arrow-circle-o-up"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>