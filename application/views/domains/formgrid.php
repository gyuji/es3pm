<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cadastrar domínio</h5>
            </div>
            <div class="ibox-content">
                <form name="formCreateDomain.form" class="form-inline">
                    <div class="form-group">
                        <select class="form-control" ng-model="formCreateDomain.data.IDEmpresas.value">
                            <option ng-
                            <?php foreach ($companiesList as $row): ?>
                                <option ng-value="<?=$row->ID?>">CID<?=$row->ID." - ".$row->RazaoSocial?></option>    
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" ng-model="formCreateDomain.data.Endereco.value"
                            <?php $Endereco->html() ?>>
                    </div>
                    <button class="btn btn-primary m-b" ng-click="formCreateDomain.onsubmit()"
                        ng-disabled="!formCreateDomain.form.$valid || formCreateDomain.sending"
                        ng-bind="formCreateDomain.sending ? 'Cadastrando...' : 'Cadastrar'">
                    </button>
                </form>
                <es3-alert alertdata="formCreateDomain.alert"></es3-alert>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <es3-alert alertdata="formDeleteDomain.alert"></es3-alert>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Empresa
                                    <select class="form-control" placeholder="Empresa" ng-model="filterGrid.IDEmpresas" ng-show="filterGrid.filtermode">
                                        <option></option>
                                        <?php foreach ($companiesList as $row): ?>
                                            <option value="<?=$row->ID?>">CID<?=$row->ID." - ".$row->RazaoSocial?></option>    
                                        <?php endforeach; ?>
                                    </select>
                                </th>
                                <th>Endereço
                                    <input class="form-control" placeholder="Endereco" ng-model="filterGrid.Endereco" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Criado em</th>
                                <th>
                                    <a class="btn" ng-class="{'btn-primary':filterGrid.filtermode, 'btn-default':!filterGrid.filtermode}" ng-click="filterGrid.toggle()">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="domainRow in _VB.domainsData |orderBy:'IDEmpresas'"
                                ng-show="filterGrid.filter(domainRow)" ng-init="domainRow.form.ID = domainRow.ID">
                                <td>CID{{_VB.companiesList[domainRow.IDEmpresas].ID + " - " + _VB.companiesList[domainRow.IDEmpresas].RazaoSocial}}</td>
                                <td>{{domainRow.Endereco}}</td>   
                                <td width="200px">{{domainRow.CriadoEm |replace:' ':'T' |date: 'dd/MM/yyyy HH:mm'}}</td>
                                <td width="100px">
                                    <a class="btn btn-danger" ng-click="formDeleteDomain.confirmDelete($index)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $thisView->includes("footer") ?>