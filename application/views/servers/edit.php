<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="m-t" name="formEditServer.form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label><?=$ID->label?></label>: <?=$ID->value?><br>
                                    <label><?=$CriadoEm->label?></label>: <?=\ES3\Utils::dateTimeFormat($CriadoEm->value)?>                                    
                                </div>
                                <div class="form-group col-md-12">
                                    <label><?=$Apelido->label?></label>
                                        <input class="form-control" ng-model="formEditServer.data.Apelido.value" 
                                            <?php $Apelido->html() ?>>
                                </div>
                                <div class="form-group">
                                    <div class="form-group col-md-6">
                                        <label><?=$Endereco->label?></label>
                                        <input class="form-control" disabled ng-model="formEditServer.data.Endereco.value"
                                            <?php $Endereco->html() ?>>
                                    </div>
                                    <div class="col-md-3">
                                        <label><?=$Porta->label?></label>
                                        <input class="form-control" disabled ng-model="formEditServer.data.Porta.value"
                                            <?php $Porta->html() ?>>
                                    </div>
                                    <div class="col-md-3">
                                        <label><?=$Seguranca->label?></label>
                                        <select class="form-control" ng-model="formEditServer.data.Seguranca.value">
                                            <option value="NONE">Nenhum</option>
                                            <option value="SSL">SSL</option>
                                            <option value="TLS">TLS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?=$Map->label?></label>
                                <div class="form-group col-md-6">
                                    <textarea rows="4" class="form-control" ng-model="formEditServer.data.Map.value"
                                        <?php $Map->html() ?>>
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-primary m-b" ng-click="formEditServer.onsubmit()"
                                        ng-disabled="!formEditServer.form.$valid || formEditServer.sending"
                                        ng-bind="'Salvar'">
                                    </button>
                                    <button class="btn btn-danger m-b" ng-click="formEditServer.confirmDeleteServer()"
                                        ng-bind="'Remover'">
                                    </button>
                                    <es3-alert alertdata="formEditServer.alert"></es3-alert>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>