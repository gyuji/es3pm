<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="m-t" name="formCreateServer.form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <div class="col-md-12">
                                        <label><?=$Apelido->label?></label>
                                        <input class="form-control" ng-model="formCreateServer.data.Apelido.value"
                                            <?php $Apelido->html() ?>>      
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-6">
                                        <label><?=$Endereco->label?></label>
                                        <input class="form-control" ng-model="formCreateServer.data.Endereco.value"
                                            <?php $Endereco->html() ?>>
                                    </div>
                                    <div class="col-md-3">
                                        <label><?=$Porta->label?></label>
                                        <input class="form-control" ng-model="formCreateServer.data.Porta.value"
                                            <?php $Porta->html() ?>>
                                    </div>
                                    <div class="col-md-3">
                                        <label><?=$Seguranca->label?></label>
                                        <select id="seguranca" class="form-control" ng-model="formCreateServer.data.Seguranca   .value">
                                            <option value="NONE">Nenhum</option>
                                            <option value="SSL">SSL</option>
                                            <option value="TLS">TLS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?=$Map->label?></label>
                                <div class="form-group col-md-6">
                                    <textarea rows="4" class="form-control" name="Map" ng-model="formCreateServer.data.Map.value"
                                        <?php $Map->html() ?>>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group col-md-12">
                                    <div class="col-md-12 form-group">
                                        <button class="btn btn-primary m-b" ng-click="formCreateServer.onsubmit()"
                                            ng-disabled="!formCreateServer.form.$valid || formCreateServer.sending"
                                            ng-bind="formCreateServer.sending ? 'Salvando...' : 'Salvar'">
                                        </button>
                                        <es3-alert alertdata="formCreateServer.alert"></es3-alert>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>