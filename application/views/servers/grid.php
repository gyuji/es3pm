<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{_VB.baseURL + 'servers/create'}}">Adicionar servidor</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID
                                    <input class="form-control" placeholder="ID" ng-model="filterGrid.ID" ng-show="filterGrid.filtermode" style="width: 65px;">
                                </th>
                                <th>Apelido
                                    <input class="form-control" placeholder="Apelido" ng-model="filterGrid.Apelido" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Endereço
                                    <input class="form-control" placeholder="Endereço" ng-model="filterGrid.Endereco" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Porta
                                    <input class="form-control" placeholder="Porta" ng-model="filterGrid.Porta" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Segurança
                                    <select class="form-control" ng-model="filterGrid.Seguranca" ng-show="filterGrid.filtermode">
                                        <option></option>
                                        <option value="NONE">Nenhum</option>
                                        <option value="SSL">SSL</option>
                                        <option value="TLS">TLS</option>
                                    </select>
                                </th>
                                <th>Status
                                    <select class="form-control" ng-model="filterGrid.Ativo" ng-show="filterGrid.filtermode">
                                        <option></option>
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </th>
                                <th>Criado em</th>
                                <th>
                                    <a class="btn" ng-class="{'btn-primary':filterGrid.filtermode, 'btn-default':!filterGrid.filtermode}" ng-click="filterGrid.toggle()">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="server in _VB.serversList" ng-show="filterGrid.filter(server)">
                                <td>#{{server.ID}}</td>
                                <td>{{server.Apelido}}</td>
                                <td>{{server.Endereco}}</td>
                                <td>{{server.Porta}}</td>
                                <td>{{server.Seguranca}}</td>
                                <td ng-bind-html="server.Ativo ? '<div class=\'bg-primary p-xs-5 b-r-sm text-center\'>Ativo</div>' : '<div class=\'bg-danger p-xs-5 b-r-sm text-center\'>Inativo</div>' |trusted"></td>
                                <td>{{server.CriadoEm |replace:' ':'T' |date: 'dd/MM/yyyy'}}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{_VB.baseURL + 'servers/edit/' + server.ID}}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php $thisView->includes("footer") ?>