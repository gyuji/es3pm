<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins" ng-if="<?=\ES3\Services\Auth::getInstance()->isRootCompany()?>">
            <div class="ibox-title">
                <h5>Cadastrar usuário</h5>
            </div>
            <div class="ibox-content">
                <form name="formCreateUser.form" class="form-inline">
                    <div class="form-group">
                        <label class="sr-only"><?=$IDEmpresas->label?></label>
                        <select class="form-control" ng-model="formCreateUser.data.IDEmpresas.value">
                            <option ng-
                            <?php foreach ($companiesList as $row): ?>
                                <option ng-value="<?=$row->ID?>">CID<?=$row->ID." - ".$row->RazaoSocial?></option>    
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?=$displayname->label?></label>
                        <input class="form-control" ng-model="formCreateUser.data.displayname.value"
                            <?php $displayname->html() ?>>
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?=$username->label?></label>
                        <input class="form-control" ng-model="formCreateUser.data.username.value"
                            <?php $username->html() ?>>
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?=$password1->label?></label>
                        <input class="form-control" ng-model="formCreateUser.data.password1.value"
                            <?php $password1->html() ?>>
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?=$password2->label?></label>
                        <input class="form-control" ng-model="formCreateUser.data.password2.value"
                            <?php $password2->html() ?>>
                    </div>
                    <button class="btn btn-primary m-b" ng-click="formCreateUser.onsubmit()"
                        ng-disabled="!formCreateUser.form.$valid || formCreateUser.sending ||
                        (!formCreateUser.data.password1.value || !formCreateUser.data.password1.value || formCreateUser.data.password1.value != formCreateUser.data.password2.value)"
                        ng-bind="formCreateUser.sending ? 'Cadastrando...' : 'Cadastrar'">
                    </button>
                </form>
                <es3-alert alertdata="formCreateUser.alert"></es3-alert>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <es3-alert alertdata="formGridEditUser.alert"></es3-alert>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Empresa
                                    <select class="form-control" ng-model="filterGrid.IDEmpresas" ng-show="filterGrid.filtermode">
                                        <option></option>
                                        <?php foreach ($companiesList as $row): ?>
                                            <option value="<?=$row->ID?>">CID<?=$row->ID." - ".$row->RazaoSocial?></option>    
                                        <?php endforeach; ?>
                                    </select>
                                </th>
                                <th>Nome
                                    <input class="form-control" placeholder="Nome" ng-model="filterGrid.Nome" ng-show="filterGrid.filtermode">
                                </th>
                                <th>E-mail
                                    <input class="form-control" placeholder="E-mail" ng-model="filterGrid.Email" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Senha</th>
                                <th>Criado em</th>
                                <th>
                                    <a class="btn" ng-class="{'btn-primary':filterGrid.filtermode, 'btn-default':!filterGrid.filtermode}" ng-click="filterGrid.toggle()">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="userRow in _VB.usersData |orderBy:'IDEmpresas'"
                                ng-show="filterGrid.filter(userRow)" ng-init="userRow.form.ID = userRow.ID">
                                <td>{{!userRow.editmode || userRow.ID == <?= $userAuth->ID ?> ? "CID"+_VB.companiesList[userRow.IDEmpresas].ID + " - " + _VB.companiesList[userRow.IDEmpresas].RazaoSocial : ''}}
                                    <select class="form-control" ng-init="userRow.form.IDEmpresas = userRow.IDEmpresas" ng-model="userRow.form.IDEmpresas" ng-if="userRow.editmode && userRow.ID != <?= $userAuth->ID ?>">
                                        <option ng-repeat="option in _VB.companiesList track by option.ID" ng-value="option.ID">CID{{option.ID + " - " + option.RazaoSocial}}</option>
                                    </select>
                                    <span ng-if="userRow.editmode && userRow.form.validate.IDEmpresas" class="text-red fs-12" ng-bind-html="userRow.form.validate.IDEmpresas |trusted"></span>
                                </td>
                                <td>{{!userRow.editmode ? userRow.Nome : ''}}
                                    <input class="form-control" ng-init="userRow.form.Nome = userRow.Nome" ng-model="userRow.form.Nome" ng-if="userRow.editmode"
                                        <?php $displayname->html() ?>>
                                    <span ng-if="userRow.editmode && userRow.form.validate.Nome" class="text-red fs-12" ng-bind-html="userRow.form.validate.Nome |trusted"></span>
                                </td>   
                                <td>{{!userRow.editmode ? userRow.Email : ''}}
                                    <input class="form-control" ng-init="userRow.form.Email = userRow.Email" ng-model="userRow.form.Email" ng-if="userRow.editmode"
                                        <?php $username->html() ?>>
                                    <span ng-if="userRow.editmode && userRow.form.validate.Email" class="text-red fs-12" ng-bind-html="userRow.form.validate.Email |trusted"></span>
                                </td>
                                <td>{{!userRow.editmode ? '*******' : ''}}
                                    <input class="form-control" ng-model="userRow.form.password1" ng-if="userRow.editmode"
                                        <?php $password1->html() ?>>
                                    <span ng-if="userRow.editmode && userRow.form.validate.password1" class="text-red fs-12" ng-bind-html="userRow.form.validate.password1 |trusted"></span>
                                    <input class="form-control m-t-5" ng-model="userRow.form.password2" ng-if="userRow.editmode"
                                        <?php $password2->html() ?>>
                                    <span ng-if="userRow.editmode && userRow.form.validate.password2" class="text-red fs-12" ng-bind-html="userRow.form.validate.password2 |trusted"></span>
                                </td>
                                <td width="200px">{{userRow.CriadoEm |replace:' ':'T' |date: 'dd/MM/yyyy HH:mm'}}</td>
                                <td width="100px">
                                    <a ng-show="!userRow.editmode" class="btn btn-primary" ng-click="userRow.editmode = true">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a ng-show="userRow.editmode" class="btn btn-primary" ng-click="formGridEditUser.onsubmit(userRow)">
                                        <i class="fa fa-save"></i>
                                    </a>
                                    <a ng-show="userRow.editmode" class="btn btn-default" ng-click="userRow.editmode = false">
                                        <i class="fa fa-window-close"></i>
                                    </a>
                                    <a class="btn btn-danger" ng-click="formGridEditUser.confirmDelete($index)" ng-if="userRow.ID != <?= $userAuth->ID ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $thisView->includes("footer") ?>