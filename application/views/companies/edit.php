<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="m-t" name="formEditCompany.form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?=$cid->label?></label>: <?=$cid->value?><br>
                                    <label><?=$Ativo->label?></label>: <?= $Ativo->value ? "Ativo" : "Inativo"?><br>
                                    <label><?=$CriadoEm->label?></label>: <?=\ES3\Utils::dateTimeFormat($CriadoEm->value)?>
                                </div>
                                <div class="form-group">
                                    <label><?=$RazaoSocial->label?></label>
                                    <input class="form-control" ng-model="formEditCompany.data.RazaoSocial.value"
                                        <?php $RazaoSocial->html() ?>>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?=$PForPJ->label?>
                                        <div class="radio">
                                            <input type="radio" id="rdPF" value="PF" ng-model="formEditCompany.data.PForPJ.value"
                                                <?php $PForPJ->isDisabled()?>>
                                            <label for="rdPF">Pessoa Física</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" id="rdPJ" value="PJ" ng-model="formEditCompany.data.PForPJ.value"
                                                <?php $PForPJ->isDisabled()?>>
                                            <label for="rdPJ">Pessoa Jurídica</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <?=$Tipo->label?>
                                        <div class="radio">
                                            <input type="radio" id="rdCLIENTE" value="CLIENTE" ng-model="formEditCompany.data.Tipo.value"
                                                <?php $Tipo->isDisabled()?>>
                                            <label for="rdCLIENTE">Cliente</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" id="rdREVENDA" value="REVENDA" ng-model="formEditCompany.data.Tipo.value"
                                                <?php $Tipo->isDisabled()?>>
                                            <label for="rdREVENDA">Revenda</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label ng-if="formEditCompany.data.PForPJ.value == 'PJ'"><?=$Cnpj->label?></label>
                                    <input class="form-control" ng-model="formEditCompany.data.Cnpj.value"
                                        <?php $Cnpj->html() ?> ng-if="formEditCompany.data.PForPJ.value == 'PJ'">
                                    <label ng-if="formEditCompany.data.PForPJ.value == 'PF'"><?=$Cpf->label?></label>
                                    <input class="form-control" ng-model="formEditCompany.data.Cpf.value"
                                        <?php $Cpf->html() ?> ng-if="formEditCompany.data.PForPJ.value == 'PF'">
                                </div>
                                <div class="form-group">
                                    <label><?=$IDRevenda->label?></label>
                                    <select class="form-control" ng-model="formEditCompany.data.IDRevenda.value"
                                        <?php $IDRevenda->html() ?>>
                                        <?php $IDRevenda->getOptions() ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?=$LimiteExecucoes->label?></label>
                                    <input class="form-control" ng-model="formEditCompany.data.LimiteExecucoes.value"
                                        <?php $LimiteExecucoes->html() ?>>
                                </div>
                                <div class="form-group">
                                    <label><?=$LimiteRepeticoes->label?></label>
                                    <input class="form-control" ng-model="formEditCompany.data.LimiteRepeticoes.value"
                                        <?php $LimiteRepeticoes->html() ?>>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php if ($formEditCompany->btnRestore): ?>
                                <button type="button" class="btn btn-primary m-b" ng-click="formEditCompany.confirmEnableCompany()"
                                    ng-bind="formEditCompany.enabling ? 'Habilitando...' : 'Habilitar'">
                                </button>
                                <?php endif; 
                                if ($formEditCompany->btnDelete): ?>
                                <button type="button" class="btn btn-danger m-b" ng-click="formEditCompany.confirmDisableCompany()"
                                    ng-bind="formEditCompany.disabling ? 'Desabilitando...' : 'Desabilitar'">
                                </button>
                                <?php endif; 
                                if ($formEditCompany->btnSubmit): ?>
                                <button class="btn btn-primary m-b pull-right" ng-click="formEditCompany.onsubmit()"
                                    ng-disabled="!formEditCompany.form.$valid || formEditCompany.sending ||
                                    formEditCompany.data.PForPJ.value == 'PJ' && !formEditCompany.data.Cnpj.value ||
                                    formEditCompany.data.PForPJ.value == 'PF' && !formEditCompany.data.Cpf.value"
                                    ng-bind="formEditCompany.sending ? 'Salvando...' : 'Salvar'">
                                </button>
                                <?php endif; ?>
                                <es3-alert alertdata="formEditCompany.alert"></es3-alert>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>