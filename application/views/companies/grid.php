<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{_VB.baseURL + 'companies/create'}}">Adicionar empresa</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>CID
                                    <input class="form-control" placeholder="CID" ng-model="filterGrid.ID" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Nome / Razão Social
                                    <input class="form-control" placeholder="Nome / Razão Social" ng-model="filterGrid.RazaoSocial" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Revenda
                                    <input class="form-control" placeholder="Revanda" ng-model="filterGrid.IDEmpresasRevenda" ng-show="filterGrid.filtermode">
                                </th>
                                <th>Tipo
                                    <input class="form-control" placeholder="Tipo" ng-model="filterGrid.Tipo" ng-show="filterGrid.filtermode">
                                </th>
                                <th>CPF / CNPJ
                                    <input class="form-control" placeholder="CPF / CNPJ" ng-model="filterGrid.CnpjCpf" ng-show="filterGrid.filtermode">
                                </th>
                                <th width="120">Status
                                    <select class="form-control" ng-model="filterGrid.Ativo" ng-show="filterGrid.filtermode">
                                        <option></option>
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </th>
                                <th>Criado em</th>
                                <th>
                                    <a class="btn" ng-class="{'btn-primary':filterGrid.filtermode, 'btn-default':!filterGrid.filtermode}" ng-click="filterGrid.toggle()">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="company in _VB.companiesList" ng-show="filterGrid.filter(company)">
                                <td>#{{company.ID}}</td>
                                <td>{{company.RazaoSocial}}</td>
                                <td>{{_VB.companiesList[company.IDEmpresasRevenda].RazaoSocial}}</td>
                                <td>{{company.Tipo}}</td>
                                <td>{{company.CnpjCpf}}</td>
                                <td ng-bind-html="company.Ativo ? '<div class=\'bg-primary p-xs-5 b-r-sm text-center\'>Ativo</div>' : '<div class=\'bg-danger p-xs-5 b-r-sm text-center\'>Inativo</div>' |trusted"></td>
                                <td>{{company.CriadoEm |replace:' ':'T' |date: 'dd/MM/yyyy'}}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{_VB.baseURL + 'companies/edit/' + company.ID}}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php $thisView->includes("footer") ?>