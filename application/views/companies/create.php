<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="m-t" name="formCreateCompany.form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=$cid->label?></label>
                                <input class="form-control" ng-model="formCreateCompany.data.cid.value"
                                    <?php $cid->html() ?>>
                            </div>
                            <div class="form-group">
                                <label><?=$RazaoSocial->label?></label>
                                <input class="form-control" ng-model="formCreateCompany.data.RazaoSocial.value"
                                    <?php $RazaoSocial->html() ?>>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <?=$PForPJ->label?>
                                    <div class="radio">
                                        <input type="radio" id="rdPF" value="PF" ng-model="formCreateCompany.data.PForPJ.value">
                                        <label for="rdPF">Pessoa Física</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" id="rdPJ" value="PJ" ng-model="formCreateCompany.data.PForPJ.value">
                                        <label for="rdPJ">Pessoa Jurídica</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <?=$Tipo->label?>
                                    <div class="radio">
                                        <input type="radio" id="rdCLIENTE" value="CLIENTE" ng-model="formCreateCompany.data.Tipo.value">
                                        <label for="rdCLIENTE">Cliente</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" id="rdREVENDA" value="REVENDA" ng-model="formCreateCompany.data.Tipo.value">
                                        <label for="rdREVENDA">Revenda</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label ng-if="formCreateCompany.data.PForPJ.value == 'PJ'"><?=$Cnpj->label?></label>
                                <input class="form-control" ng-model="formCreateCompany.data.Cnpj.value"
                                    <?php $Cnpj->html() ?> ng-if="formCreateCompany.data.PForPJ.value == 'PJ'">
                                <label ng-if="formCreateCompany.data.PForPJ.value == 'PF'"><?=$Cpf->label?></label>
                                <input class="form-control" ng-model="formCreateCompany.data.Cpf.value"
                                    <?php $Cpf->html() ?> ng-if="formCreateCompany.data.PForPJ.value == 'PF'">
                            </div>
                            <div class="form-group">
                                <label><?=$IDRevenda->label?></label>
                                <select class="form-control" ng-model="formCreateCompany.data.IDRevenda.value"
                                    <?php $IDRevenda->html() ?>>
                                    <?php $IDRevenda->getOptions() ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><?=$LimiteExecucoes->label?></label>
                                <input class="form-control" ng-model="formCreateCompany.data.LimiteExecucoes.value"
                                    <?php $LimiteExecucoes->html() ?>>
                            </div>
                            <div class="form-group">
                                <label><?=$LimiteRepeticoes->label?></label>
                                <input class="form-control" ng-model="formCreateCompany.data.LimiteRepeticoes.value"
                                    <?php $LimiteRepeticoes->html() ?> >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary m-b" ng-click="formCreateCompany.onsubmit()"
                                ng-disabled="!formCreateCompany.form.$valid || formCreateCompany.sending ||
                                formCreateCompany.data.PForPJ.value == 'PJ' && !formCreateCompany.data.Cnpj.value ||
                                formCreateCompany.data.PForPJ.value == 'PF' && !formCreateCompany.data.Cpf.value"
                                ng-bind="formCreateCompany.sending ? 'Salvando...' : 'Salvar'">
                            </button>
                            <es3-alert alertdata="formCreateCompany.alert"></es3-alert>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php $thisView->includes("footer") ?>