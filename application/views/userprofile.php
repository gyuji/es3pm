<?php 
    $thisView->includes("head");
    $thisView->includes("breadcrumb"); 
?>
<div ng-controller="<?= $ngCtrSec ?>" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados do Usuário</h5>
                </div>
                <div class="ibox-content">
                    <form class="m-t form-inline" name="formChangeDisplayName.form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nome</label>: <input class="form-control" ng-model="formChangeDisplayName.data.displayname.value"
                                        <?php $displayname->html() ?>>
                                </div>
                                <button class="btn btn-primary m-b" ng-click="formChangeDisplayName.onsubmit()"
                                    ng-disabled="!formChangeDisplayName.form.$valid || formChangeDisplayName.sending"
                                    ng-bind="formChangeDisplayName.sending ? 'Salvando...' : 'Salvar'">
                                </button>
                            </div>
                            <es3-alert alertdata="formChangeDisplayName.alert" class="col-md-6"></es3-alert>
                        </div>
                    </form>

                    <label>E-mail</label>: <?=$userAuth->Email?><br>
                    <label>Criado em</label>: <?=ES3\Utils::dateTimeFormat($userAuth->CriadoEm)?><br>
                    <hr>
                    <form class="m-t" name="formChangeUserPass.form">
                        <h4>Alterar senha</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" ng-model="formChangeUserPass.data.password1.value"
                                        <?php $password1->html() ?>>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" ng-model="formChangeUserPass.data.password2.value"
                                        <?php $password2->html() ?>>
                                </div>
                                <button class="btn btn-primary m-b" ng-click="formChangeUserPass.onsubmit()"
                                    ng-disabled="!formChangeUserPass.form.$valid || formChangeUserPass.sending ||
                                    formChangeUserPass.data.password1.value != formChangeUserPass.data.password2.value"
                                    ng-bind="formChangeUserPass.sending ? 'Salvando...' : 'Salvar'">
                                </button>
                                <es3-alert alertdata="formChangeUserPass.alert"></es3-alert>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados da Empresa</h5>
                </div>
                <div class="ibox-content">
                    <label>CID</label>: <?=$userAuth->Empresas->ID?><br>
                    <?php if (\ES3\Utils::isCNPJ($userAuth->Empresas->CnpjCpf)): ?>
                        <label>Razão Social</label>: <?=$userAuth->Empresas->RazaoSocial?><br>
                        <label>CNPJ</label>: <?=$userAuth->Empresas->CnpjCpf?><br>
                    <?php else: ?>
                        <label>Nome</label>: <?=$userAuth->Empresas->RazaoSocial?><br>
                        <label>CPF</label>: <?=$userAuth->Empresas->CnpjCpf?><br>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php $thisView->includes("footer") ?>