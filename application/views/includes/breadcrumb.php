<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            <?php if ($breadcrumb->getButtonReturn()): ?>
                <a href="<?=$breadcrumb->getButtonReturn()?>" class="btn btn-primary m-r-10">Voltar</a>
            <?php endif; ?>
            <?= $breadcrumb->getPageDesc() ?>
        </h2>
        <ol class="breadcrumb">
            <?= $breadcrumb->getHtmlCrumb() ?>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<div class="m-t">
    <?php if ($breadcrumb->getTopAlert()): 
        foreach ($breadcrumb->getTopAlert() as $alert): ?>
        <div class="alert alert-dismissable text-left alert-<?=$alert->type?>">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <p><?=$alert->msg?></p>
        </div>
    <?php endforeach;
    endif; ?> 
</div>