<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"><span></span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"><strong class="font-bold"><?= $userAuth->Nome ?></strong></span>
                        <span class="text-muted text-xs block"><?= $userAuth->Empresas->RazaoSocial ?? "Nome da empresa" ?><b class="caret"></b></span></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?= base_url("/userprofile") ?>">Perfil</a></li>
                        <li><a href="<?= base_url("/auth/logout") ?>">Sair</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    ES3
                </div>
            </li>
            <?php
                $menuStructure = $menu->getMenuStructure();
                foreach ($menuStructure as $id => $item):
                    if ($item->type == $item::SINGLE): ?>
                        <li class="<?= $item->active ? "active" : null ?>">
                            <a href="<?= $item->href ?>" id="<?= $item->htmlid ?>"><i class="<?= $item->icon ?>"></i> 
                                <span class="nav-label"><?= $item->label ?></span>
                            </a>
                        </li>
                    <?php elseif ($item->type == $item::DROPDOWN): ?>
                        <li class="<?= $item->active ? "active" : null ?>">
                            <a id="<?= $item->htmlid ?>"><i class="<?= $item->icon ?>"></i> 
                                <span class="nav-label"><?= $item->label ?></span> 
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                            <?php foreach ($item->subitems as $subitem): ?>
                                    <li><a href="<?= $subitem->href ?>"><?= $subitem->label ?></a></li>
                            <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endif;
                endforeach; 
            ?>
        </ul>
    </div>
</nav>