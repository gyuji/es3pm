<html ng-app="<?= $VB["ngApp"] ?>">
	<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $title ?? null ?></title>
		<?= $thisView::printStyle() ?>
	</head>
    <body class="" ng-controller="<?= $VB["ngCtrMain"] ?>" class="ng-hide" ng-show="true">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="0" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <div id="wrapper">
        <?php $thisView->includes("navbar") ?>
        <div id="page-wrapper" class="gray-bg" style="min-height: 723px;">
            <?php $thisView->includes("navbartop");