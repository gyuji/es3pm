<?php

namespace ES3\Models;

use ES3\Utils;

class Servidores extends BaseModel 
{

    public function countServers($active = "1")
    {
        $this->db->from('servidores');
        $this->db->WHERE("Ativo={$active}");
        return $this->db->count_all_results();
    }

    public function getByID($ID)
    {
        return $this->db->get_where("servidores", [
            "ID" => $ID
        ])->result()[0] ?? null;
    }

    public function getListServers()
    {
        $rsServers = [];
        $allServers = $this->db->get_where("servidores", ["Ativo" => true])->result() ?: [];
        foreach ($allServers as $row1) {
            $rsServers[$row1->ID] = $row1;
        }
        return $serversTree;
    }

    public function existsEndereco($endereco, bool $active = null)
    {
        $this->db->select("Endereco");
        if ($active === null) {
            $condition = ["Endereco" => $endereco];
        } else $condition = ["Endereco" => $endereco, "Ativo" => $active];
        return $this->db->get_where("servidores", $condition)->result() ? true : false;
    }

    public function existsID($ID, bool $active = null)
    {
        $this->db->select("ID");
        if ($active === null) {
            $condition = ["ID" => $ID];
        } else $condition = ["ID" => $ID, "Ativo" => $active];
        return $this->db->get_where("servidores", $condition)->result() ? true : false;
    }

    public function insertServer(\stdClass $data)
    {
        return $this->db->insert('servidores', $data);
    }

    public function updateServer($ID, \stdClass $data)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('servidores', $data);
    }

    public function deleteServer($ID)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('servidores', ["Ativo" => false, "RemovidoEm" => Utils::now()]);
    }
}
