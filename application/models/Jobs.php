<?php

namespace ES3\Models;

use ES3\Utils;

class Jobs extends BaseModel 
{
    const AGUARDANDO = "AGUARDANDO";
    const EXECUTANDO = "EXECUTANDO";
    const PAUSADO = "PAUSADO";
    const CANCELADO = "CANCELADO";
    const CONCLUIDO = "CONCLUIDO";

    public function getByID($ID)
    {
        $rs = $this->db->get_where("jobs", [
            "ID" => $ID
        ])->result()[0] ?? null;
        if (!$rs) return null;
        $rs->Empresas = $this->Empresas->getByID($rs->IDEmpresas);
        $rs->Dominios = $this->Dominios->getByID($rs->IDDominios);
        $rs->Servidores = $this->Servidores->getByID($rs->IDServidores);
        return $rs;
    }

    public function getByCompany($IDCompany)
    {
        $rs = $this->db->get_where("jobs", [
            "IDEmpresas" => $IDCompany,
            "Ativo" => true
        ])->result() ?: [];
        foreach ($rs as &$row) {
            $row->Empresas = $this->Empresas->getByID($row->IDEmpresas);
            $row->Dominios = $this->Dominios->getByID($row->IDDominios);
            $row->Servidores = $this->Servidores->getByID($row->IDServidores);
        }
        return $rs;
    }

    public function getByDomain($IDDomain)
    {
        $rs = $this->db->get_where("jobs", [
            "IDDominios" => $IDDomain,
            "Ativo" => true
        ])->result() ?: [];
        foreach ($rs as &$row) {
            $row->Empresas = $this->Empresas->getByID($row->IDEmpresas);
            $row->Dominios = $this->Dominios->getByID($row->IDDominios);
            $row->Servidores = $this->Servidores->getByID($row->IDServidores);
        }
        return $rs;
    }

    public function getByServers($IDServers)
    {
        $rs = $this->db->get_where("jobs", [
            "IDServidores" => $IDServers,
            "Ativo" => true
        ])->result() ?: [];
        foreach ($rs as &$row) {
            $row->Empresas = $this->Empresas->getByID($row->IDEmpresas);
            $row->Dominios = $this->Dominios->getByID($row->IDDominios);
            $row->Servidores = $this->Servidores->getByID($row->Servidores);
        }
        return $rs;
    }

    public function getCountExecutionByCompany($IDEmpresas, $diffID = null)
    {
        $this->db->where(["IDEmpresas" => $IDEmpresas, "Ativo" => true, "ID !=", $diffID]);
        $this->db->where_in("Status", [
            self::AGUARDANDO, self::EXECUTANDO, self::PAUSADO
        ]);
        $this->db->select("sum(Execucoes) as count");
        return $this->db->get("jobs")->result()[0]->count ?? 0;
    }

    public function existsJobsActiveOfDomain($IDDominios, $diffID = null)
    {
        $this->db->where("IDDominios", $IDDominios)
            ->where("Ativo", true)
            ->where("ID !=", $diffID)
            ->where_in("Status", [
                self::AGUARDANDO, self::EXECUTANDO, self::PAUSADO
            ]);
        return $this->db->get("jobs")->result() ?: [];
    }

    public function updateJobs($ID, \stdClass $data)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('jobs', $data);
    }

    public function createJobs(\stdClass $data)
    {
        return $this->db->insert('jobs', $data);
    }

    public function disableJobs($ID)
    {
        return $this->updateJobs($ID, (object)["Ativo" => false, "RemovidoEm" => Utils::now()]);
    }
}