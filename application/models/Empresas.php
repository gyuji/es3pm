<?php

namespace ES3\Models;

use ES3\Utils;

class Empresas extends BaseModel 
{
    const TIPO_CLIENTE = "CLIENTE";
    const TIPO_REVENDA = "REVENDA";

    public function countCompanies($active = "1")
    {
        $this->db->from('empresas');
        $this->db->WHERE("Ativo={$active}");
        return $this->db->count_all_results();
    }

    public function getByID($ID)
    {
        return $this->db->get_where("empresas", [
            "ID" => $ID
        ])->result()[0] ?? null;
    }

    public function getTreeCompaniesById($ID, bool $addRootTree = true)
    {
        $companiesTree = [];
        if ($addRootTree) {
            $companiesDirectClients = $this->db->get_where("empresas", [
                "IDEmpresasRevenda" => $ID
            ])->result() ?: [];
        } else {
            $companiesDirectClients = $this->db->get_where("empresas", [
                "ID !=" => $ID,
                "IDEmpresasRevenda" => $ID
            ])->result() ?: [];
        }
        foreach ($companiesDirectClients as $row1) {
            $companiesTree[$row1->ID] = $row1;
            if ($row1->Tipo == self::TIPO_CLIENTE) continue;
            $companiesUndirectClients = $this->db->get_where("empresas", [
                "IDEmpresasRevenda" => $row1->ID
            ])->result() ?: [];
            foreach ($companiesUndirectClients as $row2) {
                $companiesTree[$row2->ID] = $row2;
            }
        }
        return $companiesTree;
    }

    public function isPermitedAccessCompany($IDCompanyUser, $IDCheck)
    {
        $companiesTree = [];
        $this->db->select("ID, Tipo");
        $companiesDirectClients = $this->db->get_where("empresas", [
            "IDEmpresasRevenda" => $IDCompanyUser
        ])->result() ?: [];
        foreach ($companiesDirectClients as $row1) {
            $companiesTree[] = $row1->ID;
            if ($row1->Tipo == self::TIPO_CLIENTE) continue;
            $this->db->select("ID");
            $companiesUndirectClients = $this->db->get_where("empresas", [
                "IDEmpresasRevenda" => $row1->ID
            ])->result() ?: [];
            foreach ($companiesUndirectClients as $row2) {
                $companiesTree[] = $row2->ID;
            }
        }
        return in_array($IDCheck, $companiesTree);
    }

    public function isPermitedAccessResale($IDCompanyUser, $IDCheck)
    {
        $this->db->select("ID");
        return $this->db->get_where("empresas", [
            "ID" => $IDCheck,
            "IDEmpresasRevenda" => $IDCompanyUser,
            "Ativo" => true,
            "Tipo" => self::TIPO_REVENDA
        ])->result() ? true : false;
    }

    public function getTreeResaleById($IDCompanyUser)
    {
        $companiesTree = [];
        $this->db->select("ID, RazaoSocial");
        $companiesDirectClients = $this->db->get_where("empresas", [
            "IDEmpresasRevenda" => $IDCompanyUser,
            "Tipo" => self::TIPO_REVENDA,
            "Ativo" => true
        ])->result() ?: [];
        foreach ($companiesDirectClients as $row1) {
            $companiesTree[$row1->ID] = $row1;
        }
        return $companiesTree;
    }

    public function getAllocatedExecutionLimit($IDEmpresas)
    {
        $this->db->select("ID, LimiteExecucoes");
        $rsEmpresas = $this->db->get_where("empresas", [
            "ID !=" => $IDEmpresas,
            "IDEmpresasRevenda" => $IDEmpresas
        ])->result();
        $countLimit = 0;
        foreach ($rsEmpresas as $row) {
            $countLimit += $row->LimiteExecucoes;
        }
        return $countLimit;
    }

    public function existsCID($CID, bool $active = null)
    {
        $this->db->select("ID");
        if ($active === null) {
            $condition = ["ID" => $CID];
        } else $condition = ["ID" => $CID, "Ativo" => $active];
        return $this->db->get_where("empresas", $condition)->result() ? true : false;
    }

    public function existsCnpjCpf($CnpjCpf, $diffCID = null, bool $active = null)
    {
        $this->db->select("ID");
        if ($active === null) {
            $condition = ["CnpjCpf" => $CnpjCpf, "ID !=" => $diffCID];
        } else $condition = ["CnpjCpf" => $CnpjCpf, "ID !=" => $diffCID, "Ativo" => $active];
        return $this->db->get_where("empresas", $condition)->result() ? true : false;
    }

    public function existsRazaoSocial($RazaoSocial, $diffCID = null, bool $active = null)
    {
        $this->db->select("ID");
        if ($active === null) {
            $condition = ["RazaoSocial" => $RazaoSocial, "ID !=" => $diffCID];
        } else $condition = ["RazaoSocial" => $RazaoSocial, "ID !=" => $diffCID, "Ativo" => $active];
        return $this->db->get_where("empresas", $condition)->result() ? true : false;
    }

    public function insertCompany(\stdClass $data)
    {
        return $this->db->insert('empresas', $data);
    }

    public function updateCompany($ID, \stdClass $data)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('empresas', $data);
    }

    public function disableCompany($ID)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('empresas', ["Ativo" => false, "LimiteExecucoes" => 0, "LimiteRepeticoes" => 0, "EditadoEm" => Utils::now(), "RemovidoEm" => Utils::now()]);
    }

    public function enableCompany($ID)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('empresas', ["Ativo" => true, "EditadoEm" => Utils::now()]);
    }
}
