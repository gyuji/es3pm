<?php

namespace ES3\Models;

use ES3\Utils;

class Dominios extends BaseModel 
{

    public function countDomains($active = "1")
    {
        $this->db->from('dominios');
        $this->db->WHERE("Ativo={$active}");
        return $this->db->count_all_results();
    }

    public function getByID($ID)
    {
        return $this->db->get_where("dominios", ["ID" => $ID])->result()[0] ?? [];
    }

    public function getByCompaniesList(array $companiesList)
    {
        $this->db->where_in("IDEmpresas", $companiesList);
        $this->db->where(["Ativo" => true]);
        $rs = $this->db->get("dominios")->result() ?: [];
        $rsDominios = [];
        foreach ($rs as $row) {
            $rsDominios[$row->ID] = $row;
        }
        return $rsDominios;
    }

    public function getByDomain(String $Endereco, bool $Ativo = null)
    {
        $condition = ["Endereco" => $Endereco];
        if ($Ativo !== null) $condition["Ativo"] = $Ativo;
        return $this->db->get_where("dominios", $condition)->result()[0] ?? [];
    }

    public function createDomain(\stdClass $data)
    {
        return $this->db->insert('dominios', $data);
    }

    public function updateDomain($ID, \stdClass $data)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('dominios', $data);
    }

    public function disableDomain($ID)
    {
        return $this->updateDomain($ID, (object)["Ativo" => false, "RemovidoEm" => Utils::now()]);
    }
}