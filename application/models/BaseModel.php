<?php

namespace ES3\Models;

class BaseModel extends \CI_Model
{
    public function getInsertID()
    {
        return $this->db->insert_id();
    }
}
