<?php

namespace ES3\Models;

use ES3\Utils;

class Contas extends BaseModel 
{

    public function countAccounts()
    {
        $this->db->from('contas');
        return $this->db->count_all_results();
    }

    public function getByID($ID)
    {
        return $this->db->get_where("contas", ["ID" => $ID])->result()[0] ?? [];
    }

    public function getByDomain($ID)
    {
        return $this->db->get_where("contas", ["IDDominios" => $ID, "Ativo" => true])->result() ?: [];
    }

    public function insertAccounts(\stdClass $data)
    {
        return $this->db->insert("contas", $data);
    }

    public function saveAccount($ID, \stdClass $data)
    {
        return $this->db->update("contas", $data, ["ID" => $ID]);
    }

    public function disableAccount($ID)
    {
        return $this->saveAccount($ID, (object)[
            "Ativo" => false,
            "RemovidoEm" => Utils::now()
        ]);
    }
}