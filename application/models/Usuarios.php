<?php

namespace ES3\Models;

use ES3\Utils;

class Usuarios extends BaseModel 
{   
    protected $table = "usuarios";

    public function countUsers($active = "1")
    {
        $this->db->from($this->table);
        $this->db->WHERE("Ativo={$active}");
        return $this->db->count_all_results();
    }

    public function getByID($ID)
    {
        return $this->db->get_where("usuarios", ["ID" => $ID])->result()[0] ?? null;
    }

    public function getByEmail($email, bool $active = true)
    {
        $rsUsuarios = $this->db->get_where("usuarios", [
            "Email" => $email,
            "Ativo" => $active
        ])->result();

        if ($rsUsuarios) {
            foreach ($rsUsuarios as &$usuario) {
                $usuario->Empresas = $this->Empresas->getByID($usuario->IDEmpresas, true);
            }
        }

        return $rsUsuarios;
    }

    public function getByCompaniesList(array $companiesList)
    {
        $this->db->select("ID, IDEmpresas, Nome, Email, CriadoEm, EditadoEm, RemovidoEm, Ativo");
        $this->db->where_in("IDEmpresas", $companiesList);
        $this->db->where(["Ativo" => true]);
        return $this->db->get("usuarios")->result() ?: [];
    }

    public function existsEmail($Email, $diffID = null, bool $active = null)
    {
        $this->db->select("ID");
        if ($active === null) {
            $condition = ["Email" => $Email, "ID !=" => $diffID];
        } else $condition = ["Email" => $Email, "ID !=" => $diffID, "Ativo" => $active];
        return $this->db->get_where("usuarios", $condition)->result() ? true : false;
    }

    public function savePassword($ID, $password)
    {
        $this->db->set('Senha', $password);
        $this->db->set('EditadoEm', Utils::now());
        $this->db->where('ID', $ID);
        return $this->db->update('usuarios');
    }

    public function saveDisplayName($ID, $displayname)
    {
        $this->db->set('Nome', $displayname);
        $this->db->set('EditadoEm', Utils::now());
        $this->db->where('ID', $ID);
        return $this->db->update('usuarios');
    }

    public function getByCompany($IDCompany, bool $active = null)
    {
        $this->db->select("ID, IDEmpresas, Nome, Email, CriadoEm, EditadoEm, RemovidoEm, Ativo");
        $condition = ["IDEmpresas" => $IDCompany];
        if ($active !== null) $condition["Ativo"] = $active;
        return $this->db->get_where("usuarios", $condition)->result();
    }

    public function updateUser($ID, \stdClass $data)
    {
        $this->db->where('ID', $ID);
        return $this->db->update('usuarios', $data);
    }

    public function createUser(\stdClass $data)
    {
        return $this->db->insert('usuarios', $data);
    }

    public function disableUser($ID)
    {
        return $this->updateUser($ID, (object)["Ativo" => false, "RemovidoEm" => Utils::now()]);
    }
}
