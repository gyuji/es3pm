<?php

namespace ES3;

include "Text.php";

use ES3\Text;
use ES3\TClass;
use stdClass;

class Loader
{
    private $namespacesRegistered = [];
    private $classesRegistered = [];
    private $namespaceController = "";
    private $namespaceModel = "";

    static private $instance;

    private function __construct()
    {
        $this->splAutoLoader();
    }

    private function splAutoLoader()
    {
        spl_autoload_register([$this, "loaderClasses"]);
    }

    private function loaderClasses(string $class)
    {
        if (class_exists($class)) return true;
        if ($matched = $this->matchClasses($class) ?: $this->matchNamespace($class)) {
            $this->includeClasses($matched);
            if (class_exists($class)) return true;
        }
        return false;
    }

    private function includeClasses(array $classData)
    {
        if (!Text::startsWith($classData["dir"], APPPATH)) $classData["dir"] = APPPATH."/".$classData["dir"];
        if (!file_exists($classData["dir"])) {
            throw new \Exception("Not find file {$classData["dir"]} for class {$classData["class"]}");
        }
        self::require_once($classData["dir"]);
        return $this;
    }

    private function matchClasses(string $class)
    {
        foreach ($this->classesRegistered as $namespace => $fileDir) {
            if ($class === $namespace) {
                if (!Text::endsWith($fileDir, ".php")) $fileDir .= ".php";
                return [
                    "dir" => $fileDir,
                    "class" => $class,
                    "namespaceRegistered" => $namespace
                ];
            }
        }
        return false;
    }

    private function matchNamespace(string $class)
    {
        foreach ($this->namespacesRegistered as $namespace => $fileDir) {
            if (Text::startsWith($class, $namespace)) {
                $fileDir = strtr($class, [$namespace => $fileDir]);
                $fileDir = strtr($fileDir, ["\\" => "/"]);
                $fileDir .= ".php";
                return [
                    "dir" => $fileDir,
                    "class" => $class,
                    "namespaceRegistered" => $namespace
                ];
            }
        }
        return false;
    }

    public function registerClass(array $listClasses)
    {
        foreach ($listClasses as $class => $fileDir) {
            $this->sanitizeNamespace($class);
            $this->sanitizeDirectory($fileDir);
            $this->classesRegistered[$class] = $fileDir;
        }
        ksort($this->classesRegistered);
        $this->classesRegistered = array_reverse($this->classesRegistered);
    }

    public function registerNamespaces(array $listNamespaces)
    {
        foreach ($listNamespaces as $namespace => $dir) {
            $this->sanitizeNamespace($namespace);
            $this->sanitizeDirectory($dir);
            $this->namespacesRegistered[$namespace] = $dir;
        }
        ksort($this->namespacesRegistered);
        $this->namespacesRegistered = array_reverse($this->namespacesRegistered);
    }

    public function getNamespaces()
    {
        return $this->namespacesRegistered;
    }

    public function getClasses()
    {
        return $this->classesRegistered;
    }

    public function getNamespaceController(string $controller = null)
    {
        return $this->namespaceController.($controller ? "\\".$controller : null);
    }

    public function getNamespaceModel(string $model = null)
    {
        return $this->namespaceModel.($model ? "\\".$model : null);
    }

    public function getInstanceController(string $controller)
    {
        $class = $this->getNamespaceController($controller);
        return new $class();
    }

    public function getInstanceModel(string $model)
    {
        $class = $this->getNamespaceModel($model);
        return new $class();
    }

    public function getPathFileClass(string $class)
    {
        $matched = $this->matchClasses($class) ?: $this->matchNamespace($class);
        if (!isset($matched["dir"])) return null;
        if (!Text::startsWith($matched["dir"], APPPATH)) $matched["dir"] = APPPATH."/".$matched["dir"];
        self::sanitizeDirectory($matched["dir"]);
        return $matched["dir"];
    }

    public function setDefaultNamespaceController(string $namespaceController)
    {
        $this->sanitizeNamespace($namespaceController);
        if (!isset($this->namespacesRegistered[$namespaceController])) return false;
        $this->namespaceController = $namespaceController;
        return $this;
    }

    public function setDefaultNamespaceModel(string $namespaceModel)
    {
        $this->sanitizeNamespace($namespaceModel);
        if (!isset($this->namespacesRegistered[$namespaceModel])) return false;
        $this->namespaceModel = $namespaceModel;
        return $this;
    }

    static private function sanitizeNamespace(string &$namespace)
    {
        if (Text::startsWith($namespace, "\\")) Text::unsetChar($namespace, ":first");
        if (Text::endsWith($namespace, "\\")) Text::unsetChar($namespace, ":last");
        Text::unsetDuplicates($namespace, ":backslash");
    }

    static private function sanitizeDirectory(string &$dir)
    {
        Text::unsetDuplicates($dir, ":slash");
    }

    static public function require_once(string $file)
    {
        if (!Text::startsWith($file, APPPATH)) $file = APPPATH."/".$file;
        self::sanitizeDirectory($file);
        require_once($file);
    }

    static public function require(string $file)
    {
        if (!Text::startsWith($file, APPPATH)) $file = APPPATH."/".$file;
        self::sanitizeDirectory($file);
        require($file);
    }

    static public function include_once(string $file)
    {
        if (!Text::startsWith($file, APPPATH)) $file = APPPATH."/".$file;
        self::sanitizeDirectory($file);
        include_once($file);
    }
    
    static public function include(string $file)
    {
        if (!Text::startsWith($file, APPPATH)) $file = APPPATH."/".$file;
        self::sanitizeDirectory($file);
        include($file);
    }

    static public function getInstance()
    {
        if (empty(self::$instance)) self::$instance = new Loader();
        return self::$instance;
    }
}