<?php

namespace ES3;

use ES3\Validations;

class GET
{
    const TYPE_STR = "str";
    const TYPE_INT = "int";
    const TYPE_FLOAT = "float";
    const TYPE_BOOL = "bool";
    
    static public function get(String $key, String $type = null)
    {
        if ($type) {
            if ($type == self::TYPE_STR) {
                return self::get($key);
            } elseif ($type == self::TYPE_INT) {
                return self::getInt($key);
            } elseif ($type == self::TYPE_BOOL) {
                return self::getBool($key);
            } elseif ($type == self::TYPE_FLOAT) {
                return self::getFloat($key);
            }
        } else {
            return $_GET[$key] ?? null;
        }
    }

    static public function getInt(String $key)
    {
        return Validations::isValidInt(self::get($key)) ? (int)self::get($key) : null;
    }

    static public function getBool(String $key)
    {
        $value = trim(strtolower(self::get($key)));
        if (in_array($value, ["0", "null", "false", ""])) return false;
        return true;
    }

    static public function getFloat(String $key)
    {
        return Validations::isValidFloat(self::get($key)) ? (float)self::get($key) : null;
    }

    static public function getArr(Array $valueList = null)
    {
        if ($valueList) {
            foreach ($valueList as $key => $value) {
                if ($value == self::TYPE_STR) {
                    $valueList[$key] = self::get($key);
                } elseif ($value == self::TYPE_INT) {
                    $valueList[$key] = self::getInt($key);
                } elseif ($value == self::TYPE_BOOL) {
                    $valueList[$key] = self::getBool($key);
                } elseif ($value == self::TYPE_FLOAT) {
                    $valueList[$key] = self::getFloat($key);
                }
            }
            return $valueList;
        }
        return $_POST;
    }
}