<?php

namespace ES3;

class Response
{
    const TYPE_SEND_JSON = "JSON";
    const TYPE_SEND_OBJECT = "OBJECT";
    const TYPE_SEND_XML = "XML";
    const TYPE_SEND_HTML = "HTML";
    
    private $response;
    static private $responseTemplate = [
        "status" => false,
        "code" => "",
        "msg" => "",
        "data" => ""
    ];

    public function __construct(Array $resp = [])
    {
        $this->response = self::$responseTemplate;
        if ($resp) foreach ($resp as $key => $val) {
            $this->response[$key] = $val;
        }
    }

    public function __set($key, $val)
    {
        $this->response[$key] = $val; 
    }

    public function __get($key)
    {
        if(isset($this->response[$key])) return $this->response[$key];
    }

    /**
     * Realiza o envio da responsta da instancia
     *
     * @param string $type
     * @return void
     */
    private function sendInstance(string $type = self::TYPE_SEND_JSON)
    {
        header("Contet-type: application/json");
        echo json_encode($this->response);
        die();
    }

    
    /**
     * Realiza o envio da resposta de forma estática
     *
     * @param Array $response
     * @param string $type
     * @return void
     */
    static private function sendStatic(Array $response, string $type = self::TYPE_SEND_JSON)
    {
        $response = array_merge(self::$responseTemplate, $response);
        header("Contet-type: application/json");
        echo json_encode($response);
        die();
    }

    /**
     * Caso seja invocado o "send" de uma isntancia
     *
     * @param String $method
     * @param mixed $arguments
     * @return void
     */
    public function __call($method, $arguments) 
    {
        if ($method == "send") call_user_func_array([$this, "sendInstance"], $arguments);
    }

    /**
     * Caso seja invocado o "send" de forma estatica
     *
     * @param String $method
     * @param mixed $arguments
     * @return void
     */
    public static function __callStatic($method, $arguments)
    {
        if ($method == "send") call_user_func_array("ES3\\Response::sendStatic", $arguments);
    }

    public function set($arg1, $arg2 = null)
    {
        if (is_string($arg1)) {
            $this->response[$arg1] = $arg2;
        } elseif (is_array($arg1)) {
            $this->response = array_merge($this->response, $arg1);
        }
        return $this;
    }

    public function get($key = null)
    {
        return $key ? $this->response[$key] : $this->response;
    }
}   