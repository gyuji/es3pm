<?php

namespace ES3;

class Utils
{
    static private $headers = [];

    static public function arrayToObject($arr)
    {
        $obj = new \stdClass;
        foreach($arr as $k => $v) {
            if(strlen($k)) {
                if(is_array($v)) {
                    $obj->{$k} = array_to_object($v); //RECURSION
                } else {
                    $obj->{$k} = $v;
                }
            }
        }
        return $obj; 
    }

    static public function isCNPJ(String $str)
	{
		return strlen($str) == 14;
	}

    static public function isCPF(String $str)
	{
		return strlen($str) == 11;
    }
    
    static public function dateTimeFormat(String $str = null)
    {
        if ($str === null) return date("d/m/Y H:i");
        if (is_numeric($str)) return date("d/m/Y H:i", $str);
        return date("d/m/Y H:i", strtotime($str));
    }

    static public function dateFormat(String $str = null)
    {   
        if ($str === null) return date("d/m/Y");
        if (is_numeric($str)) return date("d/m/Y", $str);
        return date("d/m/Y", strtotime($str));
    }

    static public function now()
    {
        return date("Y-m-d H:i:s");
    }

    static public function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for($i = 0; $i <= strlen($mask)-1; $i++) {
            if($mask[$i] == '#' || $mask[$i] == '9') {
                if(isset($val[$k])) $maskared .= $val[$k++];
            } else {
                if(isset($mask[$i])) $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    static public function readCsvStr(String $text, String $delim = ";")
    {
        $data = str_getcsv($text, "\n"); //parse the rows 
        foreach($data as $i => &$row) {
            $isNull = true;
            $row = str_getcsv($row, ";", '"'); //parse the items in rows 
            foreach ($row as $j => &$cel) {
                $cel = trim($cel);
                if ($cel) $isNull = false;
            }
            if ($isNull) unset($data[$i]);
        }
        return $data;
    }

    static public function setHeaderCSV(array $csv, array $header)
    {
        if (!$csv || !$header) return false;
        $objContent = [];
        foreach ($csv as $nb => $row) {
            $objContent[$nb] = (object)[];
            foreach ($header as $i => $column) {
                $objContent[$nb]->$column = $row[$i];
            }
        }
        $objContent = array_values($objContent);
        return $objContent;
    }

    static public function getHeader(String $key = null)
    {
        if (empty(self::$headers)) {
            foreach (getallheaders() as $key => $value) {
                self::$headers[$key] = $value;
            }
        }
        return $key ? (self::$headers[$key] ?? null) : self::$headers;
    }
}   