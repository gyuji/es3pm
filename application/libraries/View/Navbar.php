<?php

namespace ES3\View;

class Navbar
{
    private $structure;
    
    public function __construct()
    {
        $this->structure = new \stdClass();
    }

    public function getMenuStructure()
    {
        return $this->structure;
    }

    /**
     * Adicionar um item de nível um
     *
     * @param String $position first|last|after $id
     * @param Item $item
     * @return void
     */
    public function addItem(Item $item, String $position = "last")
    {
        if ($position == "last") {
            $this->structure->{$item->id} = $item;
        }

        return $this;
    }

    public function addSubitem(String $id, Subitem $subitem)
    {
        if (!isset($this->structure->$id)) return $this;
        $this->structure->$id->addSubitem($subitem);
        return $this;
    }

    /**
     * Retorna um item ou todos
     *
     * @param String $id
     */
    public function getItem(String $id = null)
    {
        return $id && isset($this->structure->$id) ? $this->structure->$id : $this->structure;
    }
}