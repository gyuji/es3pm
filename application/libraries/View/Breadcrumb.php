<?php

namespace ES3\View;

use ES3\Text;

class Breadcrumb
{
    private $pageDesc;
    private $crumb = [];
    private $urlReturn = null;
    private $topAlertConfig = [];

    /**
     * @param String $pageDesc
     */
    public function __construct(String $pageDesc = null)
    {
        if ($pageDesc) $this->setPageDesc($pageDesc);

        $sessFlashTopAlert = get_instance()->session->flashdata('Breadcrumb::topAlert');
        if ($sessFlashTopAlert) {
            $this->topAlertConfig[] = (object)$sessFlashTopAlert;
        }
    }

    /**
     * Set a descrição da página
     *
     * @param String $pageDesc
     * @return Breadcrumb
     */
    public function setPageDesc(String $pageDesc)
    {
        $this->pageDesc = $pageDesc;
        return $this;
    }

    public function setButtonReturn(String $url)
    {
        $this->urlReturn = $url ? (Text::startsWith($url, "http") ? $url : base_url($url)) : null;
        return $this;
    }

    public function getButtonReturn()
    {
        return $this->urlReturn;
    }

    public function setTopAlert(String $type, String $msg)
    {
        $this->topAlertConfig[] = (object)[
            "type" => $type,
            "msg" => $msg
        ];
        return $this;
    }

    public function getTopAlert()
    {
        return $this->topAlertConfig;
    }

    /**
     * Returna a descrição da página
     *
     * @return String|null
     */
    public function getPageDesc()
    {
        return $this->pageDesc;
    }

    /**
     * Adiciona um crumb na lista
     *
     * @param Array|String $crumb
     * @param String $url
     * @return Breadcrumb
     */
    public function addCrumb($crumb, String $url = null)
    {
        if (is_string($crumb)) {
            $this->crumb[] = [
                "label" => trim($crumb),
                "url" => $url ? (Text::startsWith($url, "http") ? $url : base_url($url)) : null
            ];
        } elseif (is_array($crumb)) {
            foreach ($crumb as $value) {
                if (is_array($value)) $this->addCrumb($value["label"] ?? null, $value["url"] ?? null);
                elseif (is_string($value)) $this->addCrumb($value);
            }
        }
        return $this;
    }

    /**
     * Retorna o html em li do breadcrumb
     *
     * @return String
     */
    public function getHtmlCrumb()
    {
        $html = "";
        $nbCrumb = count($this->crumb) - 1;
        foreach ($this->crumb as $index => $crumb) {
            $html .= "<li class=\"".($nbCrumb == $index ? "active" : null)."\">
                <a href=\"{$crumb["url"]}\">
                    ".($nbCrumb == $index ? "<strong>{$crumb["label"]}</strong>" : $crumb["label"])."
                </a>
            </li>";
        }
        return $html;
    }
}