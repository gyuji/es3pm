<?php

namespace ES3\View;

use ES3\Text;
use ES3\Services\Auth as SAuth;

class View
{
    static private $cssUrls = [];
    static private $jsUrls = [];

    private $CI;
    private $viewBag;
    private $viewParams;
    private $viewName;
    private $_Controller;
    private $_Action;
    private $menu;
    private $breadcrumb;
    private $forms = [];
    
    public function __construct(string $view = "index")
    {
        $this->CI = &get_instance();
        $this->_Controller = $this->CI->router->class;
        $this->_Action = $this->CI->router->method;
        $this->viewName = $view;
        $this->viewParams = [
            "_Controller" => &$this->_Controller,
            "_Action" => &$this->_Action,
            "_View" => &$this->viewName,
            "thisView" => &$this
        ];
        $this->viewBag = [
            "ngApp" => "es3pm",
            "ngCtrMain" => "mainCtr",
            "ngCtrSec" => null,
            "baseURL" => base_url()
        ];
    }
    
    /**
     * Set viewbag
     * 
     * @param [Array or String] $arg1
     * @param string $arg2
     * @return void
     */
    public function setVB($arg1, $arg2 = null)
    {
        if (is_array($arg1)) {
            foreach ($arg1 as $key => $val) $this->viewBag[$key] = $val;
            return $this;
        } else if (is_string($arg1)) {
            $this->viewBag[$arg1] = $arg2;
            return $this;
        }
    }

    /**
     * Set viewbag
     * 
     * @param [Array or String] $arg1
     * @param string $arg2
     * @return void
     */
    public function set($arg1, $arg2 = null)
    {
        if (is_array($arg1)) {
            foreach ($arg1 as $key => $val) $this->viewParams[$key] = $val;
            return $this;
        } else if (is_string($arg1)) {
            $this->viewParams[$arg1] = $arg2;
            return $this;
        }
    }

    /**
     * Retorna o valor de vb indexado em key ou todos os valores de vb
     * 
     * @param string $key
     *
     * @return mixed
     */
    public function getVB(string $key = null)
    {
        return $key ? ($this->viewBag->$key ?? null) : $this->viewBag;
    }
    
    /**
     * Retorna o valor de viewparam indexado em key ou todos os valores de vb
     * 
     * @param String $key
     *
     * @return mixed
     */
    public function get(String $key = null)
    {
        return $key ? ($this->viewParams[$key] ?? null) : $this->viewParams;
    }

    /**
     * Remove VB
     *
     * @param String $key
     * @return void
     */
    public function removeVB(String $key)
    {
        if (isset($this->viewBag->$key)) unset($this->viewBag->$key);
        return $this;
    }

    /**
     * Remove viewParams
     *
     * @param string $key
     * @return void
     */
    public function remove(string $key)
    {
        if (isset($this->viewParams[$key])) unset($this->viewParams[$key]);
        return $this;
    }

    /**
     * Load view
     *
     * @param String $type user|guest 
     * @param String $view
     * @return void
     */
    public function load(string $view = null, Array $data = null)
    {
        self::addStyle([
            "bootstrap.min",
            "../font-awesome/css/font-awesome",
            "plugins/iCheck/custom",
            "plugins/switchery/switchery",
            "plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox",
            "plugins/sweetalert/sweetalert",
            "plugins/dropzone/basic",
            "plugins/jasny/jasny-bootstrap.min",
            "animate",
            "style",
            "es3style"
        ]);
        self::addJavascript([
            "jquery-3.1.1.min",
            "bootstrap.min",
            "plugins/metisMenu/jquery.metisMenu",
            "plugins/slimscroll/jquery.slimscroll.min",
            "plugins/jasny/jasny-bootstrap.min",
            "plugins/switchery/switchery",
            "plugins/sweetalert/sweetalert.min",
            "inspinia",
            "plugins/pace/pace.min",
            "plugins/iCheck/icheck.min",
            "angular",
            "plugins/ng-file-upload/ng-file-upload.min",
            "ngapp",
            "ngdirectives",
            "ngcontrollers/ctrMain",
            "plugins/switchery/ng-switchery"
        ]);
        
        if ($data) {
            $this->set($data);
            $this->setVB($data);
        }
        
        if ($this->get("ngCtrSec")) self::addJavascript([
            "/ngcontrollers/".$this->get("ngCtrSec")
        ]);
        if ($view) $this->viewName = $view;
        if (!empty($this->menu)) $this->set("menu", $this->menu);
        if (!empty($this->breadcrumb)) $this->set("breadcrumb", $this->breadcrumb);
        if (!empty($this->forms)) {
            foreach ($this->forms as $form) {
                $this->set($form->fields);
                $this->set($form->getName(), $form);
                $this->setVB($form->getName(), $form->fields);
                if (method_exists($form, "onLoadView")) $form->onLoadview($this);
            }
        }
        if (SAuth::isAuth()) {
            $this->set("userAuth", SAuth::getInstance()->getUserAuth());
        }
        $this->set("VB", $this->getVB());

        $this->CI->load->view($this->viewName, $this->get());
    }

    /**
     * Carregda uma partial em /views/includes
     *
     * @param String $includes
     * @return void
     */
    public function includes(String $includes, Array $data = null)
    {
        if ($data) $this->set($data);
        $includes = (Text::endsWith($includes, ".php") ? $includes : $includes.".php");
        if (file_exists(APPPATH."views/includes/$includes")) {
            $this->CI->load->view("includes/$includes", $this->get());
        }
    }

    public function &menu(Navbar $navbar = null)
    {
        if (empty($this->menu)) $this->menu = $navbar ?: new Navbar();
        return $this->menu;
    }

    public function breadcrumb(Breadcrumb $breadcrumb = null)
    {
        if (empty($this->breadcrumb)) $this->breadcrumb = $breadcrumb ?: new Breadcrumb();
        return $this->breadcrumb;
    }

    public function isGuest()
    {
        return empty($this->menu);
    }

    public function setForm($form)
    {
        $this->forms[] = $form;
        return $this;
    }

    public function getForms()
    {
        return $this->forms;
    }

    public function jsonVB()
    {
        return json_encode($this->viewBag, JSON_NUMERIC_CHECK);
    }

    /**
     * @param [String | Array] $cssFile
     */
    static public function addStyle($cssFile)
    {
        if (is_string($cssFile)) {
            if (Text::startsWith($cssFile, "http")) self::$cssUrls[] = $cssFile;
            else self::$cssUrls[] = base_url("/assets/css/".(Text::endsWith($cssFile, ".css") ? $cssFile : $cssFile.".css"));
        } elseif (is_array($cssFile)) {
            foreach ($cssFile as $value) {
                self::addStyle($value);
            }
        }
    }

    /**
     * @param [String | Array] $jsFile
     */
    static public function addJavascript($jsFile)
    {
        if (is_string($jsFile)) {
            if (Text::startsWith($jsFile, "http")) self::$jsUrls[] = $jsFile;
            else self::$jsUrls[] = base_url("/assets/js/".( Text::endsWith($jsFile, ".js") ? $jsFile : $jsFile.".js"));
        } elseif (is_array($jsFile)) {
            foreach ($jsFile as $value) {
                self::addJavascript($value);
            }
        }
    }

    static public function printStyle()
    {
        foreach (self::$cssUrls as $url) {
            echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$url\">\n";
        }
    }

    static public function printJsavascript()
    {
        foreach (self::$jsUrls as $url) {
            echo "<script src=\"$url\"></script>\n";
        }
    }
}   