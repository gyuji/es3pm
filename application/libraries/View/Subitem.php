<?php

namespace ES3\View;

use ES3\Text;

class Subitem
{
    private $label;
    private $href;

    public function __construct(Array $item = null)
    {
        if ($item) foreach ($item as $key => $value) $this->__set($key, $value);
    }

    public function __get($name)
    {
        return $this->$name ?? null;
    }

    public function __set($name, String $value)
    {
        switch ($name) {
            case ("label"):
                $this->label = $value;
                break;
            case ("href"):
                if (!Text::startsWith($value, "http")) $value = base_url($value);
                $this->href = $value;
                break;
        }
    }
}