<?php

namespace ES3\View;

use ES3\Text;

class Item
{
    const SINGLE = 1;
    const DROPDOWN = 2;

    private $htmlid;
    private $type;
    private $label;
    private $href;
    private $icon;
    private $subitems = [];
    private $active = false;

    public function __construct(Array $item = null)
    {
        if ($item) foreach ($item as $key => $value) $this->__set($key, $value);
    }

    public function __get($name)
    {
        return $this->$name ?? null;
    }

    public function __set($name, $value)
    {
        switch ($name) {
            case ("id"):
                $this->id = $value;
                $this->htmlid = "menu_item_$value";
                break;
            case ("type"):
                if ($value != self::DROPDOWN && $value != self::SINGLE) $value = self::SINGLE;
                $this->type = (int)$value;
                break;
            case ("label"):
                $this->label = $value;
                break;
            case ("href"):
                if (!Text::startsWith($value, "http")) $value = base_url($value);
                $this->href = $value;
                break;
            case ("icon"):
                $this->icon = $value;
                break;
            case ("subitems"):
                $this->subitems[] = $value;
                break;
            case ("active"):
                $this->active = (bool)$value;
                break;
        }
    }

    /**
     * Adicioanr subitem
     *
     * @param Subitem $subitem
     * @return void
     */
    public function addSubitem(Subitem $subitem)
    {
        if ($this->type != self::DROPDOWN) $this->type = self::DROPDOWN;
        $this->subitems[] = $subitem;
    }

    /**
     * Returna uma lista de subitens
     *
     * @return Array|null
     */
    public function getSubitem()
    {
        return $this->subitems ?? null;
    }
}