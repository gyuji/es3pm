<?php

namespace ES3;

class Validations
{
    static public function companyNameIsValid($RazaoSocial)
	{
        if ($RazaoSocial != null) return true;
        else return false;
	}

	static public function cnpjIsValid($cnpj)
	{
        $cnpj = preg_replace('/[^0-9]/', '', (string)$cnpj);
        if (in_array($cnpj, [
            "00000000000000",
            "11111111111111",
            "22222222222222",
            "33333333333333",
            "44444444444444",
            "55555555555555",
            "66666666666666",
            "77777777777777",
            "88888888888888",
            "99999999999999"
        ])) return false;
        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;
        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
    
    static public function cpfIsValid($cpf)
    {
        $cpf = preg_replace('/[^0-9]/', '', (string) $cpf);
        if (in_array($cpf, [
            "00000000000",
            "11111111111",
            "22222222222",
            "33333333333",
            "44444444444",
            "55555555555",
            "66666666666",
            "77777777777",
            "88888888888",
            "99999999999"
        ])) return false;
        // Valida tamanho
        if (strlen($cpf) != 11)
            return false;
        // Calcula e confere primeiro dígito verificador
        for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--)
            $soma += $cpf{$i} * $j;
        $resto = $soma % 11;
        if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        // Calcula e confere segundo dígito verificador
        for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--)
            $soma += $cpf{$i} * $j;
        $resto = $soma % 11;
        return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
    }

    static public function limitExecutionIsValid($LimiteExecucoes)
    {
        if (is_numeric($LimiteExecucoes) &&
            $LimiteExecucoes >= 0 &&
            $LimiteExecucoes <= 100) return true;
        else return false;
    }

    static public function limitRepetitionIsValid($LimiteRepeticoes)
    {
        if (is_numeric($LimiteRepeticoes) && 
            $LimiteRepeticoes >= 0 &&
            $LimiteRepeticoes <= 100) return true;
        else return false;
    }

    static public function isValidEmail(String $email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    static public function isValidInt($value)
    {
        return filter_var($value, FILTER_VALIDATE_INT);
    }

    static public function isValidFloat($value)
    {
        return filter_var($value, FILTER_VALIDATE_FLOAT);
    }

    static public function isIP(String $IP)
    {
        return filter_var($IP, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    static public function isDomain(String $domain)
    {
        preg_match('/^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/', $domain, $out);
        return ($out[0] ?? null) === $domain;
    }

    static public function checkResolveDomain(String $domain, String $type = "A")
    {
        $type = strtoupper($type);
        return checkdnsrr($domain, $type);
    }
}   