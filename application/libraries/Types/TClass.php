<?php

namespace ES3\Types;

use ES3\Loader;

class TClass
{
    const CATEGORY_CONTROLLER = "controller";
    const CATEGORY_MODEL = "model";

    private $loader;
    private $class;
    private $classFile;

    private $instance;

    public function __construct(string $class, string $category = null)
    {
        $this->loader = Loader::getInstance();
        $category = strtolower($category);
        if ($category == self::CATEGORY_MODEL) {
            $this->class = $this->loader->getNamespaceModel($class);
        } elseif ($category == self::CATEGORY_CONTROLLER) {
            $this->class = $this->loader->getNamespaceController($class);
        } else {
            $this->class = $class;
        }

        $this->classFile = $this->loader->getPathFileClass($this->class);
    }

    public function exists()
    {
        return class_exists($this->class);
    }

    public function fileExists()
    {
        return file_exists($this->classFile);
    }

    public function methodExists(string $method)
    {
        return method_exists($this->class, $method);
    }

    public function isCallable(string $method)
    {
        return is_callable([$this->class, $method]);
    }

    public function getInstance()
    {
        if (empty($this->instance)) $this->instance = new $this->class();
        return $this->instance;
    }

    public function getNameClass()
    {
        return $this->class;
    }

    public function getClassFile()
    {
        return $this->classFile;
    }

    public function include()
    {
        if ($this->classFile) $this->loader::include($this->classFile);
    }

    public function require()
    {
        if ($this->classFile) $this->loader::require($this->classFile);
    }
}