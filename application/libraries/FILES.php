<?php

namespace ES3;

use ES3\Validations;
use ES3\Utils;

class FILES
{
    public $name;
    public $type;
    public $tmpName;
    public $error;
    public $size;
    public $content;
    public $contentAsCsv;

    public function __construct(array $fileVar)
    {
        $this->name = isset($fileVar["name"]) ? (is_array($fileVar["name"]) ? ($fileVar["name"]["value"] ?? null) : $fileVar["name"]) : null;
        $this->type = isset($fileVar["type"]) ? (is_array($fileVar["type"]) ? ($fileVar["type"]["value"] ?? null) : $fileVar["type"]) : null;
        $this->tmpName = isset($fileVar["tmp_name"]) ? (is_array($fileVar["tmp_name"]) ? ($fileVar["tmp_name"]["value"] ?? null) : $fileVar["tmp_name"]) : null;
        $this->error = isset($fileVar["error"]) ? (is_array($fileVar["error"]) ? ($fileVar["error"]["value"] ?? null) : $fileVar["error"]) : null;
        $this->size = isset($fileVar["size"]) ? (is_array($fileVar["size"]) ? ($fileVar["size"]["value"] ?? null) : $fileVar["size"]) : null;
    }

    public function getContentTmp()
    {
        if (empty($this->content)) $this->content = file_exists($this->tmpName) ? file_get_contents($this->tmpName) : null;
        return $this->content;
    }

    public function getContentAsCSV()
    {
        if (empty($this->content)) $this->getContentTmp();
        if (!$this->content) return null;
        $this->content = Utils::readCsvStr($this->content);
        return $this->content;
    }

    public function setHeaderCSV(array $header)
    {
        if (empty($this->content) || !is_array($this->content)) $this->getContentAsCSV();
        if (!$this->content) return null;
        $this->content = Utils::setHeaderCSV($this->content, $header);
        return $this->content;
    }

    static public function get($key)
    {
        return isset($_FILES[$key]) ? new self($_FILES[$key]) : null;
    }
}