<?php

namespace ES3\Forms;

use ES3\Validations;

class FormCreateDomain extends Form
{
    public function loadFields()
    {
        $this->setName("formCreateDomain");
        $this->fields["IDEmpresas"] = (new Field("IDEmpresas"))
            ->set("label", "Empresa")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_HIDDEN)
            ->set("placeholder", "Empresa")
            ->set("minnum", 1)
            ->set("required", true);
        $this->fields["Endereco"] = (new Field("Endereco"))
            ->set("label", "Domínio")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_DOMAIN)
            ->set("placeholder", "Domínio")
            ->set("maxlen", 100)
            ->set("minlen", 5)
            ->set("required", true);
    }

    protected function onParseValue_Endereco(Field &$field)
    {
        $field->value = strtolower($field->value);
    }

    public function posValidate_Endereco(Field &$field)
    {
        if (!Validations::checkResolveDomain($field->value)) {
            $this->fieldValidation->set("Endereco", "Endereco:notresolvedns", "Não é possível resolver o endereço {$field->label} ");
            return false;
        }
        return true;
    }
}