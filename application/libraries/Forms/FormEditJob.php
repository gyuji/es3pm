<?php

namespace ES3\Forms;

use ES3\Models\Jobs;

class FormEditJob extends Form
{
    public $btnRuning = false;
    public $btnStop = false;
    public $btnCancel = false;
    public $btnFinish = false;

    public function loadFields()
    {
        $this->setName("formEditJob");
        $this->fields["ID"] = (new Field("ID"))
            ->set("label", "ID")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["Status"] = (new Field("Status"))
            ->set("label", "Status")
            ->set("typeValue", Form::TYPE_STR)
            ->set("required", true);
        $this->fields["IDEmpresas"] = (new Field("IDEmpresas"))
            ->set("label", "Empresa")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["IDDominios"] = (new Field("IDDominios"))
            ->set("label", "Domínio")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["IDServidores"] = (new Field("IDServidores"))
            ->set("label", "Servidor")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["Repeticoes"] = (new Field("Repeticoes"))
            ->set("label", "Número de repeticoes")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("minnum", 1)
            ->set("value", 1)
            ->set("required", true);
        $this->fields["Execucoes"] = (new Field("Execucoes"))
            ->set("label", "Número de execições simultâneas")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("minnum", 1)
            ->set("value", 1)
            ->set("required", true);
    }

    public function onFill_Status(Field &$field, $data)
    {
        $field->value = $data->Status;
        switch ($data->Status) {
            case Jobs::AGUARDANDO:
                $this->btnRuning = true;
                break;
            case Jobs::CANCELADO:
                break;
            case Jobs::EXECUTANDO:
                $this->btnCancel = true;
                $this->btnStop = true;
                $this->btnFinish = true;
                break;
            case Jobs::CONCLUIDO:
                break;
            case Jobs::PAUSADO:
                $this->btnCancel = true;
                $this->btnFinish = true;
                $this->btnRuning = true;
                break;
        }
    }
}