<?php

namespace ES3\Forms;

class FormLogin extends Form
{
    public function loadFields()
    {
        $this->setName("formLogin");
        $this->fields["username"] = (new Field("username"))
            ->set("label", "E-mail")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_EMAIL)
            ->set("maxlen", 200)
            ->set("minlen", 5)
            ->set("required", true)
            ->set("disabled", false);

        $this->fields["password"] = (new Field("password"))
            ->set("label", "Senha")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_PASSWORD)
            ->set("maxlen", 200)
            ->set("minlen", 6)
            ->set("required", true);
    }

    protected function onParseValue_username(Field &$field)
    {
        $field->value = strtolower($field->value);
    }
}