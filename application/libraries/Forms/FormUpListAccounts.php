<?php

namespace ES3\Forms;

use ES3\View\View;
use ES3\Utils;

class FormUpListAccounts extends Form
{
    public $accList = [];

    public function loadFields()
    {
        $this->setName("formUpListAccounts");
        $this->fields["csvFile"] = (new Field("csvFile"))
            ->set("label", "Arquivo de contas")
            ->set("typeValue", Form::TYPE_FILE)
            ->set("typeInput", Form::TYPE_INPUT_FILE)
            ->set("validationFile", (object)[
                "size" => (object)[
                    "max" => "500KB"
                ],
                "pattern" => [".csv", ".txt"]
            ]);
        $this->fields["csvText"] = (new Field("csvText"))
            ->set("label", "Lista de contas (csv)")
            ->set("typeValue", Form::TYPE_CSV)
            ->set("typeInput", Form::TYPE_INPUT_TEXTAREA);
    }

    public function onLoadView(View &$view)
    {
        $view::addStyle([
            "plugins/codemirror/codemirror"
        ]);
        $view::addJavascript([
            "plugins/codemirror/codemirror",
            "plugins/codemirror/mode/xml/xml"
        ]);
    }

    public function onParseValue_csvFile(Field &$field)
    {
        $field->value = $field->value ? $field->value->setHeaderCSV([
            "addrOri", "passOri", "addrDest", "passDest"
        ]) : null;
    }

    public function onParseValue_csvText(Field &$field)
    {
        $field->value = $field->value ? Utils::setHeaderCSV($field->value, ["addrOrig", "passOrig", "addrDest", "passDest"]) : null;
    }

    public function posValidate_csvText(Field &$field)
    {
        if (!$field->value && !$this->csvFile->value) {
            $this->fieldValidation->set("csvText", "required", "Realize o upload de algum arquivo ou preencha o campo de texto com a lista de contas");
            return false;
        }
        if ($this->csvFile->value) foreach ($this->csvFile->value as $value) {
            $this->accList[] = $value;
        }
        if ($field->value) foreach ($field->value as $value) {
            $this->accList[] = $value;
        }
        return true;
    }
}