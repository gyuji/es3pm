<?php

namespace ES3\Forms;

use ES3\View\View;
use ES3\Validations;

class FormCreateServer extends Form
{
    public function loadFields()
    {
        $this->setName("formCreateServer");
    

        $this->fields["Apelido"] = (new Field("Apelido"))
            ->set("label", "Apelido")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "Apelido do Servidor")
            ->set("required", true);

        $this->fields["Endereco"] = (new Field("Endereco"))
            ->set("label", "Endereço")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_IP)
            ->set("placeholder", "Endereço")
            ->set("maxlen", 15)
            ->set("minlen", 7)
            ->set("required", true);

        $this->fields["Porta"] = (new Field("Porta"))
            ->set("label", "Porta")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("placeholder", "Porta")
            ->set("maxnum", 999)
            ->set("minnum", 100)
            ->set("required", true);

        $this->fields["Map"] = (new Field("Map"))
            ->set("label", "Template de Mapeamento")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "#Insira aqui o template de mapeamento");

        $this->fields["Seguranca"] = (new Field("Seguranca"))
            ->set("label", "Segurança")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_SELECT)
            ->set("placeholder", "Segurança")
            ->set("maxlen", 18)
            ->set("options", [
                ["label" => "Nenhum", "value" => "NONE"],
                ["label" => "SSL", "value" => "SSL"],
                ["label" => "TLS", "value" => "TLS"]
            ]);
    }

    public function onLoadView(View &$View)
    {
        $View::addJavascript("plugins/jasny/jasny-bootstrap.min");
    }

    public function onParseValue_Cnpj(Field &$field)
    {
        $field->value = preg_replace('/[^0-9]/', '', (string)$field->value);
    }

    public function onParseValue_Cpf(Field &$field)
    {
        $field->value = preg_replace('/[^0-9]/', '', (string)$field->value);
    }

    public function posValidate_Seguranca(Field $field)
    {
        if (!in_array($this->Seguranca->value, ["NONE", "SSL", "TLS"])) {
            $this->fieldValidation->set($field->name, "invalid::seguranca", "Protocolo de Segurança inserido está incorreto.");
            return false;
        }
    }
}