<?php

namespace ES3\Forms;

class Field
{
    public $label;
    public $name;
    public $placeholder;
    public $typeValue;
    public $typeInput;
    public $value;
    public $maxlen;
    public $minlen;
    public $minnum;
    public $maxnum;
    public $stepnum;
    public $title;
    public $mask;
    public $options;
    public $hide;
    public $validationFile;
    public $required = false;
    public $disabled = false;

    public function __construct(String $name)
    {
        $this->name = $name;
    }

    public function set(String $key, $value = null)
    {
        $this->$key = $value;
        return $this;
    }

    public function html()
    {
        $this->getTypeInput()
            ->getPlaceholder()
            ->getMaxlength()
            ->getMinlength()
            ->getMinnum()
            ->getMaxnum()
            ->getStepnum()
            ->getTitle()
            ->getMask()
            ->isRequired()
            ->isDisabled()
            ->isHide();
        return $this;
    }

    public function isRequired()
    {
        if (!empty($this->required)) echo $this->required ? "required " : false;
        return $this;
    }

    public function isDisabled()
    {
        if (!empty($this->disabled)) echo $this->disabled ? "disabled=\"disabled\" " : false;
        return $this;
    }

    public function isHide()
    {
        if (!empty($this->hide)) echo $this->hide ? "ng-hide=\"{$this->hide}\" " : false;
        return $this;
    }

    public function getOptions(String $id = null)
    {
        if ($this->typeInput == Form::TYPE_INPUT_RADIO) {
            return $this->options[$id] ?? null;
        } elseif ($this->typeInput == Form::TYPE_INPUT_SELECT) {
            $html = "<select>";
            foreach ($this->options as $value => $label) {
                $html .= "<option ng-value=\"{$value}\" value=\"{$value}\">{$label}</option>\n";
            }
            $html .= "</select>";
            echo $html;
        }
    }

    public function getMask()
    {
        if (!empty($this->mask)) echo "data-mask=\"{$this->mask}\" ";
        return $this;
    }

    public function getPlaceholder()
    {
        if (!empty($this->placeholder)) echo "placeholder=\"{$this->placeholder}\" ";
        return $this;
    }

    public function getTypeInput()
    {
        if (!empty(explode(":", $this->typeInput)[0])) echo "type=\"{$this->typeInput}\" ";
        return $this;
    }

    public function getMaxlength()
    {
        if (!empty($this->maxlen)) echo "maxlength=\"{$this->maxlen}\" ";
        return $this;
    }

    public function getMinlength()
    {
        if (!empty($this->minlen)) echo "minlength=\"{$this->minlen}\" ";
        return $this;
    }

    public function getMinnum()
    {
        if($this->minnum !== null) echo "min=\"{$this->minnum}\" ";
        return $this;
    }

    public function getMaxnum()
    {
        if($this->maxnum !== null) echo "max=\"{$this->maxnum}\" ";
        return $this;
    }

    public function getStepnum()
    {
        if(!empty($this->stepnum)) echo "step=\"{$this->stepnum}\" ";
        return $this;
    }

    public function getTitle()
    {
        if(!empty($this->title)) echo "title=\"{$this->title}\" ";
        return $this;
    }

    public function getValidationFile()
    {
        if (!empty($this->validationFile)) {
            if ($this->validationFile->size->min ?? null) echo "ngf-min-size=\"{$this->validationFile->size->min}\" ";
            if ($this->validationFile->size->max ?? null) echo "ngf-max-size=\"{$this->validationFile->size->max}\" ";
            if ($this->validationFile->pattern ?? null) echo "ngf-pattern=\"".("'".implode(",", $this->validationFile->pattern)."'")."\" ";
        }
        return $this;
    }
}