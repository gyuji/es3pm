<?php

namespace ES3\Forms;

class FormChangeDisplayName extends Form
{
    public function loadFields()
    {
        $this->setName("formChangeDisplayName");
        $this->fields["displayname"] = (new Field("displayname"))
            ->set("label", "Nome de exibição")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "Nome de exibição")
            ->set("maxlen", 200)
            ->set("minlen", 3)
            ->set("required", true);
    }
}