<?php

namespace ES3\Forms;

use ES3\View\View;
use ES3\Utils;
use ES3\POST;
use ES3\Validations;

class FormEditAccounts extends Form
{
    public $accList = [];
    private $permitedDomains = [];

    public function loadFields()
    {
        $this->setName("formEditAccounts");
        $this->fields["index"] = (new Field("index"))
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["ID"] = (new Field("ID"))
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["addrOrig"] = (new Field("addrOrig"))
            ->set("label", "Endereço de origem")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_EMAIL)
            ->set("maxlen", 200)
            ->set("required", true);
        $this->fields["addrDest"] = (new Field("addrDest"))
            ->set("label", "Endereço de destino")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_EMAIL)
            ->set("maxlen", 200)
            ->set("required", true);
        $this->fields["passOrig"] = (new Field("passOrig"))
            ->set("label", "Senha de origem")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("maxlen", 200)
            ->set("required", true);
        $this->fields["passDest"] = (new Field("passDest"))
            ->set("label", "Senha de destino")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("maxlen", 200)
            ->set("required", true);
    }
}