<?php

namespace ES3\Forms;

class FormCreateUser extends Form
{
    public function loadFields()
    {
        $this->setName("formCreateUser");
        $this->fields["IDEmpresas"] = (new Field("IDEmpresas"))
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("minum", 1)
            ->set("required", true);
        $this->fields["displayname"] = (new Field("displayname"))
            ->set("label", "Nome de exibição")
            ->set("placeholder", "Nome de exibição")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("maxlen", 200)
            ->set("minlen", 3)
            ->set("required", true);
        $this->fields["username"] = (new Field("username"))
            ->set("label", "Endereço de E-mail")
            ->set("placeholder", "Endereço de E-mail")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_EMAIL)
            ->set("maxlen", 200)
            ->set("minlen", 5)
            ->set("required", true)
            ->set("disabled", false);
        $this->fields["password1"] = (new Field("password1"))
            ->set("label", "Senha")
            ->set("placeholder", "Senha")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_PASSWORD)
            ->set("required", true)
            ->set("maxlen", 200)
            ->set("minlen", 6);
        $this->fields["password2"] = (new Field("password2"))
            ->set("label", "Senha")
            ->set("placeholder", "Confirmar senha")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_PASSWORD)
            ->set("required", true)
            ->set("maxlen", 200)
            ->set("minlen", 6);
    }

    protected function onParseValue_username(Field &$field)
    {
        $field->value = strtolower($field->value);
    }

    public function posValidate_password2(Field &$field)
    {
        if ($field->value != $this->password1->value) {
            $this->fieldValidation->set("password2", "notequal", "As senhas não conferem");
            return false;
        }
        return true;
    }
}