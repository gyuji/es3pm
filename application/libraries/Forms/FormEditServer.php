<?php

namespace ES3\Forms;

use ES3\View\View;
use ES3\Validations;
use ES3\Utils;

class FormEditServer extends Form
{
    public function loadFields()
    {
        $this->setName("formEditServer");
        $this->fields["ID"] = (new Field("ID"))
            ->set("label", "ID do Servidor")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_HIDDEN)
            ->set("maxnum", 9999999)
            ->set("minnum", 2)
            ->set("required", true)
            ->set("disabled", true);
            $this->fields["Apelido"] = (new Field("Apelido"))
            ->set("label", "Apelido")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "Apelido do Servidor")
            ->set("required", true);

        $this->fields["Endereco"] = (new Field("Endereco"))
            ->set("label", "Endereço")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "Endereço")
            ->set("maxlen", 15)
            ->set("minlen", 7)
            ->set("required", true);

        $this->fields["Porta"] = (new Field("Porta"))
            ->set("label", "Porta")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("placeholder", "Porta")
            ->set("maxnum", 999)
            ->set("minnum", 100)
            ->set("required", true);

        $this->fields["Map"] = (new Field("Map"))
            ->set("label", "Template de Mapeamento")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "#Insira aqui o template de mapeamento");

        $this->fields["Seguranca"] = (new Field("Seguranca"))
            ->set("label", "Segurança")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_SELECT)
            ->set("placeholder", "Segurança")
            ->set("value", "NONE")
            ->set("maxlen", 18)
            ->set("options", [
                ["label" => "Nenhum", "value" => "NONE"],
                ["label" => "SSL", "value" => "SSL"],
                ["label" => "TLS", "value" => "TLS"]
            ]);
        $this->fields["CriadoEm"] = (new Field("CriadoEm"))
            ->set("label", "Criado Em")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "CriadoEm");

        $this->fields["EditadoEm"] = (new Field("EditadoEm"))
            ->set("label", "Editado Em")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "EditadoEm");
    }

    public function onLoadView(View &$View)
    {
        $View::addJavascript("plugins/jasny/jasny-bootstrap.min");
    }

    public function onFill_Map(Field &$field, $data)
    {
        $field->value = $data->TemplateMapeamento;
    }
}