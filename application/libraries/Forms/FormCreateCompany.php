<?php

namespace ES3\Forms;

use ES3\View\View;
use ES3\Validations;

class FormCreateCompany extends Form
{
    public function loadFields()
    {
        $this->setName("formCreateCompany");
        $this->fields["cid"] = (new Field("cid"))
            ->set("label", "ID do cliente")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("placeholder", "ID do cliente")
            ->set("maxnum", 9999999)
            ->set("minnum", 2)
            ->set("required", true);

        $this->fields["RazaoSocial"] = (new Field("RazaoSocial"))
            ->set("label", "Nome / Razão Social")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "Nome / Razão Social")
            ->set("maxlen", 200)
            ->set("minlen", 3)
            ->set("required", true);
        
        $this->fields["PForPJ"] = (new Field("PForPJ"))
            ->set("label", "Tipo da empresa")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_RADIO)
            ->set("value", "PJ")
            ->set("options", [
                "rdPF" => ["label" => "Pessoa física", "value" => "PF"],
                "rdPJ" => ["label" => "Pessoa Jurídica", "value" => "PJ"]
            ]);
        $this->fields["Tipo"] = (new Field("Tipo"))
            ->set("label", "Tipo da empresa")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_RADIO)
            ->set("value", "CLIENTE")
            ->set("options", [
                "rdCLIENTE" => ["label" => "Cliente", "value" => "CLIENTE"],
                "rdREVENDA" => ["label" => "Revenda", "value" => "REVENDA"]
            ]);

        $this->fields["Cnpj"] = (new Field("Cnpj"))
            ->set("label", "CNPJ")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "CNPJ")
            ->set("mask", "99.999.999/9999-99")
            ->set("maxlen", 18);
        $this->fields["Cpf"] = (new Field("Cpf"))
            ->set("label", "CPF")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("placeholder", "CPF")
            ->set("mask", "999.999.999-99")
            ->set("maxlen", 14);

        $this->fields["IDRevenda"] = (new Field("IDRevenda"))
            ->set("label", "Revenda")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_SELECT)
            ->set("required", true);

        $this->fields["LimiteExecucoes"] = (new Field("LimiteExecucoes"))
            ->set("label", "Limite de execuções")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("placeholder", "Limite de execuções")
            ->set("value", 0)
            ->set("minnum", 0)
            ->set("required", true);
        $this->fields["LimiteRepeticoes"] = (new Field("LimiteRepeticoes"))
            ->set("label", "Limite de repetições")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("placeholder", "Limite de repetições")
            ->set("value", 0)
            ->set("minnum", 0)
            ->set("required", true);
    }

    public function onParseValue_Cnpj(Field &$field)
    {
        $field->value = preg_replace('/[^0-9]/', '', (string)$field->value);
    }

    public function onParseValue_Cpf(Field &$field)
    {
        $field->value = preg_replace('/[^0-9]/', '', (string)$field->value);
    }

    public function posValidate_Cnpj(Field $field)
    {
        if ($this->PForPJ->value == "PJ" && !Validations::cnpjIsValid($field->value)) {
            $this->fieldValidation->set($field->name, "invalid::cnpj", "O número do CNPJ não é válido");
            return false;
        }
    }

    public function posValidate_Cpf(Field $field)
    {
        if ($this->PForPJ->value == "PF" && !Validations::cpfIsValid($field->value)) {
            $this->fieldValidation->set($field->name, "invalid::cpf", "O número do CPF não é válido");
            return false;
        }
    }
}