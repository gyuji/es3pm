<?php

namespace ES3\Forms;

use ES3\View\View;
use ES3\Utils;
use ES3\POST;
use ES3\Validations;

class FormConfirmAccounts extends Form
{
    public $accList = [];
    private $permitedDomains = [];

    public function loadFields()
    {
        $this->setName("formConfirmAccounts");
        $this->fields["addrOrig"] = (new Field("addrOrig"))
            ->set("label", "Endereço de origem")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_EMAIL)
            ->set("maxlen", 200)
            ->set("required", true);
        $this->fields["addrDest"] = (new Field("addrDest"))
            ->set("label", "Endereço de destino")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_EMAIL)
            ->set("maxlen", 200)
            ->set("required", true);
        $this->fields["passOrig"] = (new Field("passOrig"))
            ->set("label", "Senha de origem")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("maxlen", 200)
            ->set("required", true);
        $this->fields["passDest"] = (new Field("passDest"))
            ->set("label", "Senha de destino")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_TEXT)
            ->set("maxlen", 200)
            ->set("required", true);
    }

    public function customValidate()
    {
        $this->fieldValidation = new FieldValidation();

        if (!$this->permitedDomains) {
            $this->fieldValidation->set("accList", "notdomain", "Antes de cadastrar as contas é necessário que a sua empresa possua pelo menos 1 domínio vinculado.<br>
            Por favor, contato o adminsitrador do sistema solicitando o registro do domínio.");
            return $this->fieldValidation;
        }

        $this->accList = POST::get("accList");
        if (!$this->accList) {
            $this->fieldValidation->set("accList", "required", "Selecione pelo menos 1 conta");
            return $this->fieldValidation;
        }
        foreach ($this->accList as $index => &$acc) {
            $acc = (object)$acc;
            $line = ($acc->line ?? 0) + 1;
            $acc->addrOrig = trim($acc->addrOrig ?? null);
            $acc->passOrig = trim($acc->passOrig ?? null);
            $acc->addrDest = trim($acc->addrDest ?? null);
            $acc->passDest = trim($acc->passDest ?? null);
            // is required
            if (!$acc->addrOrig || !$acc->passOrig || !$acc->addrDest || !$acc->passDest) {
                $this->fieldValidation->set("accList::$line", "required::$line", "Preencha todos os campos da linha #<b>$line</b>");
                continue;
            }
            // maxlen
            if (strlen($acc->addrOrig) > $this->addrOrig->maxlen) {
                $this->fieldValidation->set("accList::$line", "maxlen::$line::addrOrig", "Na linha #<b>$line</b> o <b>endereço de origem</b> deve possuir no máximo {$this->addrOrig->maxlen} carácteres");
                continue;
            }
            if (strlen($acc->passOrig) > $this->passOrig->maxlen) {
                $this->fieldValidation->set("accList::$line", "maxlen::$line::passOrig", "Na linha #<b>$line</b> a <b>senha de origem</b> deve possuir no máximo {$this->passOrig->maxlen} carácteres");
                continue;
            }
            if (strlen($acc->addrDest) > $this->addrDest->maxlen) {
                $this->fieldValidation->set("accList::$line", "maxlen::$line::addrDest", "Na linha #<b>$line</b> o <b>endereço de destino</b> deve possuir no máximo {$this->addrDest->maxlen} carácteres");
                continue;
            }
            if (strlen($acc->passDest) > $this->passDest->maxlen) {
                $this->fieldValidation->set("accList::$line", "maxlen::$line::passDest", "Na linha #<b>$line</b> a <b>senha de destino</b> deve possuir no máximo {$this->passDest->maxlen} carácteres");
                continue;
            }

            // is valid email
            if (!Validations::isValidEmail($acc->addrOrig)) {
                $this->fieldValidation->set("accList::$line::addrOrig", "invalid::$line::addrOrig", "Na linha #<b>$line</b> o <b>endereço de origem</b> não é válido");
                continue;
            }
            if (!Validations::isValidEmail($acc->addrDest)) {
                $this->fieldValidation->set("accList::$line::addrDest", "invalid::$line::addrDest", "Na linha #<b>$line</b> o <b>endereço de destino</b> não é válido");
                continue;
            }

            $acc->domainDest = explode("@", $acc->addrDest)[1];

            if (!in_array($acc->domainDest, $this->getDomains())) {
                $this->fieldValidation->set("accList::$line::addrDest", "invalid::$line::notpermiteddomain", 
                "Na linha #<b>$line</b> você não tem permissão para migrar contas para o domínio <b>{$acc->domainDest}</b>, apenas <b>".(implode(", ", $this->getDomains()))."</b>.<br>
                Caso seja um engano, por favor contate o administrador do sistema para registrar o domíni <b>{$acc->domainDest}</b>");
                return $this->fieldValidation;
            }
            $acc->domain = $this->permitedDomains[$acc->domainDest];
        }
        return $this->fieldValidation;
    }

    public function setDomains(array $domains)
    {
        foreach ($domains as $row) {
            $this->permitedDomains[$row->Endereco] = $row;
        }
    }

    public function getDomains(bool $getRegister = false)
    {
        return $getRegister ? $this->permitedDomains : array_keys($this->permitedDomains);
    }
}