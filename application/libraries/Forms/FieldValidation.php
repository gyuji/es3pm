<?php

namespace ES3\Forms;

class FieldValidation
{
    private $listValidation = [];

    public function set(String $fieldName, String $code, String $message)
    {
        $this->listValidation[$fieldName] = (object)[
            "code" => $code,
            "msg" => $message
        ];

        return $this;
    }

    public function get(String $fieldName = null)
    {
        return $fieldName ? ($this->listValidation[$fieldName] ?? (object)["code" => null, "msg" => null]) : $this->listValidation;
    }

    public function getHtml()
    {
        $html = "";
        foreach ($this->listValidation as $field => $validation) {
            $html .= "<hide-data name=\"fieldvalidation::{$field}\">{$validation->code}</hide-data>
                <p class=\"m-tb-5\">{$validation->msg}</p>
            ";
        }
        return $html;
    }

    public function unset(String $fieldName)
    {
        if (isset($this->listValidation[$fieldName])) unset($this->listValidation[$fieldName]);
        return $this;
    }
}