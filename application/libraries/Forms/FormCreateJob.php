<?php

namespace ES3\Forms;

class FormCreateJob extends Form
{
    public function loadFields()
    {
        $this->setName("formCreateJob");
        $this->fields["IDEmpresas"] = (new Field("IDEmpresas"))
            ->set("label", "Empresa")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["IDDominios"] = (new Field("IDDominios"))
            ->set("label", "Domínio")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["IDServidores"] = (new Field("IDServidores"))
            ->set("label", "Servidor")
            ->set("typeValue", Form::TYPE_INT)
            ->set("required", true);
        $this->fields["Repeticoes"] = (new Field("Repeticoes"))
            ->set("label", "Número de repeticoes")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("minnum", 1)
            ->set("value", 1)
            ->set("required", true);
        $this->fields["Execucoes"] = (new Field("Execucoes"))
            ->set("label", "Número de execições simultâneas")
            ->set("typeValue", Form::TYPE_INT)
            ->set("typeInput", Form::TYPE_INPUT_NUMBER)
            ->set("minnum", 1)
            ->set("value", 1)
            ->set("required", true);
    }
}