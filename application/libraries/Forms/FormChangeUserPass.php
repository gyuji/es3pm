<?php

namespace ES3\Forms;

class FormChangeUserPass extends Form
{
    public function loadFields()
    {
        $this->setName("formChangeUserPass");
        $this->fields["password1"] = (new Field("password1"))
            ->set("label", "Senha")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_PASSWORD)
            ->set("placeholder", "Senha")
            ->set("maxlen", 200)
            ->set("minlen", 6)
            ->set("required", true);

        $this->fields["password2"] = (new Field("password2"))
            ->set("label", "Confirmar senha")
            ->set("typeValue", Form::TYPE_STR)
            ->set("typeInput", Form::TYPE_INPUT_PASSWORD)
            ->set("placeholder", "Confirmar senha")
            ->set("maxlen", 200)
            ->set("minlen", 6)
            ->set("required", true);
    }

    public function posValidate_password2(Field $field)
    {
        if ($field->value != $this->password1->value) {
            $this->fieldValidation->set($name, "notequal", "As senhas não conferem");
            return false;
        }   
    }
}