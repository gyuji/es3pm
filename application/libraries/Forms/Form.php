<?php

namespace ES3\Forms;

use ES3\Validations;
use ES3\POST;
use ES3\FILES;
use ES3\Utils;

class Form
{
    const TYPE_STR = "str";
    const TYPE_INT = "int";
    const TYPE_FLOAT = "float";
    const TYPE_BOOL = "bool";
    const TYPE_FILE = "file";
    const TYPE_CSV = "csv";
    const TYPE_INPUT_EMAIL = "email";
    const TYPE_INPUT_TEXT = "text";
    const TYPE_INPUT_NUMBER = "number";
    const TYPE_INPUT_PASSWORD = "password";
    const TYPE_INPUT_RADIO = "radio";
    const TYPE_INPUT_SELECT = "select";
    const TYPE_INPUT_HIDDEN = "hidden";
    const TYPE_INPUT_IP = "text:ip";
    const TYPE_INPUT_DOMAIN = "text:domain";
    const TYPE_INPUT_IP_DOMAIN = "text:ip:domain";
    const TYPE_INPUT_FILE = "file";
    const TYPE_INPUT_TEXTAREA = "textarea";
    
    private $fields = [];
    private $fieldValidation;
    private $formName;
    public $btnSubmit = true;
    public $btnDelete = true;
    public $btnRestore = true;

    static public $messages = [
        "required" => "O campo <b>%s</b> é obrigatório",
        "maxlen" => "O valor do campo <b>%s</b> excedeu <b>%s</b> carácteres",
        "minlen" => "O valor do campo <b>%s</b> deve possuir no mínimo <b>%s</b> carácteres",
        "maxnum" => "O valor do campo <b>%s</b> deve ser menor que <b>%s</b>",
        "minnum" => "O valor do campo <b>%s</b> deve ser maior que <b>%s</b>",
        "invalid:email" => "O valor do campo <b>%s</b> não é um endereço de e-mail válido",
        "invalid:option" => "A opção assinalada do campo <b>%s</b> não é válida",
        "invalid:domain" => "O endereço de domínio do campo <b>%s</b> não é válido",
        "invalid:ip" => "O endereço de IPv4 do campo <b>%s</b> não é válido",
        "invalid:ipdomain" => "O endereço de IPv4/domínio do campo <b>%s</b> não é válido",
    ];

    public function __construct(bool $justLoad = true)
    {
        if (method_exists($this, "loadFields")) $this->loadFields();

        if (!$justLoad) foreach ($this->fields as $key => $fieldConfig) {
            if ($fieldConfig->typeValue == self::TYPE_FILE) {
                $this->fields[$key]->value = FILES::get($key);
            } else {
                $this->fields[$key]->value = POST::getFieldValue($key);
                if ($fieldConfig->typeValue == self::TYPE_CSV) {
                    $this->fields[$key]->value = $this->fields[$key]->value ? Utils::readCsvStr($this->fields[$key]->value) : null;
                }
            }
            if (method_exists($this, "onParseValue_".$key)) $this->{"onParseValue_".$key}($this->fields[$key]);
        }
    }

    public function &__get(String $key)
    {
        if (isset($this->fields[$key])) {
            return $this->fields[$key];
        }
        return $this->$key;
    }

    public function setName(String $name)
    {
        $this->formName = $name;
    }

    public function getName()
    {
        return $this->formName;
    }

    public function validate()
    {
        $this->fieldValidation = new FieldValidation();
        foreach ($this->fields as $name => $configField) {
            if (method_exists($this, "preValidate_".$name)) {
                if (!$this->{"preValidate_".$name}($configField)) continue;
            }
            if (is_string($configField->value)) $configField->value = trim($configField->value);
            if ($configField->required && ($configField->value === "" || $configField->value === null)) {
                $this->fieldValidation->set($name, "required", sprintf(self::$messages["required"], $configField->label));
                continue;
            }

            if (in_array($configField->typeInput, [
                    self::TYPE_INPUT_EMAIL, self::TYPE_INPUT_PASSWORD, self::TYPE_INPUT_TEXT,
                    self::TYPE_INPUT_DOMAIN, self::TYPE_INPUT_IP, self::TYPE_INPUT_IP_DOMAIN
                ])) {
                if ($configField->minlen && strlen($configField->value) < $configField->minlen) {
                    $this->fieldValidation->set($name, "minlen", sprintf(self::$messages["minlen"], $configField->label, $configField->minlen));
                    continue;
                }

                if ($configField->maxlen && strlen($configField->value) > $configField->maxlen) {
                    $this->fieldValidation->set($name, "maxlen", sprintf(self::$messages["maxlen"], $configField->label, $configField->maxlen));
                    continue;
                }
            }

            if ($configField->typeInput == self::TYPE_INPUT_NUMBER) {
                if ($configField->minnum && $configField->value < $configField->minnum) {
                    $this->fieldValidation->set($name, "minnum", sprintf(self::$messages["minnum"], $configField->label, $configField->minnum));
                    continue;
                }

                if ($configField->maxnum && $configField->value > $configField->maxnum) {
                    $this->fieldValidation->set($name, "maxnum", sprintf(self::$messages["maxnum"], $configField->label, $configField->maxnum));
                    continue;
                }
            }

            if ($configField->typeInput == self::TYPE_INPUT_EMAIL) {
                if (!Validations::isValidEmail($configField->value)) {
                    $this->fieldValidation->set($name, "invalid:email", sprintf(self::$messages["invalid:email"], $configField->label));
                    continue;
                }
            }

            if ($configField->typeInput == self::TYPE_INPUT_DOMAIN) {
                if (!Validations::isDomain($configField->value)) {
                    $this->fieldValidation->set($name, "invalid:domain", sprintf(self::$messages["invalid:domain"], $configField->label));
                    continue;
                } 
            }

            if ($configField->typeInput == self::TYPE_INPUT_IP) {
                if (!Validations::isIP($configField->value)) {
                    $this->fieldValidation->set($name, "invalid:ip", sprintf(self::$messages["invalid:ip"], $configField->label));
                    continue;
                }
            }

            if ($configField->typeInput == self::TYPE_INPUT_IP_DOMAIN) {
                if (!Validations::isDomain($configField->value) && !Validations::isIP($configField->value)) {
                    $this->fieldValidation->set($name, "invalid:ipdomain", sprintf(self::$messages["invalid:ipdomain"], $configField->label));
                    continue;
                }
            }

            if ($configField->typeInput == self::TYPE_INPUT_RADIO) {
                $isValid = false;
                if ($configField->options) {
                    foreach ($configField->options as $option) {
                        if ($option["value"] == $configField->value) {
                            $isValid = true;
                            break;
                        }
                    }
                } else $isValid = true;
                if (!$isValid) {
                    $this->fieldValidation->set($name, "invalid:option", sprintf(self::$messages["invalid:option"], $configField->label));
                    continue;
                }
            }

            if (method_exists($this, "posValidate_".$name)) {
                if (!$this->{"posValidate_".$name}($configField)) continue;
            }

            $this->fieldValidation->unset($name);
        }
        return $this->fieldValidation;
    }

    public function getValidate(String $field = null)
    {
        return $this->fieldValidation->get($field);
    }

    public function toObject()
    {
        $object = (object)[];
        foreach ($this->fields as $name => $configField) {
            $object->$name = $configField->value;
        }
        return $object;
    }

    public function fill($data)
    {
        $data = (object)$data;
        foreach ($this->fields as $key => &$field) {
            if (method_exists($this, "onFill_{$key}")) $this->{"onFill_".$key}($field, $data);
            else if (isset($data->$key)) $field->value = $data->$key;
        }
        return $this;
    }

    public function disableAll()
    {
        foreach ($this->fields as $key => &$field) {
            $field->disabled = true;
        }
        return $this;
    }
}