<?php

namespace ES3;

use ES3\Validations;

class POST
{
    const TYPE_STR = "str";
    const TYPE_INT = "int";
    const TYPE_FLOAT = "float";
    const TYPE_BOOL = "bool";

    static public function get($key, String $type = null)
    {
        if ($type) {
            if ($type == self::TYPE_INT) {
                return self::getInt($key);
            } elseif ($type == self::TYPE_BOOL) {
                return self::getBool($key);
            } elseif ($type == self::TYPE_FLOAT) {
                return self::getFloat($key);
            } else {
                return self::get($key);
            }
        } else {
            return $_POST[$key] ?? null;
        }
    }

    static public function getFieldValue(String $field)
    {
        $field = self::get($field);
        if (!$field || !is_array($field)) return null;
        $field["value"] = $field["value"] ?? null;
        if ($field["typeValue"] == self::TYPE_INT) {
            return self::getInt($field["value"], true);
        } elseif ($field["typeValue"] == self::TYPE_BOOL) {
            return self::getBool($field["value"], true);
        } elseif ($field["typeValue"] == self::TYPE_FLOAT) {
            return self::getFloat($field["value"], true);
        } else  {
            return trim($field["value"]);
        }
    }

    static public function getInt($key, bool $convert = false)
    {
        return $convert ? Validations::isValidInt($key) : Validations::isValidInt(self::get($key));
    }

    static public function getBool($key, bool $convert = false)
    {
        $value = $convert ? trim(strtolower($key)) : trim(strtolower(self::get($key)));
        if (in_array($value, ["0", "null", "false", ""])) return false;
        return true;
    }

    static public function getFloat($key, bool $convert = false)
    {
        return $convert ? Validations::isValidFloat($key) : Validations::isValidFloat(self::get($key));
    }

    static public function getArr(Array $valueList = null)
    {
        if ($valueList) {
            foreach ($valueList as $key => $value) {
                if ($value == self::TYPE_STR) {
                    $valueList[$key] = self::get($key);
                } elseif ($value == self::TYPE_INT) {
                    $valueList[$key] = self::getInt($key);
                } elseif ($value == self::TYPE_BOOL) {
                    $valueList[$key] = self::getBool($key);
                } elseif ($value == self::TYPE_FLOAT) {
                    $valueList[$key] = self::getFloat($key);
                }
            }
            return $valueList;
        }
        return $_POST;
    }
}