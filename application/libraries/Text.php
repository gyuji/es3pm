<?php

namespace ES3;

class Text
{
    static function startsWith(string $haystack = null, string $needle)
    {
        if (!$haystack) return false;
        return $needle === '' || strrpos($haystack, $needle, - strlen($haystack)) !== false;
    }
    
    static function endsWith(string $haystack, string $needle)
    {
        if ($needle === '') {
            return true;
        }
        $diff = \strlen($haystack) - \strlen($needle);
        return $diff >= 0 && strpos($haystack, $needle, $diff) !== false;
    }

    static function unsetChar(string &$text, $index) {
        if ($index == ":first") {
            $text = substr($text, 1);
        } elseif ($index == ":last") {
            $text = substr($text, 0, -1);
        }
    }

    static function unsetDuplicates(string &$text, string $char)
    {
        if ($char == ":slash") {
            $text = preg_replace('/(\/){1,}/', "$1", $text);
        } elseif ($char == ":backslash") {
            $text = preg_replace('/(\\\){1,}/', "$1", $text);
        } else {
            $text = preg_replace('/('.$char.'){1,}/', "$1", $text);
        }
    }
}