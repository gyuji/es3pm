-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 14-Nov-2018 às 02:32
-- Versão do servidor: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portalmigracao`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas`
--

DROP TABLE IF EXISTS `contas`;
CREATE TABLE IF NOT EXISTS `contas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDDominios` int(11) NOT NULL,
  `EndOrigem` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SenhaOrigem` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `EndDestino` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SenhaDestino` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CriadoEm` datetime NOT NULL,
  `EditadoEm` datetime DEFAULT NULL,
  `RemovidoEm` timestamp NULL DEFAULT NULL,
  `Ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_contas_dominios1_idx` (`IDDominios`),
  KEY `Ativo` (`Ativo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contas`
--

INSERT INTO `contas` (`ID`, `IDDominios`, `EndOrigem`, `SenhaOrigem`, `EndDestino`, `SenhaDestino`, `CriadoEm`, `EditadoEm`, `RemovidoEm`, `Ativo`) VALUES
(1, 12, 'fulano1@domino.com', 'minhaSenha', 'fulano1@houseti.com.br', 'minhaSenha', '2018-11-12 12:09:06', '2018-11-13 09:51:19', '2018-11-13 12:06:39', 0),
(2, 12, 'fulano3@domino.com', 'minhaSenha', 'fulano3@houseti.com.br', 'oigofsdgfsdfffffffffffffffffffffffffffffoigofsdgfsdfffffffffffffffffffffffffffffoigofsdgfsdfffffffffffffffffffffffffffffoigofsdgfsdfffffffffffffffffffffffffffffoigofsdgfsdfffffffffffffffffffffffffffff', '2018-11-12 12:09:06', '2018-11-13 09:46:31', '2018-11-13 12:05:16', 0),
(3, 12, 'fulano1@domino.com', 'minhaSenha', 'fulano1@houseti.com.br', 'minhaSenha', '2018-11-12 12:10:20', '2018-11-13 10:06:34', NULL, 1),
(4, 12, 'fulano2@domino.com', 'minhaSenha', 'fulano2@houseti.com.br', 'minhaSenha', '2018-11-12 12:10:20', '2018-11-13 09:46:06', NULL, 1),
(5, 12, 'fulano3@domino.com', 'minhaSenha', 'fulano3@houseti.com.br', 'teste', '2018-11-12 12:10:20', '2018-11-13 09:46:18', '2018-11-13 12:06:06', 0),
(6, 22, 'fulano1@domino.com', 'minhaSenha', 'fulano1@fasdf.com', 'minhaSenha', '2018-11-12 14:00:05', NULL, '2018-11-13 12:06:18', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dominios`
--

DROP TABLE IF EXISTS `dominios`;
CREATE TABLE IF NOT EXISTS `dominios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDEmpresas` int(11) NOT NULL,
  `Endereco` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CriadoEm` datetime NOT NULL,
  `EditadoEm` datetime DEFAULT NULL,
  `RemovidoEm` datetime DEFAULT NULL,
  `Ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDEmpresas` (`IDEmpresas`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `dominios`
--

INSERT INTO `dominios` (`ID`, `IDEmpresas`, `Endereco`, `CriadoEm`, `EditadoEm`, `RemovidoEm`, `Ativo`) VALUES
(1, 1, 'imap.gmail.com', '2018-11-10 12:52:28', NULL, '2018-11-10 18:09:19', 0),
(2, 1, 'houseti.com.br', '2018-11-10 12:54:20', NULL, '2018-11-10 18:35:52', 0),
(3, 1, 'dualtec.com.br', '2018-11-10 12:54:48', NULL, '2018-11-10 18:35:57', 0),
(4, 1, 'gmail.com.br', '2018-11-10 12:55:28', NULL, '2018-11-10 18:36:00', 0),
(5, 1, 'afff.com.br', '2018-11-10 12:55:44', NULL, NULL, 0),
(6, 1, 'afff.com.br', '2018-11-10 12:56:35', NULL, '2018-11-10 18:11:28', 0),
(7, 1, 'affs2.com.com', '2018-11-10 12:57:11', NULL, '2018-11-10 18:09:52', 0),
(8, 1, 'yuji.com.br', '2018-11-10 13:17:38', NULL, '2018-11-10 18:11:23', 0),
(9, 1, 'melqui.com.br', '2018-11-10 13:18:18', NULL, '2018-11-10 18:10:59', 0),
(10, 2, 'teste.com.br', '2018-11-10 13:20:26', NULL, '2018-11-10 18:09:57', 0),
(11, 1, 'fdsa.com', '2018-11-10 18:11:33', NULL, '2018-11-10 18:12:40', 0),
(12, 1, 'houseti.com.br', '2018-11-10 18:36:02', NULL, NULL, 1),
(13, 1, 'houseti.com.fdsa', '2018-11-10 18:36:13', NULL, '2018-11-10 18:52:12', 0),
(14, 1, 'fdas.com.fdsa', '2018-11-10 18:36:49', NULL, '2018-11-10 18:52:10', 0),
(15, 1, 'fdsfsd', '2018-11-10 18:37:46', NULL, '2018-11-10 18:52:07', 0),
(16, 1, 'fdasfdas', '2018-11-10 18:41:07', NULL, '2018-11-10 18:52:05', 0),
(17, 1, '192.168.0.2', '2018-11-10 18:51:10', NULL, '2018-11-10 18:52:02', 0),
(18, 1, '12.255.234.255', '2018-11-10 18:51:27', NULL, '2018-11-10 18:51:59', 0),
(19, 1, 'fdasfdas.com', '2018-11-10 18:52:41', NULL, NULL, 1),
(20, 1, 'imap.houseti.com.br', '2018-11-10 18:54:37', NULL, NULL, 1),
(21, 1, 'imap.houseti.com', '2018-11-10 18:54:49', NULL, NULL, 1),
(22, 1, 'fasdf.com', '2018-11-10 18:55:15', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

DROP TABLE IF EXISTS `empresas`;
CREATE TABLE IF NOT EXISTS `empresas` (
  `ID` int(11) NOT NULL,
  `IDEmpresasRevenda` int(11) NOT NULL,
  `CnpjCpf` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `RazaoSocial` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Tipo` enum('CLIENTE','REVENDA') COLLATE utf8_unicode_ci NOT NULL,
  `LimiteExecucoes` int(11) NOT NULL,
  `LimiteRepeticoes` int(11) NOT NULL,
  `CriadoEm` datetime NOT NULL,
  `EditadoEm` datetime DEFAULT NULL,
  `RemovidoEm` datetime DEFAULT NULL,
  `Ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Cnpj_UNIQUE` (`CnpjCpf`),
  KEY `fk_empresas_empresas1_idx` (`IDEmpresasRevenda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`ID`, `IDEmpresasRevenda`, `CnpjCpf`, `RazaoSocial`, `Tipo`, `LimiteExecucoes`, `LimiteRepeticoes`, `CriadoEm`, `EditadoEm`, `RemovidoEm`, `Ativo`) VALUES
(1, 1, '18941423000171', 'Empresa root', 'REVENDA', 40, 5, '2018-09-21 00:00:00', '2018-09-21 00:00:00', NULL, 1),
(2, 1, '32360307000190', 'Empresa filha - salva 1', 'CLIENTE', 10, 10, '2018-11-02 00:00:00', '2018-11-07 14:27:02', NULL, 1),
(3, 1, '446028844829', 'Empresa revenda filha', 'REVENDA', 10, 10, '2018-11-05 00:00:00', NULL, NULL, 1),
(4, 3, '46028844828', 'Empresa cliente de revenda filha', 'CLIENTE', 0, 0, '2018-11-05 00:00:00', '2018-11-09 13:33:50', NULL, 0),
(5, 1, '46028844829', 'teste', 'REVENDA', 0, 0, '2018-11-07 00:07:26', '2018-11-09 15:36:36', NULL, 1),
(6, 1, '22812548000179', 'Elias e Vitória Mudanças Ltda', 'CLIENTE', 5, 0, '2018-11-07 09:25:12', NULL, NULL, 1),
(343, 1, '62207444031', 'fdsfdas', 'CLIENTE', 0, 0, '2018-11-09 13:35:55', '2018-11-09 13:37:18', NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDEmpresas` int(11) NOT NULL,
  `IDServidores` int(11) NOT NULL,
  `IDDominios` int(11) NOT NULL,
  `Execucoes` int(11) NOT NULL,
  `Repeticoes` int(11) NOT NULL,
  `Status` enum('AGUARDANDO','EXECUTANDO','PAUSADO','CANCELADO','CONCLUIDO') COLLATE utf8_unicode_ci NOT NULL,
  `CriadoEm` datetime NOT NULL,
  `EditadoEm` datetime DEFAULT NULL,
  `RemovidoEm` datetime DEFAULT NULL,
  `Ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_jobs_dominios1_idx` (`IDDominios`),
  KEY `fk_jobs_servidores1_idx` (`IDServidores`),
  KEY `fk_jobs_empresas1_idx` (`IDEmpresas`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `jobs`
--

INSERT INTO `jobs` (`ID`, `IDEmpresas`, `IDServidores`, `IDDominios`, `Execucoes`, `Repeticoes`, `Status`, `CriadoEm`, `EditadoEm`, `RemovidoEm`, `Ativo`) VALUES
(1, 1, 1, 12, 1, 1, 'AGUARDANDO', '2018-11-13 13:25:23', '2018-11-13 23:56:28', '2018-11-14 00:11:09', 0),
(6, 1, 1, 12, 1, 1, 'AGUARDANDO', '2018-11-14 00:21:52', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servidores`
--

DROP TABLE IF EXISTS `servidores`;
CREATE TABLE IF NOT EXISTS `servidores` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Apelido` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Endereco` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Porta` int(11) NOT NULL,
  `Seguranca` enum('NONE','SSL','TLS') COLLATE utf8_unicode_ci NOT NULL,
  `TemplateMapeamento` mediumtext COLLATE utf8_unicode_ci,
  `CriadoEm` datetime NOT NULL,
  `EditadoEm` datetime DEFAULT NULL,
  `RemovidoEm` datetime DEFAULT NULL,
  `Ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servidores`
--

INSERT INTO `servidores` (`ID`, `Apelido`, `Endereco`, `Porta`, `Seguranca`, `TemplateMapeamento`, `CriadoEm`, `EditadoEm`, `RemovidoEm`, `Ativo`) VALUES
(1, 'imap.houseti.com.br', 'imap.houseti.co', 143, 'NONE', '--f1f2 \"fdsa\"=\"fsa\"', '2018-11-12 14:02:21', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `transferencias`
--

DROP TABLE IF EXISTS `transferencias`;
CREATE TABLE IF NOT EXISTS `transferencias` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDContas` int(11) NOT NULL,
  `IDJobs` int(11) NOT NULL,
  `Script` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `IDAutenticacoesOrigem` int(11) NOT NULL,
  `IDAutenticacoesDestino` int(11) NOT NULL,
  `PID` int(11) DEFAULT NULL,
  `Inicio` datetime DEFAULT NULL,
  `Termino` datetime DEFAULT NULL,
  `MsgMaxLineLength` int(11) DEFAULT NULL,
  `MsgOrigem` int(11) DEFAULT NULL,
  `MsgDestino` int(11) DEFAULT NULL,
  `MsgSinc` int(11) DEFAULT NULL,
  `MsgRestantes` int(11) DEFAULT NULL,
  `QtdErro` int(11) DEFAULT NULL,
  `MsgIgno` int(11) DEFAULT NULL,
  `BOrigem` bigint(20) DEFAULT NULL,
  `BDestino` bigint(20) DEFAULT NULL,
  `BRestantes` bigint(20) DEFAULT NULL,
  `BIgno` bigint(20) DEFAULT NULL,
  `BErro` bigint(20) DEFAULT NULL,
  `Erros` longtext COLLATE utf8_unicode_ci,
  `PastasOrigem` longtext COLLATE utf8_unicode_ci,
  `PastasDestino` longtext COLLATE utf8_unicode_ci,
  `KBsVelocidade` float(6,3) DEFAULT NULL,
  `BSinc` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_transferencias_contas1_idx` (`IDContas`),
  KEY `fk_transferencias_jobs1_idx` (`IDJobs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDEmpresas` int(11) NOT NULL,
  `Nome` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Senha` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CriadoEm` datetime NOT NULL,
  `EditadoEm` datetime DEFAULT NULL,
  `RemovidoEm` datetime DEFAULT NULL,
  `Ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_funcionarios_empresas1_idx` (`IDEmpresas`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`ID`, `IDEmpresas`, `Nome`, `Email`, `Senha`, `CriadoEm`, `EditadoEm`, `RemovidoEm`, `Ativo`) VALUES
(2, 1, 'Soares', 'gabriel.soaresdelima@gmail.com', '$2y$10$.KHf3NGtyivGHLjUh6gEIu/Lq1iSfAGDantTnhCeq//dquwA2ynzu', '2018-11-02 15:25:00', '2018-11-13 13:59:36', NULL, 1),
(3, 2, 'Yuji', 'gabriel.yuji@gmail.com', '$2y$10$VFPSi3lWUGq4ooX90RvDPueu5noRtHuLrUUOBzOxfwJEi42rwnHUK', '2018-11-02 00:00:00', NULL, NULL, 0),
(4, 1, 'TESTE 80.808.787/0001-51', 'gabriel.soares@houseti.com.br', '$2y$10$VpGG8JdiDtJT82ayY2mi2.w0/fM3LsEYG3YvJzxbchTf/zIK.bgqq', '2018-11-09 12:44:40', '2018-11-09 12:55:22', NULL, 1),
(5, 1, 'TESTE 80.808.787/0001-51', 'gabriel@houseti.com.br', '$2y$10$v.cGSiduGXiKaEWhLbxU3.wPf/14rn3LFBqk4c8FHqeH/VnHD7v3G', '2018-11-09 12:46:23', NULL, NULL, 0),
(6, 1, 'TESTE 80.808.787/0001-51', 'gabriel.lima@houseti.com.br', '$2y$10$Z.zzLWF1DJIE97itAUHQjelB4E8.zeL6U0SJhLMdsLt9oo6NSVgWm', '2018-11-09 12:49:50', NULL, NULL, 0),
(7, 1, 'soares', 'soares@gmail.com', '$2y$10$R7.A1jjaCtCOYKnwwTO11ONE2FnzGoaGjAERuKb3l4Ie/kAQMlZKm', '2018-11-09 12:51:23', NULL, NULL, 0),
(8, 1, 'lima Soares', 'lima@gmail.com', '$2y$10$aRjbx/6Mx75.5jEealUKHe2t8xP5DMBv8c0rUMNhMFg/0GYMKq5DK', '2018-11-09 12:54:29', '2018-11-09 12:54:47', NULL, 0),
(9, 1, 'alan', 'alan@gmail.com', '$2y$10$ufInTJ15TvioRw83Euz6geDQWQnDiKNDqNT05XHe3i68UdXTv7i.e', '2018-11-09 12:56:55', '2018-11-09 12:57:28', NULL, 0),
(10, 1, 'TESTE 80.808.787/0001-51', 'alan@gmail.com', '$2y$10$dxkC09K2/u67l5utOL8GMOVurOCS64roGAS84qUv5xy5A9CphNtOm', '2018-11-09 13:25:13', NULL, NULL, 0),
(11, 1, 'soares', 'soares@houseti.com', '$2y$10$lBEYBCSABm8VVF4nOSMxCe13MtpSNf8g2vjqLPMcAM34HyuzlVlyu', '2018-11-09 13:30:10', '2018-11-13 13:59:09', NULL, 1),
(12, 1, 'Yuji', 'yuji@gmail.com', '$2y$10$7rvXQ1AP4aR/Va5Ktdob.uvw1y.MB8w/why3/Zh2IY5NqSQ0fNVJi', '2018-11-09 15:34:51', '2018-11-09 15:35:03', NULL, 0),
(13, 1, 'teste', 'teste@gmail.com', '$2y$10$.QI5MSmYb2Sds/s.n5/v3.XJIdOc/PIkcG6KChUDuuTcQV8CExnkS', '2018-11-10 11:07:24', NULL, NULL, 1),
(14, 6, 'teste', 'teste2@gmail.com', '$2y$10$YNWP9is.qaXjaaKsiJ6qqOHNvqLcJp1Loz8YhI17dChdHcHuC0s7O', '2018-11-10 11:07:38', NULL, NULL, 1),
(15, 4, 'Gabriel Soares de Lima', 'oioi@gmail.com', '$2y$10$wMJDEw2gwIyfADEaUhDsz.KSebCYQCs7MVaSixHuSyUNbY0b//g/.', '2018-11-10 11:08:59', '2018-11-13 13:59:53', NULL, 1),
(16, 1, 'Gabriel', 'tchau@fds.com', '$2y$10$lin/tlvqGgjen3FpnJHQCuUBXEmaHnpqzF/0b/DZ3vF7ZCcUrmb/.', '2018-11-10 11:13:45', NULL, NULL, 1),
(17, 5, 'OI teste', 'teste3@gmail.com', '$2y$10$YqDO5RPgPHmMwjPyXgvOguHRi/8i8i924j9vi3puuxyy8OTIChVcu', '2018-11-10 11:24:50', '2018-11-10 11:29:27', NULL, 1),
(18, 5, 'teste 6', 'teste4@gmail.com', '$2y$10$w6KiSokVZMedtUFraFdGNeY/gc88UQfvhHVzZftGckFb5aU6RaazS', '2018-11-10 11:29:58', NULL, NULL, 1),
(19, 5, 'testes', 'teste5@gmail.com', '$2y$10$NQadN3WDFRKKxG1nAf.vUOa.Dl6VgxVHqH3.SXlgnSw4nrrjOcaOq', '2018-11-10 11:31:32', NULL, NULL, 1),
(20, 5, 'teste', 'teste6@gmail.com', '$2y$10$2TxwvHLlPZZjrxvMyA2HQeJr2GGdo6cuV5M8BBVNLhIjEi/PEdHyC', '2018-11-10 11:32:16', NULL, NULL, 1),
(21, 1, 'teste6@gmail.com', 'teste7@gmail.com', '$2y$10$.2GiKaOxoZfAJpp/AJPIMOFktpGsckaX0XgG3.z79FqV8OVCuc/5a', '2018-11-10 11:32:42', NULL, NULL, 1),
(22, 5, 'Gabriel Soares de Lima', 'teste8@gmail.com', '$2y$10$34WU2S7SAvav0wczLcybCuJi40KlAK6WpeHAT7rTLS8cIXHeTHy96', '2018-11-10 11:36:42', '2018-11-12 13:58:28', NULL, 1),
(23, 5, 'teste2@gmail.com', 'teste9@gmail.com', '$2y$10$RcciysYc/Y5vQjrRBIvBuuLy0mh3t0EmpiDdH8Kcymk4JXTcM.dmO', '2018-11-10 11:39:57', NULL, NULL, 1),
(24, 5, 'teste2@gmail.com', 'teste10@gmail.com', '$2y$10$cq6MVdCiIEeCwlJcN4ZJXu4JcDBWeRiZVuW6dMa0qzAvWzWEiYULC', '2018-11-10 11:41:47', NULL, NULL, 1),
(25, 5, 'teste11@gmail.com', 'teste11@gmail.com', '$2y$10$c1XI35/l0RaKyUEHtnC61.VBvBQIia.uduf0eKxjoH.4EfoZtmX6i', '2018-11-10 11:42:31', NULL, NULL, 1),
(26, 5, 'teste12@gmail.com', 'teste12@gmail.com', '$2y$10$19f9dNAZOIHSI923afwTx.QqC3SKniyg141LvD/Tyxqii.9TuVECK', '2018-11-10 11:42:49', NULL, NULL, 1);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `contas`
--
ALTER TABLE `contas`
  ADD CONSTRAINT `fk_contas_dominios1` FOREIGN KEY (`IDDominios`) REFERENCES `dominios` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `dominios`
--
ALTER TABLE `dominios`
  ADD CONSTRAINT `fk_dominios_empresas1` FOREIGN KEY (`IDEmpresas`) REFERENCES `empresas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `fk_empresas_empresas1` FOREIGN KEY (`IDEmpresasRevenda`) REFERENCES `empresas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `fk_jobs_dominios1` FOREIGN KEY (`IDDominios`) REFERENCES `dominios` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jobs_empresas1` FOREIGN KEY (`IDEmpresas`) REFERENCES `empresas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jobs_servidores1` FOREIGN KEY (`IDServidores`) REFERENCES `servidores` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `transferencias`
--
ALTER TABLE `transferencias`
  ADD CONSTRAINT `fk_transferencias_contas1` FOREIGN KEY (`IDContas`) REFERENCES `contas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transferencias_jobs1` FOREIGN KEY (`IDJobs`) REFERENCES `jobs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_funcionarios_empresas1` FOREIGN KEY (`IDEmpresas`) REFERENCES `empresas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
