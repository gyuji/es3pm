App.controller("ctrJobs", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.formCreateJob = {
        sending : false,
        data : $s._VB.formCreateJob,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formCreateJob;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/jobs/create",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                }
            });
        }
    }

    $s.formEditJob = {
        sending : false,
        data : $s._VB.formEditJob,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formEditJob;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/jobs/editjob",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                }
            });
        },
        deleteJob: function() {
            var _self = $s.formEditJob;
            _self.deleting = true;
            $s.http({
                url: "/jobs/delete/"+_self.data.ID.value,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.deleting = false;
                },
                success: function(resp) {
                    swal("Feito!", resp.msg, "success");
                },
                failed: function(resp) {
                    swal("Ops!", resp.msg ? resp.msg : "Houve um erro inesperado", "error");
                },
                httpError : function() {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                }
            });
        },
        confirmDelete: function() {
            var _self = $s.formEditJob;
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente remover o job #"+_self.data.ID.value+"?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode remover",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formEditJob.deleteJob();
            });
        }
    }

    $s.filterGrid = {
        ID: null,
        RazaoSocial: null,
        EndrecoServidor: null,
        EnderecoDominio: null,
        Status: null,
        filter : function(rowData) {
            _self = $s.filterGrid;
            passRow = true;
            angular.forEach(rowData, function(value, field){
                switch (field) {
                    case "ID":
                        if (_self.ID && _self.ID != value) passRow = false;
                        break;
                    case "Empresas":
                        value.RazaoSocial = value.RazaoSocial.toLowerCase();
                        _self.RazaoSocial = _self.RazaoSocial ? _self.RazaoSocial.toLowerCase() : null;
                        if (_self.RazaoSocial && value.RazaoSocial.indexOf(_self.RazaoSocial) == -1) passRow = false;
                        break;
                    case "Servidores":
                        value.Endereco = value.Endereco.toLowerCase();
                        _self.EndrecoServidor = _self.EndrecoServidor ? _self.EndrecoServidor.toLowerCase() : null;
                        if (_self.EndrecoServidor && value.Endereco.indexOf(_self.EndrecoServidor) == -1) passRow = false;
                        break;
                    case "Dominios":
                        value.Endereco = value.Endereco.toLowerCase();
                        _self.EnderecoDominio = _self.EnderecoDominio ? _self.EnderecoDominio.toLowerCase() : null;
                        if (_self.EnderecoDominio && value.Endereco.indexOf(_self.EnderecoDominio) == -1) passRow = false;
                        break;
                    case "Status":
                        if (_self.Status && _self.Status != value) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            $s.filterGrid.filtermode = !$s.filterGrid.filtermode;
            if (!$s.filterGrid.filtermode) {
                $s.filterGrid.ID = null;
                $s.filterGrid.RazaoSocial = null;
                $s.filterGrid.EndrecoServidor = null;
                $s.filterGrid.EnderecoDominio = null;
                $s.filterGrid.Status = null;
            }
        }
    }

    $s.filterGridAcc = {
        EndOrigem: null,
        EndDestino: null,
        Execucao: null,
        Status: null,
        filter : function(rowData) {
            _self = $s.filterGridAcc;
            passRow = true;
            angular.forEach(rowData, function(value, field){
                switch (field) {
                    case "EndOrigem":
                        value = value.toLowerCase();
                        _self.EndOrigem = _self.EndOrigem ? _self.EndOrigem.toLowerCase() : null;
                        if (_self.EndOrigem && value.indexOf(_self.EndOrigem) == -1) passRow = false;
                        break;
                    case "EndDestino":
                        value = value.toLowerCase();
                        _self.EndDestino = _self.EndDestino ? _self.EndDestino.toLowerCase() : null;
                        if (_self.EndDestino && value.indexOf(_self.EndDestino) == -1) passRow = false;
                        break;
                    case "Execucao":
                        if (_self.Status && _self.Status != value) passRow = false;
                        break;
                    case "Status":
                        if (_self.Status && _self.Status != value) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            $s.filterGridAcc.filtermode = !$s.filterGridAcc.filtermode;
            if (!$s.filterGridAcc.filtermode) {
                $s.filterGridAcc.EndOrigem = null;
                $s.filterGridAcc.EndDestino = null;
                $s.filterGridAcc.Execucao = null;
                $s.filterGridAcc.Status = null;
            }
        }
    }
});
