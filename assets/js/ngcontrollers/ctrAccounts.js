App.controller("ctrAccounts", function($scope) {
    // Abreviação de scope
    $s = $scope;
    $s.csvTextareaEditor = null;
    $s.domainSelectedGrid = null;

    $s.formEditAccounts = {
        gettingAccounts : false,
        complete : false,
        data : $s._VB.formEditAccounts,
        domainSelectedGrid: null,
        domainsLoaded : [],
        alert : new Alert(),
        setSelectedDomain: function(domain) {
            var _self = $s.formEditAccounts;
            _self.domainSelectedGrid = domain;
            
            if (typeof _self.domainsLoaded[domain.ID] == "undefined") _self.domainsLoaded[domain.ID] = {
                domain: domain,
                accounts: []
            }

            if (_self.domainsLoaded[domain.ID].accounts.length > 0) return;
            
            _self.gettingAccounts = true;
            $s.http({
                url: "/accounts/getbydomain/"+domain.ID,
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.gettingAccounts = false;
                },
                success: function(resp) {
                    if (typeof resp.data != "array") {
                        this.frm.domainsLoaded[domain.ID].accounts = resp.data;
                    } else {
                        swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                    }
                },
                failed: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                },
                httpError : function() {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                }
            });
        },
        save: function(accRow, $index) {
            var _self = $s.formEditAccounts;
            _self.data.index.value = $index;
            _self.data.ID.value = accRow.ID;
            _self.data.passOrig.value = accRow.SenhaOrigem;
            _self.data.passDest.value = accRow.SenhaDestino;
            _self.data.addrOrig.value = accRow.EndOrigem;
            _self.data.addrDest.value = accRow.EndDestino;

            $s.http({
                url: "accounts/editaccount",
                data: _self.data,
                frm: _self,
                accRow: accRow,
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    if (!resp.msg) this.httpError();
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, tente novamente mais tarde", "error");
                }
            });
        },
        delete: function($index) {
            var _self = $s.formEditAccounts;
            _self.deleting = true;
            _self.alert = new Alert();
            _self.accRow = _self.domainsLoaded[_self.domainSelectedGrid.ID].accounts[$index];
            $s.http({
                url: "/accounts/deleteaccount/"+_self.accRow.ID,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.deleting = false;
                },
                success: function(resp) {
                    swal("Feito!", resp.msg, "success");
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    this.frm.domainsLoaded[this.frm.domainSelectedGrid.ID].accounts.splice($index, 1);
                },
                failed: function(resp) {
                    swal("Ops!",  resp.msg ? resp.msg : "Houve um erro inesperado", "error");
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, tente novamente mais tarde", "error");
                }
            });
        },
        confirmDelete : function($index) {
            acc = $s.formEditAccounts.domainsLoaded[$s.formEditAccounts.domainSelectedGrid.ID].accounts[$index];
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente remover a conta "+acc.EndOrigem+" -> "+acc.EndDestino+" (Linha #"+($index+1)+")?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode remover",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formEditAccounts.delete($index);
            });
        },
    }

    $s.formUpListAccounts = {
        sending : false,
        complete : false,
        data : $s._VB.formUpListAccounts,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formUpListAccounts;
            $s.csvTextareaEditor.save();
            _self.data.csvText.value = $s.csvTextareaEditor.getTextArea().value;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/accounts/create/upfile",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    if (typeof resp.data.accList != "undefined") {
                        $s.formConfirmAccounts.accList = resp.data.accList;
                        _self.complete = true;
                    } else {
                        swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                    }
                },
                failed: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                },
                httpError : function() {
                    swal("Ops!", "Houve um erro inesperado, por favor tente novamente", "error");
                }
            });
        }
    }

    $s.formConfirmAccounts = {
        sending : false,
        complete : false,
        data : [],
        accList : null,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formConfirmAccounts;
            _self.sending = true;
            _self.alert = new Alert();
            _self.data.accList = $s.formConfirmAccounts.getAccList();
            console.log($s.formConfirmAccounts.getAccList());
            $s.http({
                url: "/accounts/create/confirm",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    if (typeof resp.data.formConfirmAccounts != "undefined") {
                        $s._VB.formConfirmAccounts = resp.data.formConfirmAccounts;

                    }
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        },
        confirmDeleteLine: function($index) {
            $s.formConfirmAccounts.accList.splice($index, 1);
        },
        nbAcc: function(active = true) {
            nb = 0;
            angular.forEach($s.formConfirmAccounts.accList, function(acc, i) {
                nb += active && !acc.deleted ? 1 : 0;
            });
            return nb;
        },
        getAccList: function(active = true) {
            accList = [];
            angular.forEach($s.formConfirmAccounts.accList, function(acc, i) {
                acc.line = i;
                if (active && !acc.deleted) accList.push(acc);
                else if (!active && acc.deleted) accList.push(acc);
            });
            return accList;
        } 
    }


    $s.filterGrid = {
        index : null,
        EndOrig : null,
        EndDest : null,
        SenhaOrig : null,
        SenhaDest : null,
        filter : function(rowData, $index) {
            passRow = true;
            if ($s.filterGrid.index && $s.filterGrid.index != $index + 1) passRow = false;
            angular.forEach(rowData, function(value, field) {
                switch (field) {
                    case "EndOrigem":
                        value = value.toLowerCase();
                        $s.filterGrid.EndOrig = $s.filterGrid.EndOrig ? $s.filterGrid.EndOrig.toLowerCase() : null;
                        if ($s.filterGrid.EndOrig && value.indexOf($s.filterGrid.EndOrig) == -1) passRow = false;
                        break;
                    case "EndDestino":
                        value = value.toLowerCase();
                        $s.filterGrid.EndDest = $s.filterGrid.EndDest ? $s.filterGrid.EndDest.toLowerCase() : null;
                        if ($s.filterGrid.EndDest && value.indexOf($s.filterGrid.EndDest) == -1) passRow = false;
                        break;
                    case "SenhaOrigem":
                        value = value.toLowerCase();
                        $s.filterGrid.SenhaOrig = $s.filterGrid.SenhaOrig ? $s.filterGrid.SenhaOrig.toLowerCase() : null;
                        if ($s.filterGrid.SenhaOrig && value.indexOf($s.filterGrid.SenhaOrig) == -1) passRow = false;
                        break;
                    case "SenhaDestino":
                        value = value.toLowerCase();
                        $s.filterGrid.SenhaDest = $s.filterGrid.SenhaDest ? $s.filterGrid.SenhaDest.toLowerCase() : null;
                        if ($s.filterGrid.SenhaDest && value.indexOf($s.filterGrid.SenhaDest) == -1) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            $s.filterGrid.filtermode = !$s.filterGrid.filtermode;
            if (!$s.filterGrid.filtermode) {
                $s.filterGrid.EndOrig = null;
                $s.filterGrid.EndDest = null;
                $s.filterGrid.SenhaOrig = null;
                $s.filterGrid.SenhaDest = null;
            }
        }
    }

    // Document ready
    $(document).ready(function(){
        if (typeof CodeMirror != "undefined") $s.csvTextareaEditor = CodeMirror.fromTextArea(csvTextarea, {
            lineNumbers: true,
            matchBrackets: true
        });
    });
});
