App.controller("ctrUsers", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.formCreateUser = {
        sending : false,
        data : $s._VB.formCreateUser,
        alert : new Alert(),
        // Métodos
        onsubmit : function() {
            var _self = $s.formCreateUser;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/users/create",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    resp.data.userData.IDEmpresas = parseInt(resp.data.userData.IDEmpresas);
                    $s._VB.usersData.push(resp.data.userData);
                    $s._VB.formCreateUser.displayname.value = null;
                    $s._VB.formCreateUser.username.value = null;
                    $s._VB.formCreateUser.password1.value = null;
                    $s._VB.formCreateUser.password2.value = null;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, tente novamente mais tarde", "error");
                }
            });
        }
    }

    $s.formGridEditUser = {
        sending : false,
        data : $s._VB.formGridEditUser,
        alert : new Alert(),

        // Métodos
        onsubmit : function(userRow) {
            var _self = $s.formGridEditUser;
            _self.sending = true;
            _self.alert = new Alert();

            _self.data.ID.value = userRow.form.ID;
            _self.data.IDEmpresas.value = userRow.form.IDEmpresas;
            _self.data.displayname.value = userRow.form.Nome;
            _self.data.username.value = userRow.form.Email;
            _self.data.password1.value = userRow.form.password1;
            _self.data.password2.value = userRow.form.password2;

            $s.http({
                url: "/users/edit",
                data: _self.data,
                frm: _self,
                userRow: userRow,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    this.userRow.editmode = false;
                    if (typeof this.userRow.validate == "undefined") this.userRow.validate = {};
                    this.userRow.form.validate.Nome = null;
                    this.userRow.form.validate.IDEmpresas = null;
                    this.userRow.form.validate.Email = null;
                    this.userRow.form.validate.password2 = null;

                    userRow.IDEmpresas = _self.data.IDEmpresas.value;
                    userRow.Nome = _self.data.displayname.value;
                    userRow.Email = _self.data.username.value;
                },
                failed: function(resp) {
                    if (resp.msg) {
                        this.frm.alert.type = "danger";
                        this.frm.alert.msg = resp.msg;
                    }
                    if (typeof resp.validate != "undefined") {
                        if (typeof this.userRow.form.validate == "undefined") this.userRow.form.validate = {};
                        this.userRow.form.validate.IDEmpresas = typeof resp.validate.IDEmpresas != "undefined" ? resp.validate.IDEmpresas.msg : null;
                        this.userRow.form.validate.Nome = typeof resp.validate.displayname != "undefined" ? resp.validate.displayname.msg : null;
                        this.userRow.form.validate.Email = typeof resp.validate.username != "undefined" ? resp.validate.username.msg : null;
                        this.userRow.form.validate.password1 = typeof resp.validate.password1 != "undefined" ? resp.validate.password1.msg : null;
                        this.userRow.form.validate.password2 = typeof resp.validate.password2 != "undefined" ? resp.validate.password2.msg : null;
                    }
                },
                httpError: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, tente novamente mais tarde", "error");
                }
            });
        },
        deleteUser : function($index) {
            var _self = $s.formGridEditUser;
            _self.deleting = true;
            _self.alert = new Alert();
            _self.userRow = $s._VB.usersData[$index];
            $s.http({
                url: "/users/delete/"+_self.userRow.ID,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.deleting = false;
                },
                success: function(resp) {
                    swal("Feito!", resp.msg, "success");
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    $s._VB.usersData.splice($index, 1);
                },
                failed: function(resp) {
                    swal("Ops!", 
                        resp.msg ? resp.msg : "Houve um erro inesperado", 
                        "error");
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, tente novamente mais tarde", "error");
                }
            });
        },
        confirmDelete : function($index) {
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente remover o usuário "+$s._VB.usersData[$index].Nome+" ("+$s._VB.usersData[$index].Email+")?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode remover",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formGridEditUser.deleteUser($index);
            });
        },
    }

    $s.filterGrid = {
        IDEmpresas : null,
        Nome : null,
        Email : null,
        editmode : false,
        filter : function(rowData) {
            passRow = true;
            angular.forEach(rowData, function(value, field){
                switch (field) {
                    case "IDEmpresas":
                        if ($s.filterGrid.IDEmpresas && $s.filterGrid.IDEmpresas != value) passRow = false;
                        break;
                    case "Nome":
                        value = value.toLowerCase();
                        $s.filterGrid.Nome = $s.filterGrid.Nome ? $s.filterGrid.Nome.toLowerCase() : null;
                        if ($s.filterGrid.Nome && value.indexOf($s.filterGrid.Nome) == -1) passRow = false;
                        break;
                    case "Email":
                        value = value.toLowerCase();
                        $s.filterGrid.Email = $s.filterGrid.Email ? $s.filterGrid.Email.toLowerCase() : null;
                        if ($s.filterGrid.Email && value.indexOf($s.filterGrid.Email) == -1) passRow = false;
                        break;
                    case "CriadoEm":
                        if ($s.filterGrid.CriadoEm && value.indexOf($s.filterGrid.CriadoEm) == -1) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            $s.filterGrid.filtermode = !$s.filterGrid.filtermode;
            if (!$s.filterGrid.filtermode) {
                $s.filterGrid.IDEmpresas = null;
                $s.filterGrid.Nome = null;
                $s.filterGrid.Email = null;
            }
        }
    }

    $s.typeOf = function($var) {
        console.log($var);
        return typeof $var;
    }
});