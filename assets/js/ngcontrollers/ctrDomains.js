App.controller("ctrDomains", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.formCreateDomain = {
        sending : false,
        data : $s._VB.formCreateDomain,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formCreateDomain;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/domains/create",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    resp.data.domainData.IDEmpresas = parseInt(resp.data.domainData.IDEmpresas);
                    $s._VB.domainsData.push(resp.data.domainData);
                    $s.formCreateDomain.data.Endereco.value = null;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        }
    }

    $s.formDeleteDomain = {
        sending : false,
        alert : new Alert(),

        // Métodos
        delete : function($index) {
            var _self = $s.formDeleteDomain;
            _self.sending = true;
            _self.alert = new Alert();
            _self.domainRow = $s._VB.domainsData[$index];
            $s.http({
                url: "/domains/delete/"+_self.domainRow.ID,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    swal("Feito!", resp.msg, "success");
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                    $s._VB.domainsData.splice($index, 1);
                },
                failed: function(resp) {
                    swal("Ops!", resp.msg ? resp.msg : "Houve um erro inesperado",  "error");
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError: function(resp) {
                    swal("Ops!", "Houve um erro inesperado, tente novamente mais tarde", "error");
                }
            });
        },
        confirmDelete : function($index) {
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente remover o domínio "+_VB.domainsData[$index].Endereco+"?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode remover",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formDeleteDomain.delete($index);
            });
        },
    }

    $s.filterGrid = {
        IDEmpresas : null,
        Endereco : null,
        editmode : false,
        filter : function(rowData) {
            passRow = true;
            angular.forEach(rowData, function(value, field){
                switch (field) {
                    case "IDEmpresas":
                        if ($s.filterGrid.IDEmpresas && $s.filterGrid.IDEmpresas != value) passRow = false;
                        break;
                    case "Endereco":
                        value = value.toLowerCase();
                        $s.filterGrid.Endereco = $s.filterGrid.Endereco ? $s.filterGrid.Endereco.toLowerCase() : null;
                        if ($s.filterGrid.Endereco && value.indexOf($s.filterGrid.Endereco) == -1) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            $s.filterGrid.filtermode = !$s.filterGrid.filtermode;
            if (!$s.filterGrid.filtermode) {
                $s.filterGrid.IDEmpresas = null;
                $s.filterGrid.Endereco = null;
            }
        }
    }
});