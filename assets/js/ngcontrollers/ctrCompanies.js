App.controller("ctrCompanies", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.tableCompanySearch = function(company) {
        if (typeof $s.tableCompanySearchPattern == "undefined") $s.tableCompanySearchPattern = "";
        var pattern = $s.tableCompanySearchPattern.toLowerCase().trim();
        return !pattern || company.ID == pattern ||
            company.RazaoSocial.toLowerCase().indexOf(pattern) >= 0 ||
            company.CnpjCpf.toString().indexOf(pattern) >= 0 ||
            company.Tipo.toLowerCase().indexOf(pattern) >= 0 ||
            $s._VB.companiesList[company.IDEmpresasRevenda].RazaoSocial.toLowerCase().indexOf(pattern) >= 0;
    }

    $s.formCreateCompany = {
        sending : false,
        data : $s._VB.formCreateCompany,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formCreateCompany;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/companies/create",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        }
    }

    $s.formEditCompany = {
        sending : false,
        data : $s._VB.formEditCompany,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formEditCompany;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/companies/edit/"+_self.data.cid.value,
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        },
        disableCompany : function() {
            var _self = $s.formEditCompany;
            _self.disabling = true;
            $s.http({
                url: "/companies/disable/"+_self.data.cid.value,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.disabling = false;
                },
                success: function(resp) {
                    swal("Feito!", 
                        resp.msg, 
                        "success");
                },
                failed: function(resp) {
                    swal("Ops!", 
                        resp.msg ? resp.msg : "Houve um erro inesperado", 
                        "error");
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        },
        enableCompany : function() {
            var _self = $s.formEditCompany;
            _self.enabling = true;
            $s.http({
                url: "/companies/enable/"+_self.data.cid.value,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.enabling = false;
                },
                success: function(resp) {
                    swal("Feito!", 
                        resp.msg, 
                        "success");
                },
                failed: function(resp) {
                    swal("Ops!", 
                        resp.msg ? resp.msg : "Houve um erro inesperado", 
                        "error");
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        },
        confirmDisableCompany : function() {
            var _self = $s.formEditCompany;
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente desabilitar a empresa \"CID"+_self.data.cid.value+" "+_self.data.RazaoSocial.value+"\"?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode desabilitar",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formEditCompany.disableCompany();
            });
        },
        confirmEnableCompany: function() {
            var _self = $s.formEditCompany;
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente habilitar a empresa \"CID"+_self.data.cid.value+" "+_self.data.RazaoSocial.value+"\"?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode habilitar",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formEditCompany.enableCompany();
            });
        }
    }

    $s.filterGrid = {
        ID : null,
        RazaoSocial : null,
        IDEmpresasRevenda : null,
        Tipo : null,
        CnpjCpf : null,
        Ativo : null,
        editmode : false,
        filter : function(rowData) {
            var _self = $s.filterGrid;
            passRow = true;
            angular.forEach(rowData, function(value, field){
                switch (field) {
                    case "ID":
                        if (_self.ID && _self.ID != value) passRow = false;
                        break;
                    case "RazaoSocial":
                        value = value.toLowerCase();
                        _self.RazaoSocial = _self.RazaoSocial ? _self.RazaoSocial.toLowerCase() : null;
                        if (_self.RazaoSocial && value.indexOf(_self.RazaoSocial) == -1) passRow = false;
                        break;
                    case "IDEmpresasRevenda":
                        value = $s._VB.companiesList[value].RazaoSocial.toLowerCase();
                        _self.IDEmpresasRevenda = _self.IDEmpresasRevenda ? _self.IDEmpresasRevenda.toLowerCase() : null;
                        if (_self.IDEmpresasRevenda && value.indexOf(_self.IDEmpresasRevenda) == -1) passRow = false;
                        break;
                    case "CnpjCpf":
                        if (_self.CnpjCpf && value.indexOf(_self.CnpjCpf) == -1) passRow = false;
                        break;
                    case "Tipo":
                        value = value.toLowerCase();
                        _self.Tipo = _self.Tipo ? _self.Tipo.toLowerCase() : null;
                        if (_self.Tipo && value.indexOf(_self.Tipo) == -1) passRow = false;
                        break;
                    case "Ativo":
                        if (_self.Ativo && _self.Ativo != value) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            var _self = $s.filterGrid;
            $s.filterGrid.filtermode = !$s.filterGrid.filtermode;
            if (!$s.filterGrid.filtermode) {
                _self.ID = null;
                _self.RazaoSocial = null;
                _self.IDEmpresasRevenda = null;
                _self.Tipo = null;
                _self.CnpjCpf = null;
                _self.Ativo = null;
            }
        }
    }
});