App.controller("ctrLogin", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.formLogin = new function() {
        _self = this;

        this.sending = false;
        this.data = $s._VB.formLogin;
        this.alert = new Alert();

        // Métodos
        this.onsubmit = function() {
            _self.sending = true;
            this.alert = new Alert();
            $s.http({
                url: "/auth/login",
                data: this.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        }
    }
});