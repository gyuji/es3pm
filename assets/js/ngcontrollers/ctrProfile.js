App.controller("ctrProfile", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.formChangeUserPass = {
        sending : false,
        data : $s._VB.formChangeUserPass,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formChangeUserPass;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/userprofile/changepassword",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                }
            });
        }
    }

    $s.formChangeDisplayName = {
        sending : false,
        data : $s._VB.formChangeDisplayName,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formChangeDisplayName;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/userprofile/changedisplayname",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        }
    }
});