App.controller(_VB.ngCtrMain, function($scope, Upload) {
    // Abreviação de scope
    $s = $scope;
    $s._VB = _VB;

    // Métodos
    $s.http = function (httpConfig) {
        self = this;
        if (typeof self.verboseHistory == "undefined") self.verboseHistory = [];
        data = typeof httpConfig.data == "object" ? httpConfig.data : {};
        url = typeof httpConfig.url == "string" ? _VB.baseURL+httpConfig.url : null;
        method = typeof httpConfig.method == "string" ? httpConfig.method : "POST";
        httpConfig.verbose = typeof httpConfig.verbose == "undefined" ? false : httpConfig.verbose;
        if (!url) return false;

        Upload.upload({
            url: url,
            data: data,
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'HTTP_IS_AJAX' : '1',
                'HTTP_ERROR_AJAX' : '1'
            },
            transformResponse: [
                function (data) {
                    try {
                        return JSON.parse(data);
                    } catch (e) {
                        return data;
                    }
                }
            ]
        }).then(function (response) {
            verbose = {
                "status" : true,
                "httpConfig" : httpConfig,
                "response" : response,
                "err" : null
            };
            try {
                if ((!response || response && !response.data) && typeof httpConfig.httpError == "function") httpConfig.httpError(response);
                // onComplete
                if (typeof httpConfig.onComplete == "function")  httpConfig.onComplete(response.data);
                // httpSuccess
                if (typeof httpConfig.httpSuccess == "function") httpConfig.httpSuccess(response.data);
                // success
                if (response.data.status && typeof httpConfig.success == "function") httpConfig.success(response.data);
                // failed
                else if (!response.data.status && typeof httpConfig.failed == "function") httpConfig.failed(response.data);
                // redirect
                if (typeof response.data.redirect == "string" && response.data.redirect) location = response.data.redirect;
            } catch (err) {
                verbose.err = err;
                if (httpConfig.verbose) console.log(verbose);
            }
            self.verboseHistory.push(verbose);
        }, function (response) {
            verbose = {
                "status" : false,
                "httpConfig" : httpConfig,
                "response" : response,
                "err" : null
            };
            try {
                if ((!response || response && !response.data) && typeof httpConfig.httpError == "function") httpConfig.httpError(response);
                // onComplete
                if (typeof httpConfig.onComplete == "function")  httpConfig.onComplete(response.data);
                // httpError
                if (typeof response.data == "string" && typeof httpConfig.httpError == "function") httpConfig.httpError(response.data);
                else if (typeof httpConfig.httpError == "function") httpConfig.httpError(response);
                // redirect
                if (typeof response.data.redirect == "string" && response.data.redirect) $scope.redir(response.data.redirect);
            } catch (err) {
                verbose.err = err;
                if (httpConfig.verbose) console.log(verbose);
            }
            self.verboseHistory.push(verbose);
        }, function (response) {
            if (typeof httpConfig.progress == "function") httpConfig.progress(response);
        });
    }

    $s.typeOf = function($var) {
        console.log($var);
        return typeof $var;
    }
});