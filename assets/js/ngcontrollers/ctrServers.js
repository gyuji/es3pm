App.controller("ctrServers", function($scope) {
    // Abreviação de scope
    $s = $scope;

    $s.tableServerSearch = function(server) {
        if (typeof $s.tableServerSearchPattern == "undefined") $s.tableServerSearchPattern = "";
        var pattern = $s.tableServerSearchPattern.toLowerCase().trim();
        return !pattern || server.ID == pattern ||
            server.Apelido.toLowerCase().indexOf(pattern) >= 0 ||
            server.Endereco.toString().indexOf(pattern) >= 0 ||
            server.Porta.toLowerCase().indexOf(pattern) >= 0
    }

    $s.formCreateServer = {
        sending : false,
        data : $s._VB.formCreateServer,
        alert : new Alert(),

        // Métodos
        onsubmit : function() {
            var _self = $s.formCreateServer;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/servers/create",
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        }
    }

    $s.formEditServer = {
        sendingSave : false,
        data : $s._VB.formEditServer,
        alert : new Alert(),
        // Métodos
        onsubmit : function() {
            var _self = $s.formEditServer;
            _self.sending = true;
            _self.alert = new Alert();
            $s.http({
                url: "/servers/edit/"+_self.data.ID.value,
                data: _self.data,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.sending = false;
                },
                success: function(resp) {
                    this.frm.alert.type = "success";
                    this.frm.alert.msg = resp.msg;
                },
                failed: function(resp) {
                    this.frm.alert.type = "danger";
                    this.frm.alert.msg = resp.msg;
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        },
        deleteServer : function() {
            var _self = $s.formEditServer;
            _self.disabling = true;
            $s.http({
                url: "/servers/delete/"+_self.data.ID.value,
                frm: _self,
                onComplete: function(resp) {
                    this.frm.disabling = false;
                },
                success: function(resp) {
                    swal("Feito!", 
                        resp.msg, 
                        "success");
                },
                failed: function(resp) {
                    swal("Ops!", 
                        resp.msg ? resp.msg : "Houve um erro inesperado", 
                        "error");
                },
                httpError : function() {
                    swal("Ops!", 
                        "Houve um erro inesperado, por favor tente novamente", 
                        "error");
                }
            });
        },
        confirmDeleteServer : function() {
            var _self = $s.formEditServer;
            swal({
                title: "Tem certeza?",
                text: "Deseja realmente desabilitar a empresa "+_self.data.Apelido.value+" ("+_self.data.Endereco.value+")?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode deletar",
                closeOnConfirm: false
            }, function () {
                swal("Processando...", "", "info");
                $s.formEditServer.deleteServer();
            });
        }
    }

    $s.filterGrid = {
        ID: null,
        Apelido: null,
        Endereco: null,
        Porta: null,
        Seguranca: null,
        Ativo: null,
        filter : function(rowData) {
            passRow = true;
            _self = $s.filterGrid;
            angular.forEach(rowData, function(value, field) {
                switch (field) {
                    case "ID":
                        if (_self.Ativo && _self.Ativo != value) passRow = false;
                        break;
                    case "Apelido":
                        value = value.toLowerCase();
                        _self.Apelido = _self.Apelido ? _self.Apelido.toLowerCase() : null;
                        if (_self.Apelido && value.indexOf(_self.Apelido) == -1) passRow = false;
                        break;
                    case "Endereco":
                        if (!isNaN(value)) value = value.toLowerCase();  
                        _self.Endereco = _self.Endereco ? _self.Endereco.toLowerCase() : null;
                        if (_self.Endereco && value.indexOf(_self.Endereco) == -1) passRow = false;
                        break;
                    case "Porta":
                        if (_self.Porta && _self.Porta != value) passRow = false;
                        break;
                    case "Seguranca":
                        if (_self.Seguranca && value != _self.Seguranca) passRow = false;
                        break;
                    case "Ativo":
                        if (_self.Ativo && _self.Ativo != value) passRow = false;
                        break;
                }
            });
            return passRow;
        },
        toggle : function() {
            $s.filterGrid.filtermode = !$s.filterGrid.filtermode;
            if (!$s.filterGrid.filtermode) {
                $s.filterGrid.ID = null;
                $s.filterGrid.Apelido = null;
                $s.filterGrid.Endereco = null;
                $s.filterGrid.Porta = null;
                $s.filterGrid.Seguranca = null;
                $s.filterGrid.Ativo = null;
            }
        }
    }
});