App = angular.module(_VB.ngApp, ["ngFileUpload", "NgSwitchery"])
.filter('trusted', ['$sce', function($sce){
    return function(text) {
        try {
            return $sce.trustAsHtml(text);
        } catch (err) {}
    };
}])
.filter('formatCurrency', ['$sce', function($sce){
    return function(currency) {
        try {
            return "R$ "+currency.replace(/,/g,'').replace('.', ',');
        } catch (err) {}
    };
}])
.filter('replace', function () {
    return function (input, from, to) {
        try {
            input = input || '';
            from = from || '';
            to = to || '';
            return input.replace(new RegExp(from, 'g'), to);
        } catch (err) {}
    };
});

class Alert {
    constructor(msg = null, type = null, dismiss = false) {
        this.msg = msg;
        this.type = type;
        this.dismiss = dismiss;
    }

    clear() {
        this.msg = null;
        this.type = null;
        this.dismiss = false;
    }
}

function getHideData() {
    var hideData = [];
    $("hide-data").each(function(i, o){
        hideData.push({
            name: $(o).attr("name"),
            content: $(o).html(),
            elem: o
        });
    });
    return hideData;
}


// JQuery
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green'
});