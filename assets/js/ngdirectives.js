App.directive("es3Alert", () => {
    return {
        controller: ($scope) => {
        },
        scope: {
            alertData: "=alertdata"
        },
        template: `
        <div class="m-5 alert alert-dismissable text-left alert-{{alertData.type}}" ng-if="alertData.msg">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button" ng-if="alertData.dismiss">×</button>
            <p ng-bind-html="alertData.msg |trusted"></p>
        </div>
        `
    };
});

App.directive("messageReport", function(){
    return {
        controller: function($scope) {},
        scope: {
            msgData: "=msg"
        },
        template: `
            <span ng-show="msgData.content" style="color: {{msgData.status ? 'green' : 'red'}}" ng-bind-html="msgData.content |trusted"></span>
        `
    };
});